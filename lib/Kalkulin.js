"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var DemandDB = require('./database/DemandDB.js');

var CycleDB = require('./database/CycleDB.js');

var RoomDB = require('./database/RoomDB.js');

var ResultDB = require('./database/ResultDB.js');

var HotelDB = require('./database/HotelDB.js');

var ConfigDB = require('./database/ConfigDB.js');

var RevenueDB = require('./database/HotelRevenueDB');

var HotelCostsDB = require('./database/HotelCostsDB');

var Demand = require('./models/Demand.js');

var Room = require('./models/Room.js');

var Hotel = require('./models/Hotel.js');

var Revenue = require('./models/Revenue');

var HotelCosts = require('./models/HotelCosts');

var GameConfiguration = require('./GameConfiguration.js');

var Kalkulin =
/*#__PURE__*/
function () {
  function Kalkulin() {
    var cycleDb = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : './data/cycleDB.json';
    var demandDb = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : './data/demandDB.json';
    var roomDb = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : './data/RoomDB.json';
    var resultDb = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : './data/resultDB.json';
    var hotelDb = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : './data/hotelDB.json';
    var configDb = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : './data/gameConfigDB.json';

    _classCallCheck(this, Kalkulin);

    this.dbDemand = new DemandDB(demandDb);
    this.dbCycle = new CycleDB(cycleDb);
    this.dbRoom = new RoomDB(roomDb);
    this.dbResult = new ResultDB(resultDb);
    this.dbHotel = new HotelDB(hotelDb);
    this.dbConfig = new ConfigDB(configDb);
    this.dbRevenue = new RevenueDB('./data/revenueDB.json');
    this.dbHotelCosts = new HotelCostsDB('./data/costsDB.json'); // indexes

    this.INDXSTANDARTROOMPRICE = 0;
    this.INDXBETTERROOMPRICE = 1;
    this.INDXPROVISIONOTA = 2;
    this.INDXSALEFORCOMPANYCLIENTS = 3;
    this.INDXSALEFORCOMPANYCLIENTSWITHACTION = 4;
    this.INDXSALECKINDIVIDUAL = 5;
    this.INDXSALECKGROUP = 6;
    this.INDXGRATUITE = 7;
    this.INDXADVANCENONREF = 8;
    this.INDXRELEASEPROADVANCE = 9;
    this.INDXTOTALROOMSCOUNT = 10;
    this.INDXROOMSSOLD = 11;
    this.INDXROOMSREMAIN = 12;
    this.INDXSOLDSTANDARTROOM = 13;
    this.INDXSOLDSTANDARTROOMREMAIN = 14;
    this.INDXSOLDBETTERROOM = 15;
    this.INDXSOLDBETTERROOMREMAIN = 16;
    this.INDXONLINEOFFERSTANDARTROOM = 17;
    this.INDXONLINEOFFERBETTERROOM = 18;
    this.INDXCLOSETOARRIVAL = 19;
    this.INDXCLOSETODEPARTURE = 20;
    this.INDXMINIMUMDAYOFSTAY = 21;
  }
  /**
   * 
   * @param {number} hotelId 
   * @param {object} date { month, year }
   * @param {array} cycle 
   */


  _createClass(Kalkulin, [{
    key: "saveCycle",
    value: function saveCycle(hotelId, date, cycle) {
      this.dbCycle.save(hotelId, date, cycle);
    }
    /**
     * 
     * @param {number} hotelId number
     * @param {object} date { month, year }
     * 
     * @returns {Array} cycle
     */

  }, {
    key: "getCycle",
    value: function getCycle(hotelId, date) {
      var cycle = this.dbCycle.get(hotelId, date);

      if (cycle) {
        return cycle.cycle;
      }

      cycle = this.generateTableForMonth(hotelId, date);
      this.saveCycle(hotelId, date, cycle);
      return cycle;
    } // Settings

  }, {
    key: "setDailySettings",
    value: function setDailySettings(userId, day, month, year, settings) {
      var list = this.getPlayerList(userId, month, year);
      var arr = [this.INDXSTANDARTROOMPRICE, this.INDXBETTERROOMPRICE, this.INDXPROVISIONOTA, this.INDXSALEFORCOMPANYCLIENTS, this.INDXSALEFORCOMPANYCLIENTSWITHACTION, this.INDXSALECKINDIVIDUAL, this.INDXSALECKGROUP, this.INDXGRATUITE, this.INDXADVANCENONREF, this.INDXRELEASEPROADVANCE, this.INDXONLINEOFFERSTANDARTROOM, this.INDXONLINEOFFERBETTERROOM, this.INDXCLOSETOARRIVAL, this.INDXCLOSETODEPARTURE, this.INDXMINIMUMDAYOFSTAY];
      var i = 0;

      for (var setting in settings) {
        if (settings[setting] != null || settings[setting] != undefined) {
          list[day][arr[i]] = settings[setting];
        }

        i += 1;
      }

      this.saveList(userId, month, year, list);
    }
    /**
     * Add rooms from demand to the cycle
     * 
     * @param {object} demand
     */

  }, {
    key: "addReservationToCycle",
    value: function addReservationToCycle(demand) {
      var startDate = demand.startDate;
      var cycle = this.getCycle(demand.hotelId, startDate);
      var actualDay = demand.startDate.day;

      for (var i = 0; i < demand.period; i++) {
        cycle[actualDay][this.INDXROOMSSOLD] += demand.numberOfRooms;
        cycle[actualDay][this.INDXROOMSREMAIN] -= demand.numberOfRooms;

        if (demand.requirements.standardRoom === true) {
          // standard room
          console.log('standard room');
          cycle[actualDay][this.INDXSOLDSTANDARTROOM] += demand.numberOfRooms;
          cycle[actualDay][this.INDXSOLDSTANDARTROOMREMAIN] -= demand.numberOfRooms;
        } else {
          // premium
          console.log('premium room');
          cycle[actualDay][this.INDXSOLDBETTERROOM] += demand.numberOfRooms;
          cycle[actualDay][this.INDXSOLDBETTERROOMREMAIN] -= demand.numberOfRooms;
        }

        actualDay += 1;

        if (actualDay >= cycle.length) {
          this.saveCycle(demand.hotelId, startDate, cycle);
          startDate = this.getNextMonth(startDate);
          cycle = this.getCycle(demand.hotelId, startDate);
          actualDay = 0;
        }
      }

      this.saveCycle(demand.hotelId, startDate, cycle);
    } // Demand ##############################################################

    /**
     * Find demand
     *
     * @param {number} id    id of demand
     *
     * @return {demand} return demand or null
     */

  }, {
    key: "getDemand",
    value: function getDemand(id) {
      return this.dbDemand.get(id);
    }
    /**
     * Find all demands for hotel
     *
     * @param {number} id    id of GroupDemand
     *
     * @return {GroupDemand} return GroupDemands.
     */

  }, {
    key: "getAllDemands",
    value: function getAllDemands(hotelId) {
      return this.dbDemand.getAllDemands(hotelId);
    }
    /**
     * Accept demand and save it, call method for updating cycle
     *
     * @param {number} hotelId   
     * @param {demnad} demand    demand
     *
     * @return {demand} return updated demand
     */

  }, {
    key: "acceptDemand",
    value: function acceptDemand(hotelId, demand) {
      if (demand.accepted) {
        console.log('Demand is already accepted.');
        return demand;
      }

      demand.hotelId = hotelId;
      demand.accepted = true;
      this.saveDemand(demand);
      this.addReservationToCycle(demand);
      return demand;
    }
    /**
     * Save Demand or update
     *
     * @param {Demand} demand     demand
     */

  }, {
    key: "saveDemand",
    value: function saveDemand(demand) {
      this.dbDemand.save(demand);
    }
    /**
     * Find accepted demands in month
     *
     * @param {number} hotelId  id of hotel
     * @param {object} date     {month, year}
     *
     * @return {array}          array of all accepted demands in month.
     */

  }, {
    key: "getAcceptedDemands",
    value: function getAcceptedDemands(hotelId, date) {
      return this.dbDemand.getAcceptedDemands(hotelId, date);
    }
    /**
     * Find accepted demands in selected day
     *
     * @param {number} hotelId    hotel ID
     * @param {object} date       { day, month, year }
     *
     * @return {array}
     */

  }, {
    key: "getAcceptedDemandsInDay",
    value: function getAcceptedDemandsInDay(hotelId, date) {
      var demands = this.getAcceptedDemands(hotelId, date);
      var actualDemands = demands.filter(function (demand) {
        return demand.startDay <= date.day && demand.startDay + demand.getPeriod() >= day;
      });
      return actualDemands;
    }
    /**
     * Find all demands in selected month
     *
     * @param {number} hotelId  id of hotel
     * @param {number} date     { month, year }
     *
     * @return {array} return array of all demands.
     */

  }, {
    key: "getDemandsInMonth",
    value: function getDemandsInMonth(hotelId, date) {
      return this.dbDemand.getDemandsInMonth(hotelId, date);
    } // todo OTA demand

    /**
     * Generate and save random demnads
     *
     * @param {object} date       {day, month, year}
     * @param {number} range      number of demands
     *
     * @return {array}            return array of demands
     */

  }, {
    key: "generateDemands",
    value: function generateDemands(date, range) {
      var demands = [];

      for (var i = 0; i < range; i++) {
        var numberOfGuests = Math.floor(Math.random() * 5);
        var numberOfRooms = Math.floor(Math.random() * 2);
        var period = Math.floor(Math.random() * 10);
        var d = new Demand(null, null, numberOfGuests, numberOfRooms, date, date, period, false, null, //storno date
        0, 0, {
          maxPrice: 0,
          standardRoom: true,
          premiumRoom: false,
          connectedRoom: false,
          breakfast: false,
          fullBoard: false,
          alaCarte: false,
          kidsMenu: false,
          roomService: false,
          parking: false,
          busParking: false,
          shuttle: false,
          wellnes: false,
          anotherServices: false,
          conferenceSpace: false,
          raut: false,
          bar: false
        });
        this.saveDemand(d);
        demands.push(d);
      }

      return demands;
    } // Room ############################################################

    /**
     * Return Room type
     *
     * @return {float} type
     */

  }, {
    key: "getRoomTypes",
    value: function getRoomTypes() {
      var types = {
        standard: 0.7,
        premium: 1
      };
      return types;
    } // Hotel ############################################################

    /**
     * Find Hotel configuration by hotel id in date
     *
     * @param {number} id       id of user
     * @param {object} date     {month, year}
     *
     * @return {Hotel}          return player's Hotel.
     */

  }, {
    key: "getHotel",
    value: function getHotel(hotelId, date) {
      return this.dbHotel.get(hotelId, date);
    }
    /**
     * Save or update Hotel
     *
     * @param {Hotel} hotel     Hotel
     *
     * @return {Hotel}         return Hotel.
     */

  }, {
    key: "saveHotel",
    value: function saveHotel(hotel) {
      return this.dbHotel.save(hotel);
    } //Game config #######################################################

  }, {
    key: "saveConfig",
    value: function saveConfig(config) {
      this.dbConfig.save(config);
    }
  }, {
    key: "getConfig",
    value: function getConfig() {
      return this.dbConfig.get();
    } // Guests #######################################################

    /**
     * Calculate average number of guests in month
     *
     * @param {number} userId     user ID
     * @param {number} month      month
     * @param {number} year       year
     *
     * @return {number}           average number of guests in month
     */

  }, {
    key: "getAverageNumberOfGuestsInMonth",
    value: function getAverageNumberOfGuestsInMonth(userId, month, year) {
      var demands = this.getAcceptedDemands(userId, month, year);
      var days = this.getDaysInMonth(month); //todo days in selected month

      var guests = 0;
      demands.forEach(function (demand) {
        guests += demand.numberOfGuests;
      });
      return guests / days;
    }
    /**
     * Calculate number of guests in month without dinner
     *
     * @param {number} userId     user ID
     * @param {number} month      month
     * @param {number} year       year
     *
     * @return {number}           average number of guests in month
     */

  }, {
    key: "getNumberOfGuestsWithNoDinnerInMonth",
    value: function getNumberOfGuestsWithNoDinnerInMonth(userId, month, year) {
      var demands = this.getAcceptedDemands(userId, month, year);
      var guestsWithNoDinner = 0;
      demands.forEach(function (demand) {
        if (demand.getWithFood()) {
          //todo no dinner == withFood ???
          guestsWithNoDinner += demand.getNumberOfGuests();
        }
      });
      return guestsWithNoDinner;
    } // Hotel costs model ############################################

  }, {
    key: "hotelCostsTypes",
    value: function hotelCostsTypes() {
      return {
        hr: 1,
        material: 2,
        equip: 3,
        promotion: 4,
        other: 5,
        fix: 6
      };
    }
    /*
     * Save/update hotel costs
     *
     * @param {HotelCosts} costs    costs
     *
     * @return {HotelCosts}         HotelCosts
     */

  }, {
    key: "saveHotelCosts",
    value: function saveHotelCosts(costs) {
      var cost = this.dbHotelCosts.save(costs);

      if (!cost) {
        return null;
      }

      return new HotelCosts(cost.id, cost.userId, cost.costs, cost.day, cost.month, cost.year, cost.type);
    }
    /*
     * Find all costs for hotel in month
     *
     * @param {number} userId       user ID
     * @param {number} month        month
     * @param {number} year         year
     *
     * @return {array}              all costs
     */

  }, {
    key: "getHotelCostsInMonth",
    value: function getHotelCostsInMonth(userId, month, year) {
      var costs = this.dbHotelCosts.getPlayerCostsInMonth(userId, month, year);
      var list = costs.map(function (cost) {
        return new HotelCosts(cost.id, cost.userId, cost.costs, cost.day, cost.month, cost.year, cost.type);
      });
      return list;
    } // Hotel costs ##################################################

    /*
     * Calculate costs for hotel
     *
     * @param {number} userId           user ID
     *
     * @return {number} monthly costs   all costs
     */

  }, {
    key: "calculateMonthlyHotelCosts",
    value: function calculateMonthlyHotelCosts(userId) {
      //todo vse jednou za mesic nebo za den?
      var monthlyFixedCosts = this.calculateMonthlyFixedCosts(userId);
      var days = this.getDaysInMonth();
      var dailyDirectCosts = 0;

      for (var i = 0; i < days; i++) {
        dailyDirectCosts += this.calculateDailyDirectCosts(userId, i); //todo jednotlivy den
      }

      return monthlyFixedCosts + dailyDirectCosts; //todo mesix asi nebude podle date
    }
  }, {
    key: "calculateMonthlyFixedCosts",
    value: function calculateMonthlyFixedCosts(hotelId, date) {
      var hrCosts = this.calculateHRCosts(hotelId, date);
      var equipCosts = this.calculateEquipCosts(hotelId, date);
      var promoCosts = this.calculatePromoCosts(hotelId, date);
      var otherServicesCosts = this.calculateOtherServicesCosts(hotelId, date);
      var otherFixedCosts = this.calculateFixedCosts(hotelId, date);
      return hrCosts + equipCosts + promoCosts + otherServicesCosts + otherFixedCosts;
    }
  }, {
    key: "calculateDailyDirectCosts",
    value: function calculateDailyDirectCosts(userId, day, month, year) {
      var matCosts = this.calculateMatCosts(userId, day, month, year);
      var oTaCommission = this.calculateOtaCommission(userId);
      return matCosts + oTaCommission;
    }
  }, {
    key: "calculateMatCostsAS",
    value: function calculateMatCostsAS(hotelId, date) {
      //Denní náklady na materiál ubytovacího úseku za jednu obsazenou pokojonoc
      var config = this.getConfig();
      var roomMatCostsAsStandard = config.roomQualityStandardCost;
      var roomMatCostsAsPremium = config.roomQualityPremiumCost;
      var table = this.getCycle(hotelId, date);
      var dnorStandard = table[date.day][this.INDXSOLDSTANDARTROOM]; //Daily Number of Occupied Rooms v danem dni

      var dnorPremium = table[date.day][this.INDXSOLDBETTERROOM];
      return roomMatCostsAsStandard * dnorStandard + roomMatCostsAsPremium * dnorPremium;
    }
  }, {
    key: "calculateMatCostsFB",
    value: function calculateMatCostsFB(hotelId, date) {
      var hotel = this.getHotel(hotelId, date);
      var demands = this.getAcceptedDemandsInDay(hotelId, date);
      var dNAQ = 0; //Počet ubytovaných osob v daném dni

      var dNAGQD = 0; //Počet ubytovaných skupinových osob s polopenzí v daném dni with dinner

      var dNAQND = 0; //Počet ubytovaných osob bez polopenze v daném dni no dinner

      demands.forEach(function (demand) {
        dNAQ += demand.getNumberOfGuests();

        if (demand.withFood()) {
          if (demand.getGroup()) {
            dNAGQD += demand.getNumberOfGuests();
          }
        } else {
          dNAQND += demand.getNumberOfGuests();
        }
      });
      var materialQuality = this.getPlayerFoodMaterial(hotelId);
      var breakMatCostsFB = 0; //todo // Denní náklady na materiál snídaní za jednoho ubytovaného hosta. zadano spravcem

      var groupDinnMatCostsFB = 0; //todo

      var alCMatCostsFB = 0; //todo

      var barMatCostsFB = 0; //todo

      switch (materialQuality.getLevel()) {
        case this.getLevels().economy:
          breakMatCostsFB = 50;
          groupDinnMatCostsFB = 50;
          alCMatCostsFB = 50;
          barMatCostsFB = 50;
          break;

        case this.getLevels().standard:
          breakMatCostsFB = 50;
          groupDinnMatCostsFB = 50;
          alCMatCostsFB = 50;
          barMatCostsFB = 50;
          break;

        case this.getLevels().premium:
          breakMatCostsFB = 50;
          groupDinnMatCostsFB = 50;
          alCMatCostsFB = 50;
          barMatCostsFB = 50;
          break;

        default:
          break;
      }

      return breakMatCostsFB * dNAQ + groupDinnMatCostsFB * dNAGQD + alCMatCostsFB * dNAQND * 0.3 + barMatCostsFB * dNAQ * 0.5;
    }
  }, {
    key: "calculateMatCosts",
    value: function calculateMatCosts(hotelId, date) {
      var matCostsAS = this.calculateMatCostsAS(hotelId, date);
      var matCostsFB = this.calculateMatCostsFB(hotelId, date);
      return matCostsAS + matCostsFB;
    }
  }, {
    key: "calculateOtaCommission",
    value: function calculateOtaCommission(userId) {
      var otaComRate = 0; //Procento odvodů OTA v den nákupu
      //pocet hostu ubytovanych pres ota - todo demand type ota, firma, akce...

      return 0;
    }
    /*
     * Calculate daily costs
     *
     * @param {number} userId     user ID
     *
     * @return {number}           daily costs
     */

  }, {
    key: "calculateDailyCosts",
    value: function calculateDailyCosts(userId) {
      var monthlyFixedCosts = this.calculateMonthlyFixedCosts(userId);
      var days = this.getDaysInMonth();
      var dailyDirectCosts = this.calculateDailyDirectCosts(userId);
      return monthlyFixedCosts / days + dailyDirectCosts;
    }
    /*
     * Monthly costs for all HR
     *
     * @param {number} userId     user ID
     *
     * @return {number}           costs for all employes
     */

  }, {
    key: "calculateHRCosts",
    value: function calculateHRCosts(hotelId, date) {
      var costEmplRec = this.calculateCostEmplRec(hotelId, date); //ok spojit

      var costEmplHouse = this.calculateCostEmplHouse(hotelId, date); //ok

      var costEmplBreak = this.calculateCostEmplBreak(hotelId, date); //ok

      var costEmplgDinn = this.calculateCostEmplGdinn(hotelId, date); //ok

      var costEmplalcDinn = this.calculateCostEmplalcDinn(hotelId, date);
      var costEmpllb = this.calculateCostEmplLb(hotelId, date);
      var costEmplOthers = this.calculateCostEmplOthers(hotelId, date);
      return costEmplRec + costEmplHouse + costEmplBreak + costEmplgDinn + costEmplalcDinn + costEmpllb + costEmplOthers;
    }
    /*
     * Monthly costs for reception empl
     *
     * @param {number} userId     user ID
     *
     * @return {number}           costs
     */

  }, {
    key: "calculateCostEmplRec",
    value: function calculateCostEmplRec(hotelId, date) {
      var config = this.getConfig();
      var hotel = this.getHotel(hotelId, date);
      var recJunior = hotel.hr.as.reception.junior;
      var recSenior = hotel.hr.as.reception.senior;
      var costEmplRecJunior = config.juniorReceptionWage; // 13500; //const? todo

      var costEmplRecSenior = config.seniorReceptionWage; //34000; //const?

      return (recJunior * costEmplRecJunior + recSenior * costEmplRecSenior) * 4;
    }
    /*
     * Monthly costs for housekeeping empl
     *
     * @param {number} userId     user ID
     *
     * @return {number}           costs
     */

  }, {
    key: "calculateCostEmplHouse",
    value: function calculateCostEmplHouse(hotelId, date) {
      var config = this.getConfig();
      var hotel = this.getHotel(hotelId, date);
      var houseJunior = hotel.hr.as.housekeeping.junior;
      var houseSenior = hotel.hr.as.housekeeping.senior;
      var costEmplHouseJunior = config.juniorRoomServiceWage; // 13500; //const? todo

      var costEmplHouseSenior = config.seniorRoomServiceWage; // 27000; //const?

      return (houseJunior * costEmplHouseJunior + houseSenior * costEmplHouseSenior) * 2;
    }
    /*
     * Monthly costs for F&B breakfast empl
     *
     * @param {number} userId     user ID
     *
     * @return {number}           costs
     */

  }, {
    key: "calculateCostEmplBreak",
    value: function calculateCostEmplBreak(hotelId, date) {
      var config = this.getConfig();
      var hotel = this.getHotel(hotelId, date);
      var brCookJunior = hotel.hr.fb.breakfast.kitchen.junior;
      var brCookSenior = hotel.hr.fb.breakfast.kitchen.senior;
      var brAssistJunior = hotel.hr.fb.breakfast.kitchen.auxiliary;
      var brServiceJunior = hotel.hr.fb.breakfast.restaurant.junior;
      var brServiceSenior = hotel.hr.fb.breakfast.restaurant.senior;
      var costEmplbrCookJunior = config.juniorBreakfastChefWage;
      var costEmplbrCookSenior = config.seniorBreakfastChefWage;
      var costEmplbrAssistJunior = config.helpBreakfastWage;
      var costEmplbrServiceJunior = config.juniorBreakfastWaiterWage;
      var costEmplbrServiceSenior = config.seniorBreakfastWaiterWage;
      return brCookJunior * costEmplbrCookJunior + brCookSenior * costEmplbrCookSenior + brAssistJunior * costEmplbrAssistJunior + brServiceJunior * costEmplbrServiceJunior + brServiceSenior * costEmplbrServiceSenior;
    }
    /*
     * Monthly costs for F&B group dinner empl
     *
     * @param {number} userId     user ID
     *
     * @return {number}           costs
     */

  }, {
    key: "calculateCostEmplGdinn",
    value: function calculateCostEmplGdinn(hotelId, date) {
      var config = this.getConfig();
      var hotel = this.getHotel(hotelId, date);
      var gDinnCookJunior = hotel.hr.fb.dinner.kitchen.junior;
      var gDinnCookSenior = hotel.hr.fb.dinner.kitchen.senior;
      var gDinnAssistJunior = hotel.hr.fb.dinner.kitchen.auxiliary;
      var gDinnServiceJunior = hotel.hr.fb.dinner.restaurant.junior;
      var gDinnServiceSenior = hotel.hr.fb.dinner.restaurant.senior;
      var costEmplgdinnCookJunior = config.juniorDinnerChefWage;
      var costEmplgdinnCookSenior = config.seniorDinnerChefWage;
      var costEmplgdinnAssistJunior = config.helpDinnerWage;
      var costEmplgdinnServiceJunior = config.juniorDinnerWaiterWage;
      var costEmplgdinnServiceSenior = config.seniorDinnerWaiterWage;
      return gDinnCookJunior * costEmplgdinnCookJunior + gDinnCookSenior * costEmplgdinnCookSenior + gDinnAssistJunior * costEmplgdinnAssistJunior + gDinnServiceJunior * costEmplgdinnServiceJunior + gDinnServiceSenior * costEmplgdinnServiceSenior;
    }
    /*
     * Monthly costs for ala carte empl
     *
     * @param {number} userId     user ID
     *
     * @return {number}           costs
     */

  }, {
    key: "calculateCostEmplalcDinn",
    value: function calculateCostEmplalcDinn(hotelId, date) {
      var guests = this.getNumberOfGuestsWithNoDinnerInMonth(hotelId, date.month, date.year - 1); //průměrný denní počet ubytovaných osob bez polopenze ve stejném měsíci předchozího roku

      var days = this.getDaysInMonth();
      var avgDNAQND = guests / days;
      var costEmplGdinnService = 100; //todo 𝐶𝑜𝑠𝑡𝐸𝑚𝑝𝑙𝑔𝑑𝑖𝑛𝑛𝑠𝑒𝑟𝑣𝑖𝑐𝑒 neni v docs

      return avgDNAQND * 0.3 * 0.05 * costEmplGdinnService;
    }
    /*
     * Monthly costs for loby bar empl
     *
     * @param {number} userId     user ID
     *
     * @return {number}           costs
     */

  }, {
    key: "calculateCostEmplLb",
    value: function calculateCostEmplLb(hotelId, date) {
      var costEmplLbJunior = 50; //todo neni v docs

      var costEmplLbSenior = 100; //todo neni v docs

      var avgDNAQ = this.getAverageNumberOfGuestsInMonth(hotelId, date.month, date.year - 1); // todo pokud neni tak 0? //average Daily Number of Accommodated Quests in the same month of the previous year

      if (avgDNAQ * 0.5 > 30) {
        return costEmplLbSenior + costEmplLbJunior; //todo jen senior asi blbe vzorec
      } else {
        return costEmplLbSenior;
      }
    }
    /*
     * Calculate costs for other employes
     *
     * @param {number} userId     user ID
     *
     * @return {number}           costs
     */

  }, {
    key: "calculateCostEmplOthers",
    value: function calculateCostEmplOthers(userId) {
      //todo kolik a kolik budou stat
      var hotCap = this.getNumberOfRooms(userId);
      var emplCost = 100; //todo neni v docs

      if (hotCap > 50) {
        return emplCost * 10;
      } else if (hotCap > 100) {
        return emplCost * 13;
      } else if (hotCap > 200) {
        return emplCost * 20;
      } else {
        return emplCost * 7;
      }
    }
    /*
     * Calculate costs for hotel inovation
     *
     * @param {number} userId     user ID
     *
     * @return {number}           costs
     */

  }, {
    key: "calculateEquipCosts",
    value: function calculateEquipCosts(hotelId, date) {
      var hotel = this.getHotel(hotelId, date);
      var revenues = this.getPlayerRevenuesInMonth(hotelId, this.getLastMonth(date).month, this.getLastMonth(date).year);
      var tRev = 0; //Total Revenue in a previous month

      revenues.forEach(function (revenue) {
        tRev += revenue.getRevenue();
      });
      return hotel.facilityInvestment * tRev;
    }
    /*
     * Calculate promotion costs for hotel
     *
     * @param {number} userId     user ID
     *
     * @return {number}           promo costs
     */

  }, {
    key: "calculatePromoCosts",
    value: function calculatePromoCosts(hotelId, date) {
      var hotel = this.getHotel(hotelId, date);
      var arPromoCosts = hotel.promotionInvestment;
      var daysInMonth = this.getDaysInMonth(date);
      var hotCap = hotel.rooms.standard + hotel.rooms.premium;
      return arPromoCosts * hotCap * daysInMonth;
    }
    /*
     * Calculate costs for services
     *
     * @param {number} userId     user ID
     *
     * @return {number}           promo costs
     */

  }, {
    key: "calculateOtherServicesCosts",
    value: function calculateOtherServicesCosts(userId) {
      // todo other services
      return 0;
    }
    /*
     * Calculate fixed costs
     *
     * @param {number} userId     user ID
     *
     * @return {number}           fixed costs
     */

  }, {
    key: "calculateFixedCosts",
    value: function calculateFixedCosts(userId) {
      // todo fixed costs
      return 0; // nastavuje spravce
    } // Revenue model ######################################################

  }, {
    key: "getRevenueTypes",
    value: function getRevenueTypes() {
      return {
        acc: 1,
        oth: 2
      };
    }
    /*
     * Find players revenues
     *
     * @param {number} userId   id of player
     *
     * @return {array}          hotel revenues
     */

  }, {
    key: "getPlayerRevenues",
    value: function getPlayerRevenues(userId) {
      var revenues = this.dbRevenue.getPlayerHotelRevenues(userId);
      var list = revenues.map(function (revenue) {
        return new Revenue(revenue.id, revenue.userId, revenue.revenue, revenue.day, revenue.month, revenue.year, revenue.type);
      });
      return list;
    }
    /*
     * Find player revenues for month
     *
     * @param {number} userId   id of player
     * @param {number} month
     * @param {number} year
     *
     * @return {array}          revenue
     */

  }, {
    key: "getPlayerRevenuesInMonth",
    value: function getPlayerRevenuesInMonth(userId, month, year) {
      var revenues = this.dbRevenue.getPlayerRevenuesInMonth(userId, month, year);
      var list = revenues.map(function (revenue) {
        return new Revenue(revenue.id, revenue.userId, revenue.revenue, revenue.day, revenue.month, revenue.year, revenue.type);
      });
      return list;
    }
    /*
     * Save/update revenue
     *
     * @param {Revenue} revenue   revenue
     *
     * @return {Revenue}          revenue
     */

  }, {
    key: "saveRevenue",
    value: function saveRevenue(rev) {
      var revenue = this.dbRevenue.save(rev);
      return new Revenue(revenue.id, revenue.userId, revenue.revenue, revenue.day, revenue.month, revenue.year, revenue.type);
    } // Revenue ######################################################

    /*
     * Calculate monthly revenue done
     *
     * @param {number} userId     user ID
     * @param {number} month      month
     * @param {number} year       year
     *
     * @return {number}           revenue done
     */

  }, {
    key: "calculateMonthlyRevDone",
    value: function calculateMonthlyRevDone(userId, month, year) {
      // dokazu spocitat pocet dni?
      //?month
      return this.calculateMonthlyAccRevDone(userId, month, year) + this.calculateMonthlyOthRevDone(userId, month, year);
    }
    /*
     * Calculate monthly acc revenue done
     *
     * @param {number} userId     user ID
     * @param {number} month      month
     * @param {number} year       year
     *
     * @return {number}           acc revenue done
     */

  }, {
    key: "calculateMonthlyAccRevDone",
    value: function calculateMonthlyAccRevDone(userId, month, year) {
      var days = this.daysInMonth();
      var monthlyAccRevDone = 0;

      for (var i = 0; i < days; i++) {
        var dailyAccRevDone = 1; //todo neni v docs

        monthlyAccRevDone += dailyAccRevDone;
      }

      return monthlyAccRevDone;
    }
    /*
     * Calculate oth revenue done
     *
     * @param {number} userId     user ID
     * @param {number} month      month
     * @param {number} year       year
     *
     * @return {number}           oth revenue
     */

  }, {
    key: "calculateMonthlyOthRevDone",
    value: function calculateMonthlyOthRevDone(userId, month, year) {
      var days = this.daysInMonth();
      var monthlyOthRevDone = 0;

      for (var i = 0; i < days; i++) {
        var dailyOthRevDone = 1; //todo neni v docs

        monthlyOthRevDone += dailyAccRevenue;
      }

      return monthlyOthRevDone;
    }
    /*
     * Calculate monthly revenue sold
     *
     * @param {number} userId     user ID
     * @param {number} month      month
     * @param {number} year       year
     *
     * @return {number}           monthly revenue
     */

  }, {
    key: "calculateMonthlyRevSold",
    value: function calculateMonthlyRevSold(userId, month, year) {
      //?month
      return this.calculateMonthlyAccRevSold(userId, month, year) + this.calculateMonthlyOthRevSold(userId, month, year);
    }
    /*
     * Calculate monthly acc revenue
     *
     * @param {number} userId     user ID
     * @param {number} month      month
     * @param {number} year       year
     *
     * @return {number}           revenue
     */

  }, {
    key: "calculateMonthlyAccRevSold",
    value: function calculateMonthlyAccRevSold(userId, month, year) {
      var days = this.daysInMonth();
      var monthlyAccRevSold = 0;

      for (var i = 0; i < days; i++) {
        var _dailyAccRevenue = 1; //todo neni v docs

        monthlyAccRevSold += _dailyAccRevenue;
      }

      return monthlyAccRevSold;
    }
    /*
     * Calculate monthly other revenue
     *
     * @param {number} userId     user ID
     * @param {number} month      month
     * @param {number} year       year
     *
     * @return {number}           revenue
     */

  }, {
    key: "calculateMonthlyOthRevSold",
    value: function calculateMonthlyOthRevSold(userId, month, year) {
      var days = this.daysInMonth();
      var monthlyOthRevSold = 0;

      for (var i = 0; i < days; i++) {
        var dailyOthRevSold = 1; //todo neni v docs

        monthlyOthRevSold += dailyOthRevSold;
      }

      return monthlyOthRevSold;
    } // Calculating ##############################################################

    /**
     * 
     * @param {number} hotelId 
     * @param {object} date { month, year }
     * 
     * @returns {object}
     */

  }, {
    key: "getResultsFromDb",
    value: function getResultsFromDb(hotelId, date) {
      var r = this.dbResult.getPlayersResults(hotelId, date);

      if (r) {
        return r.value;
      } else {
        return null;
      }
    }
    /**
     * 
     * @param {number} hotelId 
     * 
     * @returns {array}
     */

  }, {
    key: "getAllPlayerResults",
    value: function getAllPlayerResults(hotelId) {
      return this.dbResult.getAllPlayersResults(hotelId);
    }
    /**
     * 
     * @param {number} hotelId 
     * @param {object} date { month, year }
     */

  }, {
    key: "getResults",
    value: function getResults(hotelId, date) {
      var eq = this.calculateEquipQuality(hotelId, date);
      var mq = this.calculateMaterialQuality(hotelId, date);
      var hr = this.calculateHRPotential(hotelId, date);
      var oa = this.calculateOfferAtract(hotelId, date); // todo

      var pp = this.calculateProductsPortfolio(hotelId, date);
      var oac = this.calculateOfferAccesibility(hotelId, date);
      var gs = this.calculateQuestSatisfaction(hotelId, date); // todo

      var pq = this.calculateProductsQuality(hotelId, date);
      return {
        equipQuality: eq,
        materialQuality: mq,
        hrPotential: hr,
        offerAtract: oa,
        productsPortfolio: pp,
        offerAccesibility: oac,
        guestSatisfaction: gs,
        productsQuality: pq
      };
    }
  }, {
    key: "saveResults",
    value: function saveResults(hotelId, date) {
      // todo ukladat?
      var results = this.getResults(userId);
      var lastMonth = this.getLastMonth(date);
      this.dbResult.save(hotelId, lastMonth, results);
    }
  }, {
    key: "generateTableForMonth",
    value: function generateTableForMonth(hotelId, date) {
      var hotel = this.getHotel(hotelId, date);
      var days = this.getDaysInMonth(date);
      var day = [99, 119, 15, 10, 15, 25, 30, 21, -20, 21, 80, //hotel.rooms.standard + hotel.rooms.premium,
      0, 80, 0, 60, //hotel.rooms.standard,
      0, 20, //hotel.rooms.premium,
      20, 10, 0, 0, 0];
      var month = [];

      for (var i = 0; i < days; i++) {
        month.push(day);
      }

      return month;
    }
  }, {
    key: "getGreenTableRooms",
    value: function getGreenTableRooms(hotelId, date) {
      var _this = this;

      var cycle = this.getCycle(hotelId, date);
      var list1 = [];
      var list2 = [];
      cycle.forEach(function (day) {
        list1.push([day[_this.INDXTOTALROOMSCOUNT], day[_this.INDXROOMSSOLD], day[_this.INDXROOMSREMAIN]]);
        list2.push([day[_this.INDXSOLDSTANDARTROOM], day[_this.INDXSOLDSTANDARTROOMREMAIN], day[_this.INDXSOLDBETTERROOM], day[_this.INDXSOLDBETTERROOMREMAIN]]);
      });
      return {
        roomStatus: list1,
        detailRoomStatus: list2
      };
    }
  }, {
    key: "calculateEquipQuality",
    value: function calculateEquipQuality(hotelId, date) {
      var hotel = this.getHotel(hotelId, date);
      var allResults = this.getResultsFromDb(hotelId, date);
      var equipQualityLastMonth = allResults.equipQuality;

      if (!equipQualityLastMonth) {
        equipQualityLastMonth = 0.8; //todo constant pokud neni hodnota
      }

      var ocrLastMonth = this.occupancyRate(hotelId, date); // Průměrná měsíční Obsazenost hotelu (OCcupency Rate);

      if (!ocrLastMonth) {
        ocrLastMonth = 0.5; //todo
      }

      var equipQuality = equipQualityLastMonth - 0.03 * ocrLastMonth + hotel.facilityInvestment;

      if (equipQuality > 1) {
        equipQuality = 1;
      }

      return equipQuality;
    }
  }, {
    key: "calculateMaterialQuality",
    value: function calculateMaterialQuality(hotelId, date) {
      var config = this.getConfig();
      var hotel = this.getHotel(hotelId, date);
      return config.cMQAS * hotel.roomMaterialQuality + config.cMQFB * hotel.foodMaterialQuality;
    }
  }, {
    key: "calculateHRPotential",
    value: function calculateHRPotential(hotelId, date) {
      var config = this.getConfig();
      var hrFb = this.getPotentialHrFb(hotelId, date); //todo infinity

      var hrAs = this.getPotentialHrAs(hotelId, date); //todo infinity

      return config.cHRAS * hrAs + config.cHRFB * hrFb;
    }
  }, {
    key: "getPotentialHrAs",
    value: function getPotentialHrAs(hotelId, date) {
      var config = this.getConfig();
      var hotel = this.getHotel(hotelId, date);
      var cycle = this.getCycle(hotelId, date);
      var daysInMonth = this.getDaysInMonth(date);
      var occupiedDays = 0;

      for (var i = 0; i < cycle.length; i++) {
        occupiedDays += cycle[i][this.INDXROOMSSOLD];
      }

      var emplRecJun = hotel.hr.as.reception.junior * this.getHRLevels().junior;
      var emplRecSen = hotel.hr.as.reception.senior * this.getHRLevels().senior;
      var emplRec = emplRecJun + emplRecSen;
      var anorpd = occupiedDays / daysInMonth; // Average Number of Occupied Rooms Per Day

      var emplRecOpt = anorpd * 0.025; // optimální míra výkonnosti pracovníků recepce potřebná pro obsloužení hostů na obsazených pokojích

      var potentialHrAsRec = emplRec / emplRecOpt;
      var emplHousJun = hotel.hr.as.housekeeping.junior * this.getHRLevels().junior;
      var emplHousSen = hotel.hr.as.housekeeping.senior * this.getHRLevels().senior;
      var emplHous = emplHousJun + emplHousSen;
      var emplHousOpt = anorpd * 0.071;
      var potentialHrAsHous = emplHous / emplHousOpt;
      return config.cHRASRec * potentialHrAsRec + config.cHRASHous * potentialHrAsHous;
    } // str 9, 10

  }, {
    key: "getPotentialHrFb",
    value: function getPotentialHrFb(hotelId, date) {
      //todo demandy
      var config = this.getConfig();
      var hotel = this.getHotel(hotelId, date);
      var daysInMonth = this.getDaysInMonth(date);
      var demandsLastMonth = this.getAcceptedDemands(hotelId, date);
      var accommodatedQuests = 0;

      for (var i = 0; i < demandsLastMonth.length; i++) {
        var period = demandsLastMonth[i].period;
        var firstDay = demandsLastMonth[i].startDate.day;

        if (period + firstDay < daysInMonth) {
          accommodatedQuests += demandsLastMonth[i].numberOfGuests * period;
        } else {
          accommodatedQuests += demandsLastMonth[i].numberOfGuests * (daysInMonth - firstDay);
        }
      }

      var anaqpd = accommodatedQuests / daysInMonth; //Average Number of Accommodated Group Quests Per Day

      var emplBreakKitchenOpt = anaqpd * 0.0425;
      var potentialHRBreakKitchen = (hotel.hr.fb.breakfast.kitchen.junior * this.getHRLevels().junior + hotel.hr.fb.breakfast.kitchen.senior * this.getHRLevels().senior) / emplBreakKitchenOpt;
      var emplBreakRestOpt = anaqpd * 0.025;
      var potentialHRBreakRest = (hotel.hr.fb.breakfast.restaurant.junior * this.getHRLevels().junior + hotel.hr.fb.breakfast.restaurant.senior * this.getHRLevels().senior) / emplBreakRestOpt;
      var potentialHRFBBreak = config.chrfbBreakKitchen * potentialHRBreakKitchen + config.chrfbBreakRest * potentialHRBreakRest;
      var groupDemandsLastMonth = demandsLastMonth; //todo poptavky s veceri

      var accommodatedGroupQuestsWithFood = 0;

      for (var _i = 0; _i < groupDemandsLastMonth.length; _i++) {
        if (groupDemandsLastMonth[_i].requirements.breakfast) {
          //todo
          var _period = groupDemandsLastMonth[_i].period;
          var _firstDay = groupDemandsLastMonth[_i].startDate.day;

          if (_period + _firstDay < daysInMonth) {
            accommodatedGroupQuestsWithFood += groupDemandsLastMonth[_i].numberOfGuests * _period;
          } else {
            accommodatedGroupQuestsWithFood += groupDemandsLastMonth[_i].numberOfGuests * (daysInMonth - _firstDay);
          }
        }
      }

      var anagqfpd = accommodatedGroupQuestsWithFood / daysInMonth; //Average Number of Accommodated Group Quests with Food Per Day

      var emplGroupKitchenOpt = anagqfpd * 0.0675;
      var potentialHRFBGroupKitchen = (hotel.hr.fb.dinner.kitchen.junior * this.getHRLevels().junior + hotel.hr.fb.dinner.kitchen.senior * this.getHRLevels().senior) / emplGroupKitchenOpt;
      var emplGroupRestOpt = anagqfpd * 0.06;
      var potentialHRFBGroupRest = (hotel.hr.fb.dinner.restaurant.junior * this.getHRLevels().junior + hotel.hr.fb.dinner.restaurant.senior + this.getHRLevels().senior) / emplGroupRestOpt;
      var potentialHRFBGroup = potentialHRFBGroupKitchen * config.chrfbGroupKitchen + potentialHRFBGroupRest * config.chrfbGroupRest;
      return config.chrfbBreak * potentialHRFBBreak + config.chrfbGroup * potentialHRFBGroup;
    }
  }, {
    key: "calculateProductsQuality",
    value: function calculateProductsQuality(hotelId, date) {
      var config = this.getConfig();
      var hrPotential = this.calculateHRPotential(hotelId, date);
      var matQuality = this.calculateMaterialQuality(hotelId, date);
      var equipQuality = this.calculateEquipQuality(hotelId, date);
      return config.cpqhr * hrPotential + config.cpqmq * matQuality + config.cpqeq * equipQuality;
    }
  }, {
    key: "calculateProductsPortfolio",
    value: function calculateProductsPortfolio(hotelId, date) {
      //todo
      var allServices = 16;
      var hotelEquipt = this.getHotel(hotelId, date).equipment;
      var hotelServices = 0;
      Object.keys(hotelEquipt).forEach(function (value) {
        if (hotelEquipt[value] == true) {
          hotelServices += 1;
        }
      });
      return hotelServices / allServices;
    }
  }, {
    key: "calculateOfferAtract",
    value: function calculateOfferAtract(hotelId, date) {
      var config = this.getConfig();
      var equipQuality = this.calculateEquipQuality(hotelId, date);
      var productsPortfolio = this.calculateProductsPortfolio(hotelId, date);
      return config.coaeq * equipQuality + config.coapp * productsPortfolio + config.coabl * config.brandLevel;
    }
  }, {
    key: "calculateQuestSatisfaction",
    value: function calculateQuestSatisfaction(hotelId, date) {
      //todo
      var productsQuality = this.calculateProductsQuality(hotelId, date);
      var overbookTolerance = 0; //hoste kteří nebyly v předchozím měsíci ubytováni z důvodů plné kapacitu hotelu

      var lastMonth = this.getLastMonth(date); //todo reservation...

      var allDemandsLastMonth = this.getDemandsInMonth(hotelId, lastMonth);
      var denyReservations = 0;

      for (var i = 0; i < allDemandsLastMonth.length; i++) {
        if (allDemandsLastMonth[i].accepted == false) {
          denyReservations += 1;
        }
      }

      var wqr = denyReservations / allDemandsLastMonth.length;

      if (wqr <= 0.02) {
        overbookTolerance = 1;
      } else {
        overbookTolerance = 1 - (wqr - 0.02) * 5;
      }

      if (overbookTolerance < 0) {
        overbookTolerance = 0;
      }

      return 5 * ((productsQuality + overbookTolerance) / 2);
    } // todo - neni vzorec

  }, {
    key: "calculateOfferAccesibility",
    value: function calculateOfferAccesibility(hotelId, date) {
      var hotel = this.getHotel(hotelId, date);
      var offerAccesibility = 1;
      var promotion = hotel.promotionInvestment;

      if (offerAccesibility > 1) {
        offerAccesibility = 1;
      }

      return offerAccesibility;
    }
  }, {
    key: "getLastMonth",
    value: function getLastMonth(date) {
      var lastMonth = date.month - 1; //todo fix

      var year = date.year;

      if (lastMonth <= -1) {
        lastMonth = 11;
        year -= 1;
      }

      return {
        month: lastMonth,
        year: year
      };
    }
  }, {
    key: "getNextMonth",
    value: function getNextMonth(date) {
      var nextMonth = date.month + 1;
      var year = date.year;

      if (nextMonth >= 12) {
        nextMonth = 0;
        year += 1;
      }

      return {
        month: nextMonth,
        year: year
      };
    }
  }, {
    key: "getHRPositions",
    value: function getHRPositions() {
      var positions = {
        restaurant: 1,
        kitchen: 2
      };
      return positions;
    }
  }, {
    key: "getHRPlaces",
    value: function getHRPlaces() {
      var places = {
        reception: 1,
        housekeeping: 2,
        breakfast: 3,
        dinner: 4
      };
      return places;
    }
  }, {
    key: "getHRTypes",
    value: function getHRTypes() {
      var types = {
        auxiliary: 1,
        junior: 2,
        senior: 3
      };
      return types;
    }
  }, {
    key: "getHRLevels",
    value: function getHRLevels() {
      return {
        junior: 0.7,
        senior: 1
      };
    }
  }, {
    key: "getLevels",
    value: function getLevels() {
      var levels = {
        economy: 0.5,
        standard: 0.7,
        premium: 1
      };
      return levels;
    }
  }, {
    key: "getDaysInMonth",
    value: function getDaysInMonth(date) {
      return new Date(date.year, date.month, 0).getDate();
    } //todo muze nastavit spravce
    // Measure functions #######################################################

  }, {
    key: "occupancyRate",
    value: function occupancyRate(hotelId, date) {
      var hotel = this.getHotel(hotelId, date);
      var rooms = hotel.rooms.standard + hotel.rooms.premium;
      var cycle = this.getCycle(hotelId, date);
      var soldRooms = 0;

      for (var i = 0; i < cycle.length; i++) {
        soldRooms += cycle[i][this.INDXROOMSSOLD];
      }

      return (rooms - soldRooms) / rooms * 100;
    }
  }, {
    key: "averageDailyRate",
    value: function averageDailyRate(hotelId, date) {
      var cycle = this.getPlayerList(userId, date.month, date.year);
      var today = 1;
      var totalSum = 0;
      var roomsCount = 0;

      for (var i = 0; i < today; i++) {
        totalSum += cycle[i][this.INDXSOLDSTANDARTROOM] * cycle[i][this.INDXSTANDARTROOMPRICE];
        totalSum += cycle[i][this.INDXSOLDBETTERROOM] * cycle[i][this.INDXBETTERROOMPRICE];
        roomsCount += cycle[i][this.INDXROOMSSOLD];
      }

      return totalSum / roomsCount;
    }
  }, {
    key: "calculateRevenue",
    value: function calculateRevenue(hotelId, date) {
      var config = this.getConfig();
      var hotel = this.getHotel(hotelId, date);
      var rooms = this.getNumberOfRooms(hotelId);
      var services = this.getPlayerRoomMaterial(hotelId);
      var foodServices = this.getPlayerFoodMaterial(hotelId);
      var days = this.getDaysInMonth(date);
      var sumRoom = 0;

      if (services.getLevel() == this.getLevels().standard) {
        sumRoom = config.getRoomQualityStandardCost() * rooms * days;
      } else {
        sumRoom = config.getRoomQualityPremiumCost() * rooms * days;
      }

      var sumFood = 0;

      if (foodServices.getLevel() == this.getLevels().economy) {
        sumFood = config.getFoodQualityEconomyAlaCarteCost();
        sumFood += config.getFoodQualityEconomyBarCost();
        sumFood += config.getFoodQualityEconomyBreakfastCost();
        sumFood += config.getFoodQualityEconomyDinnerCost();
      } else if (foodServices.getLevel() == this.getLevels().standard) {
        sumFood = config.getFoodQualityStandardAlaCarteCost();
        sumFood += config.getFoodQualityStandardBarCost();
        sumFood += config.getFoodQualityStandardBreakfastCost();
        sumFood += config.getFoodQualityStandardDinnerCost();
      } else {
        sumFood = config.getFoodQualityPremiumAlaCarteCost();
        sumFood += config.getFoodQualityPremiumBarCost();
        sumFood += config.getFoodQualityPremiumBreakfastCost();
        sumFood += config.getFoodQualityPremiumDinnerCost();
      }

      sumFood = sumFood * rooms * days;
      return {
        revenuePerAvalilableRoom: sumRoom / rooms,
        totalRevenuePerAvalilableRoom: (sumRoom + sumFood) / rooms
      };
    } //todo asi neni potreba

  }, {
    key: "countPriceForDemand",
    value: function countPriceForDemand(userId, demand) {
      var day = demand.getStartDay();
      var year = this.getActualYear();
      var period = demand.getPeriod();
      var month = demand.getStartMonth();
      var list = this.getPlayerList(userId, month, year);
      var rooms = this.getRoomsByDemand(demand);
      var roomsCount = rooms.length;
      var price = 0;
      var nextMonth = this.getNextMonth(date);

      for (var i = 0; i < period; i++) {
        if (day > 29) {
          day = 0;
          list = this.getPlayerList(userId, nextMonth.month, nextMonth.year);
        }

        if (demand.getRoomType() == this.getRoomTypes().standard) {
          price += list[day][this.INDXSTANDARTROOMPRICE] * roomsCount;
        } else {
          //premium
          price += list[day][this.INDXBETTERROOMPRICE] * roomsCount;
        }

        day += 1;
      }

      return price;
    }
  }]);

  return Kalkulin;
}();

module.exports = Kalkulin; // generování demnadu pro všechny + pak přiřadit tomu kdo vyhral
// monthly rev sold/done - co to je?, budu schopen zjistit pocet dni podle mesice?
// calculateCostEmplLb - podle hodnoty v minulem roce - co když žádná není? 0? - víc takových vzorců, není špatně vzorec?
//todo co kdyt nebude zaznam hotelu v budoucnu... datum atd z konfigurace

console.log(new Kalkulin().getResults(0, {
  month: 4,
  year: 2020
}));