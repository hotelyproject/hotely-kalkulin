"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var GroupDemand =
/*#__PURE__*/
function () {
  function GroupDemand() {
    var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var userId = arguments.length > 1 ? arguments[1] : undefined;
    var name = arguments.length > 2 ? arguments[2] : undefined;
    var numberOfGuests = arguments.length > 3 ? arguments[3] : undefined;
    var startDay = arguments.length > 4 ? arguments[4] : undefined;
    var startMonth = arguments.length > 5 ? arguments[5] : undefined;
    var period = arguments.length > 6 ? arguments[6] : undefined;
    var accepted = arguments.length > 7 && arguments[7] !== undefined ? arguments[7] : false;
    var eating = arguments.length > 8 && arguments[8] !== undefined ? arguments[8] : true;

    _classCallCheck(this, GroupDemand);

    this.id = id;
    this.userId = userId;
    this.name = name;
    this.numberOfGuests = numberOfGuests;
    this.startDay = startDay;
    this.startMonth = startMonth;
    this.period = period;
    this.accepted = accepted;
    this.eating = eating;
  }

  _createClass(GroupDemand, [{
    key: "setId",
    value: function setId(id) {
      this.id = id;
    }
  }, {
    key: "setUserId",
    value: function setUserId(userId) {
      this.userId = userId;
    }
  }, {
    key: "setName",
    value: function setName(name) {
      this.name = name;
    }
  }, {
    key: "setNumberOfGuests",
    value: function setNumberOfGuests(number) {
      this.numberOfGuests = number;
    }
  }, {
    key: "setStartDay",
    value: function setStartDay(number) {
      this.startDay = number;
    }
  }, {
    key: "setStartMonth",
    value: function setStartMonth(number) {
      this.startMonth = number;
    }
  }, {
    key: "setPeriod",
    value: function setPeriod(period) {
      this.period = period;
    }
  }, {
    key: "setAccepted",
    value: function setAccepted(accepted) {
      this.accepted = accepted;
    }
  }, {
    key: "setEating",
    value: function setEating(eating) {
      this.eating = eating;
    }
  }, {
    key: "getId",
    value: function getId() {
      return this.id;
    }
  }, {
    key: "getUserId",
    value: function getUserId() {
      return this.userId;
    }
  }, {
    key: "getName",
    value: function getName() {
      return this.name;
    }
  }, {
    key: "getNumberOfGuests",
    value: function getNumberOfGuests() {
      return this.numberOfGuests;
    }
  }, {
    key: "getStartDay",
    value: function getStartDay() {
      return this.startDay;
    }
  }, {
    key: "getStartMonth",
    value: function getStartMonth() {
      return this.startMonth;
    }
  }, {
    key: "getPeriod",
    value: function getPeriod() {
      return this.period;
    }
  }, {
    key: "getAccepted",
    value: function getAccepted() {
      return this.accepted;
    }
  }, {
    key: "getEating",
    value: function getEating() {
      return this.eating;
    }
  }]);

  return GroupDemand;
}();

module.exports = GroupDemand;