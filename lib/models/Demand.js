"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Demand = function Demand() {
  var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var hotelId = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  var numberOfGuests = arguments.length > 2 ? arguments[2] : undefined;
  var numberOfRooms = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 1;
  var reservationDate = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {
    day: 0,
    month: 0,
    year: 0
  };
  var startDate = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : {
    day: 0,
    month: 0,
    year: 0
  };
  var period = arguments.length > 6 ? arguments[6] : undefined;
  var accepted = arguments.length > 7 && arguments[7] !== undefined ? arguments[7] : false;
  var stornoDate = arguments.length > 8 && arguments[8] !== undefined ? arguments[8] : null;
  var price = arguments.length > 9 && arguments[9] !== undefined ? arguments[9] : 0;
  var type = arguments.length > 10 ? arguments[10] : undefined;
  var requirements = arguments.length > 11 && arguments[11] !== undefined ? arguments[11] : {
    maxPrice: 0,
    // maximalni cena
    standardRoom: false,
    // standartní pokoj (ano/ne)
    premiumRoom: false,
    // prémium pokoj (ano/ne)
    connectedRoom: false,
    // spojené pokoje (ano/ne)
    breakfast: false,
    // snídaně (ano/ne)
    fullBoard: false,
    // polopenze/plná penze (ano/ne)
    alaCarte: false,
    // a la carte restaurace (ano/ne)
    kidsMenu: false,
    // dětské menu/vybavení pro děti (ano/ne)
    roomService: false,
    // room service (ano/ne)
    parking: false,
    // parkování (ano/ne)
    busParking: false,
    // parkování pro autobus (ano/ne)
    shuttle: false,
    // taxi/shuttle (ano/ne)
    wellnes: false,
    // wellnes a masáže (ano/ne)
    anotherServices: false,
    // další služby (ano/ne)
    conferenceSpace: false,
    // konferenční prostory (ano/ne)
    raut: false,
    // raut/slavnostní oběd (ano/ne)
    bar: false // bar (ano/ne)

  };

  _classCallCheck(this, Demand);

  this.id = id;
  this.hotelId = hotelId;
  this.numberOfGuests = numberOfGuests; // počet hostů

  this.numberOfRooms = numberOfRooms; // pocet pokoju

  this.reservationDate = reservationDate; // datum rezervace

  this.startDate = startDate; // od kdy

  this.period = period; // délka ubytování

  this.accepted = accepted; // příjmuta?

  this.stornoDate = stornoDate; // datum storna

  this.price = price; // cena poptavky

  this.type = type; // typ

  this.requirements = requirements; // pozadavky
};

module.exports = Demand;