"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var ModelDate = require('./ModelDate');

var Hotel =
/*#__PURE__*/
function (_ModelDate) {
  _inherits(Hotel, _ModelDate);

  function Hotel() {
    var _this;

    var hotelId = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var month = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    var year = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    var facilityInvestment = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;
    var promotionInvestment = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 0;
    var foodMaterialQuality = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : 0.5;
    var roomMaterialQuality = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : 0.7;
    var hr = arguments.length > 7 && arguments[7] !== undefined ? arguments[7] : {
      as: {
        // accomodation service
        reception: {
          // pracovnici recepce
          junior: 0,
          senior: 0
        },
        housekeeping: {
          // pracovnici uklidu
          junior: 0,
          senior: 0
        }
      },
      fb: {
        // pracovnici restaurace
        breakfast: {
          // snidane
          kitchen: {
            // kuhari
            junior: 0,
            senior: 0,
            auxiliary: 0
          },
          // cisnici
          restaurant: {
            junior: 0,
            senior: 0
          }
        },
        dinner: {
          // vecere
          kitchen: {
            // kuchari
            junior: 0,
            senior: 0,
            auxiliary: 0
          },
          // cisnici
          restaurant: {
            junior: 0,
            senior: 0
          }
        }
      }
    };
    var rooms = arguments.length > 8 && arguments[8] !== undefined ? arguments[8] : {
      // pocet jednotlivych pokoju
      standard: 60,
      premium: 20
    };
    var equipment = arguments.length > 9 && arguments[9] !== undefined ? arguments[9] : {
      standardRoom: false,
      // standartní pokoj (ano/ne)
      premiumRoom: false,
      // prémium pokoj (ano/ne)
      connectedRoom: false,
      // spojené pokoje (ano/ne)
      breakfast: false,
      // snídaně (ano/ne)
      fullBoard: false,
      // polopenze/plná penze (ano/ne)
      alaCarte: false,
      // a la carte restaurace (ano/ne)
      kidsMenu: false,
      // dětské menu/vybavení pro děti (ano/ne)
      roomService: false,
      // room service (ano/ne)
      parking: false,
      // parkování (ano/ne)
      busParking: false,
      // parkování pro autobus (ano/ne)
      shuttle: false,
      // taxi/shuttle (ano/ne)
      wellnes: false,
      // wellnes a masáže (ano/ne)
      anotherServices: false,
      // další služby (ano/ne)
      conferenceSpace: false,
      // konferenční prostory (ano/ne)
      raut: false,
      // raut/slavnostní oběd (ano/ne)
      bar: false // bar (ano/ne)

    };

    _classCallCheck(this, Hotel);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Hotel).call(this, month, year));
    _this.hotelId = hotelId;
    _this.facilityInvestment = facilityInvestment; // investice do vybaveni

    _this.promotionInvestment = promotionInvestment; // investice do prezentace

    _this.foodMaterialQuality = foodMaterialQuality; // kvalita jidla

    _this.roomMaterialQuality = roomMaterialQuality; // kvalita vybaveni

    _this.hr = hr; // zamestnanci hotelu

    _this.rooms = rooms; // počet pokojů

    _this.equipment = equipment; // vybavení hotelu (object)

    return _this;
  }

  return Hotel;
}(ModelDate);

module.exports = Hotel;