"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

//abstract model
module.exports = function Model() {
  var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var hotelId = arguments.length > 1 ? arguments[1] : undefined;
  var month = arguments.length > 2 ? arguments[2] : undefined;
  var year = arguments.length > 3 ? arguments[3] : undefined;

  _classCallCheck(this, Model);

  this.id = id;
  this.hotelId = hotelId;
  this.month = month;
  this.year = year;
};