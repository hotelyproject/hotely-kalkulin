"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

module.exports = function ModelDate(month, year) {
  _classCallCheck(this, ModelDate);

  this.month = month;
  this.year = year;
};