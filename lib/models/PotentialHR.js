"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Model = require('./Model');

var PotentialHR =
/*#__PURE__*/
function (_Model) {
  _inherits(PotentialHR, _Model);

  // HR F&B úsek
  function PotentialHR() {
    var _this;

    var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var userId = arguments.length > 1 ? arguments[1] : undefined;
    var place = arguments.length > 2 ? arguments[2] : undefined;
    var position = arguments.length > 3 ? arguments[3] : undefined;
    var type = arguments.length > 4 ? arguments[4] : undefined;
    var count = arguments.length > 5 ? arguments[5] : undefined;

    _classCallCheck(this, PotentialHR);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(PotentialHR).call(this, id, userId));
    _this.place = place; // pracovní doba (breakfast: 3, dinner: 4)

    _this.position = position; // pozice (restaurant: 1, kitchen: 2) restaurace = číšník, kitchen = kuchař

    _this.type = type; // zkušenost (auxiliary: 1, junior: 2, senior: 3)

    _this.count = count; // počet

    return _this;
  }

  _createClass(PotentialHR, [{
    key: "setPlace",
    value: function setPlace(place) {
      this.place = place;
    }
  }, {
    key: "setPosition",
    value: function setPosition(position) {
      this.position = position;
    }
  }, {
    key: "setType",
    value: function setType(type) {
      this.type = type;
    }
  }, {
    key: "setCount",
    value: function setCount(count) {
      this.count = count;
    }
  }, {
    key: "getPlace",
    value: function getPlace() {
      return this.place;
    }
  }, {
    key: "getPosition",
    value: function getPosition() {
      return this.position;
    }
  }, {
    key: "getType",
    value: function getType() {
      return this.type;
    }
  }, {
    key: "getCount",
    value: function getCount() {
      return this.count;
    }
  }]);

  return PotentialHR;
}(Model);

module.exports = PotentialHR;