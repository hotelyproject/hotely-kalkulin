"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var ModelDate = require('./ModelDate');

var HotelCosts =
/*#__PURE__*/
function (_ModelDate) {
  _inherits(HotelCosts, _ModelDate);

  function HotelCosts() {
    var _this;

    var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var userId = arguments.length > 1 ? arguments[1] : undefined;
    var costs = arguments.length > 2 ? arguments[2] : undefined;
    var day = arguments.length > 3 ? arguments[3] : undefined;
    var month = arguments.length > 4 ? arguments[4] : undefined;
    var year = arguments.length > 5 ? arguments[5] : undefined;
    var type = arguments.length > 6 ? arguments[6] : undefined;

    _classCallCheck(this, HotelCosts);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(HotelCosts).call(this, id, userId, day, month, year));
    _this.costs = costs;
    _this.type = type; // hr/eq/mat/prop/oth/fix

    return _this;
  }

  _createClass(HotelCosts, [{
    key: "getCosts",
    value: function getCosts() {
      return this.costs;
    }
  }, {
    key: "setCosts",
    value: function setCosts(costs) {
      this.costs = costs;
    }
  }, {
    key: "getType",
    value: function getType() {
      return this.type;
    }
  }, {
    key: "setType",
    value: function setType(type) {
      this.type = type;
    }
  }]);

  return HotelCosts;
}(ModelDate);

module.exports = HotelCosts;