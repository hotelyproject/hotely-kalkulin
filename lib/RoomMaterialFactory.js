"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var DatabaseHandler = require('./DatabaseHandler.js');

var RoomMaterial = require('./models/RoomMaterialQuality');

var RoomMaterialFactory =
/*#__PURE__*/
function () {
  function RoomMaterialFactory() {
    _classCallCheck(this, RoomMaterialFactory);

    this.db = new DatabaseHandler();
  }
  /*
  * Find RoomMaterial
  *
  * @param {number} id    id of RoomMaterial
  *
  * @return {RoomMaterial} return selected RoomMaterial.
  */


  _createClass(RoomMaterialFactory, [{
    key: "get",
    value: function get(id) {
      var m = this.db.getRoomMaterial(id);
      return new RoomMaterial(m.id, m.level, m.cost);
    }
    /*
     * Save or upadate RoomMaterial
     *
     * @param {RoomMaterial}     RoomMaterial
     *
     * @return {} return 
     */

  }, {
    key: "save",
    value: function save(material) {
      this.db.saveRoomMaterial(material);
    }
  }]);

  return RoomMaterialFactory;
}();

module.exports = RoomMaterialFactory;