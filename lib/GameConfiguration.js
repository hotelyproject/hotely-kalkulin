"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var GameConfiguration = function GameConfiguration(cHRASRec, //koeficienty upravující váhu HR potenciálu recepce a housekeepingu v celkovém HR potenciálu ubytovacích služeb
cHRASHous, //koeficienty upravující váhu HR potenciálu recepce a housekeepingu v celkovém HR potenciálu ubytovacích služeb
cHRAS, //koeficienty upravující váhu HR potenciálu jednotlivých typů služeb v celkovém HR potenciálu hotelu
cHRFB, //koeficienty upravující váhu HR potenciálu jednotlivých typů služeb v celkovém HR potenciálu hotelu
chrfbBreakKitchen, //koeficienty upravující váhu HR potenciálů stravovacích služeb během snídaní (break) v kuchyni (kitchen) a v restauraci (rest) v celkovém HR potenciálu stravovacích služeb během snídaní
chrfbBreakRest, //koeficienty upravující váhu HR potenciálů stravovacích služeb během snídaní (break) v kuchyni (kitchen) a v restauraci (rest) v celkovém HR potenciálu stravovacích služeb během snídaní
chrfbBreak, //koeficienty upravující váhu HR potenciálu stravovacích služeb během snídaní (break) a skupinových večeří (group) v celkovém HR potenciálu stravovacích služeb
chrfbGroup, //koeficienty upravující váhu HR potenciálu stravovacích služeb během snídaní (break) a skupinových večeří (group) v celkovém HR potenciálu stravovacích služeb
cpqhr, //koeficienty upravující váhy HR potenciálu, Kvality surovin a Kvality vybavení hotelu na celkovém vnímání Kvality služeb hotelu
cpqmq, //koeficienty upravující váhy HR potenciálu, Kvality surovin a Kvality vybavení hotelu na celkovém vnímání Kvality služeb hotelu
cpqeq, //koeficienty upravující váhy HR potenciálu, Kvality surovin a Kvality vybavení hotelu na celkovém vnímání Kvality služeb hotelu
chrfbGroupKitchen, //koeficienty upravující váhu HR potenciálů stravovacích služeb během skupinových večeří (group) v kuchyni (kitchen) a v restauraci (rest) v celkovém HR potenciálu stravovacích služeb během skupinových večeří
chrfbGroupRest, //koeficienty upravující váhu HR potenciálů stravovacích služeb během skupinových večeří (group) v kuchyni (kitchen) a v restauraci (rest) v celkovém HR potenciálu stravovacích služeb během skupinových večeří
cMQAS, //koeficienty upravující váhu kvality surovin použitých na ubytovacím a stravovacím úseku hotelu
cMQFB, //koeficienty upravující váhu kvality surovin použitých na ubytovacím a stravovacím úseku hotelu
coaeq, //koeficienty upravující váhy Kvality vybavení, Portfolia služeb a Úrovně značky hotelu na celkovém vnímání Atraktivity nabídky hotelu
coapp, //koeficienty upravující váhy Kvality vybavení, Portfolia služeb a Úrovně značky hotelu na celkovém vnímání Atraktivity nabídky hotelu
coabl, //koeficienty upravující váhy Kvality vybavení, Portfolia služeb a Úrovně značky hotelu na celkovém vnímání Atraktivity nabídky hotelu
brandLevel, //Úroveň značky hotelu; nastavuje správce hry, může nabývat hodnot v rozmezí 0 (bídná) až 1 (vynikající)
juniorReceptionWage, //mzda juniora na recepci
seniorReceptionWage, //mzda seniora na recepci
juniorRoomServiceWage, //mzda juniora housekeeping
seniorRoomServiceWage, //mzda seniora housekeeping
juniorBreakfastChefWage, //mzda juniorního kuchaře na snídaních
seniorBreakfastChefWage, //mzda seniorního kuchaře na snídaních
helpBreakfastWage, //mzda pomocné síly v kuchyni na snídaních
juniorBreakfastWaiterWage, //mzda juniorního číšníka na snídaních
seniorBreakfastWaiterWage, //mzda seniorního číšníka na snídaních
juniorDinnerChefWage, //mzda juniorního kuchaře na večeři
seniorDinnerChefWage, //mzda seniorního kuchaře na večeři
helpDinnerWage, //mzda pomocné síly v kuchyni na večeři
juniorDinnerWaiterWage, //mzda juniorního číšníka na večeři
seniorDinnerWaiterWage, //mzda seniorního číšníka na večeři
roomQualityStandardCost, //náklady RoomMaterial standard
roomQualityPremiumCost, //náklady RoomMaterial premium
foodQualityEconomyBreakfastCost, //náklady na snídaně economy
foodQualityEconomyDinnerCost, //náklady na večeře economy
foodQualityEconomyAlaCarteCost, //náklady na a la carte economy
foodQualityEconomyBarCost, //náklady na bar economy
foodQualityStandardBreakfastCost, //náklady na snídaně standard
foodQualityStandardDinnerCost, //náklady na večeře standard
foodQualityStandardAlaCarteCost, //náklady na a la carte standard
foodQualityStandardBarCost, //náklady na bar standard
foodQualityPremiumBreakfastCost, //náklady na snídaně premium
foodQualityPremiumDinnerCost, //náklady na večeře premium
foodQualityPremiumAlaCarteCost, //náklady na a la carte premium
foodQualityPremiumBarCost) //náklady na bar premium
{
  _classCallCheck(this, GameConfiguration);

  this.cHRASRec = cHRASRec;
  this.cHRASHous = cHRASHous;
  this.cHRAS = cHRAS;
  this.cHRFB = cHRFB;
  this.chrfbBreakKitchen = chrfbBreakKitchen;
  this.chrfbBreakRest = chrfbBreakRest;
  this.chrfbBreak = chrfbBreak;
  this.chrfbGroup = chrfbGroup;
  this.cpqhr = cpqhr;
  this.cpqmq = cpqmq;
  this.cpqeq = cpqeq;
  this.chrfbGroupKitchen = chrfbGroupKitchen;
  this.chrfbGroupRest = chrfbGroupRest;
  this.cMQAS = cMQAS;
  this.cMQFB = cMQFB;
  this.coaeq = coaeq;
  this.coapp = coapp;
  this.coabl = coabl;
  this.brandLevel = brandLevel;
  this.juniorReceptionWage = juniorReceptionWage;
  this.seniorReceptionWage = seniorReceptionWage;
  this.juniorRoomServiceWage = juniorRoomServiceWage;
  this.seniorRoomServiceWage = seniorRoomServiceWage;
  this.juniorBreakfastChefWage = juniorBreakfastChefWage;
  this.seniorBreakfastChefWage = seniorBreakfastChefWage;
  this.helpBreakfastWage = helpBreakfastWage;
  this.juniorBreakfastWaiterWage = juniorBreakfastWaiterWage;
  this.seniorBreakfastWaiterWage = seniorBreakfastWaiterWage;
  this.juniorDinnerChefWage = juniorDinnerChefWage;
  this.seniorDinnerChefWage = seniorDinnerChefWage;
  this.helpDinnerWage = helpDinnerWage;
  this.juniorDinnerWaiterWage = juniorDinnerWaiterWage;
  this.seniorDinnerWaiterWage = seniorDinnerWaiterWage;
  this.roomQualityStandardCost = roomQualityStandardCost;
  this.roomQualityPremiumCost = roomQualityPremiumCost;
  this.foodQualityEconomyBreakfastCost = foodQualityEconomyBreakfastCost;
  this.foodQualityEconomyDinnerCost = foodQualityEconomyDinnerCost;
  this.foodQualityEconomyAlaCarteCost = foodQualityEconomyAlaCarteCost;
  this.foodQualityEconomyBarCost = foodQualityEconomyBarCost;
  this.foodQualityStandardBreakfastCost = foodQualityStandardBreakfastCost;
  this.foodQualityStandardDinnerCost = foodQualityStandardDinnerCost;
  this.foodQualityStandardAlaCarteCost = foodQualityStandardAlaCarteCost;
  this.foodQualityStandardBarCost = foodQualityStandardBarCost;
  this.foodQualityPremiumBreakfastCost = foodQualityPremiumBreakfastCost;
  this.foodQualityPremiumDinnerCost = foodQualityPremiumDinnerCost;
  this.foodQualityPremiumAlaCarteCost = foodQualityPremiumAlaCarteCost;
  this.foodQualityPremiumBarCost = foodQualityPremiumBarCost;
};

module.exports = GameConfiguration;