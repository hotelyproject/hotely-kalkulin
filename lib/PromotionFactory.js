"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var DatabaseHandler = require('./DatabaseHandler.js');

var Promotion = require('./models/Promotion.js');

var PromotionFactory =
/*#__PURE__*/
function () {
  function PromotionFactory() {
    _classCallCheck(this, PromotionFactory);

    this.db = new DatabaseHandler();
  }
  /*
  * Find promotion
  *
  * @param {number} id    id of promotion
  *
  * @return {promotion} return selected promotion.
  */


  _createClass(PromotionFactory, [{
    key: "get",
    value: function get(id) {
      var p = this.db.getPromotion(id);
      return new Promotion(p.id, p.investment);
    }
    /*
     * Save or upadate promotion
     *
     * @param {promotion}     promotion
     *
     * @return {} return 
     */

  }, {
    key: "save",
    value: function save(promotion) {
      this.db.savePromotion(promotion);
    }
  }]);

  return PromotionFactory;
}();

module.exports = PromotionFactory;