"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Database = require('./Database.js');

var FacilityDB =
/*#__PURE__*/
function (_Database) {
  _inherits(FacilityDB, _Database);

  function FacilityDB(file) {
    var _this;

    _classCallCheck(this, FacilityDB);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(FacilityDB).call(this, file));

    _this.db.defaults({
      facility: [],
      facilityCount: 0,
      facilityLastId: 0
    }).write();

    return _this;
  }

  _createClass(FacilityDB, [{
    key: "save",
    value: function save(facility) {
      if (this.get(facility.getId())) {
        this._update(facility.getId(), facility);

        console.log('Facility updated');
        return this.get(facility.getId());
      } else if (this.getPlayerFacility(facility.getUserId())) {
        var f = this.getPlayerFacility(facility.getUserId());

        this._update(f.id, facility);

        console.log('Facility updated');
        return this.get(f.id);
      } else {
        var id = this.db.get('facilityLastId').value();
        this.db.update('facilityLastId', function (n) {
          return n + 1;
        }).write();
        this.db.get('facility').push({
          id: id,
          userId: facility.getUserId(),
          investment: facility.getInvestment(),
          level: facility.getLevel()
        }).write();
        this.db.update('facilityCount', function (n) {
          return n + 1;
        }).write();
        console.log('Facility inserted into db, id: ' + id);
        return {
          id: id,
          userId: facility.getUserId(),
          investment: facility.getInvestment(),
          level: facility.getLevel()
        };
      }
    }
  }, {
    key: "get",
    value: function get(id) {
      try {
        return this.db.get('facility').find({
          id: id
        }).value();
      } catch (error) {
        console.log('db/getFacility return: null');
        return null;
      }
    }
  }, {
    key: "getPlayerFacility",
    value: function getPlayerFacility(userId) {
      try {
        return this.db.get('facility').find({
          userId: userId
        }).value();
      } catch (error) {
        console.log('db/getPlayerFacility return: null');
        return null;
      }
    }
  }, {
    key: "_update",
    value: function _update(id, facility) {
      console.log('update Facility');
      this.db.get('facility').find({
        id: id
      }).assign({
        userId: facility.getUserId(),
        investment: facility.getInvestment(),
        level: facility.getLevel()
      }).write();
    } // _removeFacalityById(id)

  }]);

  return FacilityDB;
}(Database);

module.exports = FacilityDB;