"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Database = require('./Database.js');

var RoomDB =
/*#__PURE__*/
function (_Database) {
  _inherits(RoomDB, _Database);

  function RoomDB(file) {
    var _this;

    _classCallCheck(this, RoomDB);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(RoomDB).call(this, file));

    _this.db.defaults({
      room: [],
      roomCount: 0,
      roomLastId: 0
    }).write();

    return _this;
  }

  _createClass(RoomDB, [{
    key: "save",
    value: function save(room) {
      if (this.get(room.getId())) {
        this._update(room.getId(), room);

        console.log('Room updated');
        return this.get(room.getId());
      } else {
        var id = this.db.get('roomLastId').value();
        this.db.update('roomLastId', function (n) {
          return n + 1;
        }).write();
        this.db.get('room').push({
          id: id,
          userId: room.getUserId(),
          demandId: room.getDemandId(),
          roomNumber: room.getRoomNumber(),
          numberOfBeds: room.getNumberOfBeds(),
          type: room.getType()
        }).write();
        this.db.update('roomCount', function (n) {
          return n + 1;
        }).write();
        console.log('Room inserted into db, id: ' + id);
        return {
          id: id,
          userId: room.getUserId(),
          demandId: room.getDemandId(),
          roomNumber: room.getRoomNumber(),
          numberOfBeds: room.getNumberOfBeds(),
          type: room.getType()
        };
      }
    }
  }, {
    key: "get",
    value: function get(id) {
      try {
        return this.db.get('room').find({
          id: id
        }).value();
      } catch (error) {
        console.log('db/getRoom return: null');
        return null;
      }
    }
  }, {
    key: "getPlayersRooms",
    value: function getPlayersRooms(userId) {
      return this.db.get('room').filter({
        userId: userId
      }).value();
    }
  }, {
    key: "getRoomsByDemand",
    value: function getRoomsByDemand(userId, demandId) {
      return this.db.get('room').filter({
        userId: userId,
        demandId: demandId
      }).value();
    }
  }, {
    key: "getAllOccupiedRooms",
    value: function getAllOccupiedRooms() {
      var allRooms = this.db.get('room').value();
      var occupiedRooms = [];
      allRooms.forEach(function (room) {
        if (room.demandId != null) {
          occupiedRooms.push(room);
        }
      });
      return occupiedRooms;
    }
  }, {
    key: "_update",
    value: function _update(id, room) {
      console.log('update Room');
      this.db.get('room').find({
        id: id
      }).assign({
        userId: room.getUserId(),
        demandId: room.getDemandId(),
        roomNumber: room.getRoomNumber(),
        numberOfBeds: room.getNumberOfBeds(),
        type: room.getType()
      }).write();
    }
  }]);

  return RoomDB;
}(Database);

module.exports = RoomDB;