"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Database = require('./Database.js');

var HotelCostsDB =
/*#__PURE__*/
function (_Database) {
  _inherits(HotelCostsDB, _Database);

  function HotelCostsDB(file) {
    var _this;

    _classCallCheck(this, HotelCostsDB);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(HotelCostsDB).call(this, file));

    _this.db.defaults({
      costs: [],
      costsCount: 0,
      costsLastId: 0
    }).write();

    return _this;
  }

  _createClass(HotelCostsDB, [{
    key: "save",
    value: function save(hotelCosts) {
      //todo podle data
      if (this.get(hotelCosts.getId())) {
        this._update(hotelCosts.getId(), hotelCosts);

        return this.get(hotelCosts.getId());
      } else {
        var id = this.db.get('costsLastId').value();
        this.db.update('costsLastId', function (n) {
          return n + 1;
        }).write();
        this.db.get('costs').push({
          id: id,
          userId: hotelCosts.getUserId(),
          costs: hotelCosts.getCosts(),
          day: hotelCosts.getDay(),
          month: hotelCosts.getMonth(),
          year: hotelCosts.getYear(),
          type: hotelCosts.getType()
        }).write();
        this.db.update('costsCount', function (n) {
          return n + 1;
        }).write();
        console.log('hotelCosts inserted into db, id: ' + id);
        return {
          id: id,
          userId: hotelCosts.getUserId(),
          costs: hotelCosts.getCosts(),
          day: hotelCosts.getDay(),
          month: hotelCosts.getMonth(),
          year: hotelCosts.getYear(),
          type: hotelCosts.getType()
        };
      }
    }
  }, {
    key: "get",
    value: function get(id) {
      try {
        return this.db.get('costs').find({
          id: id
        }).value();
      } catch (error) {
        console.log('db/getCosts return: null', error);
        return null;
      }
    }
  }, {
    key: "getPlayerCostsInMonth",
    value: function getPlayerCostsInMonth(userId, month, year) {
      try {
        return this.db.get('costs').filter({
          userId: userId,
          month: month,
          year: year
        }).value();
      } catch (error) {
        console.log('db/getPlayerCosts return: null', error);
        return null;
      }
    }
  }, {
    key: "_update",
    value: function _update(id, hotelCosts) {
      console.log('update HotelCosts');
      this.db.get('costs').find({
        id: id
      }).assign({
        userId: hotelCosts.getUserId(),
        costs: hotelCosts.getCosts(),
        day: hotelCosts.getDay(),
        month: hotelCosts.getMonth(),
        year: hotelCosts.getYear(),
        type: hotelCosts.getType()
      }).write();
    }
  }]);

  return HotelCostsDB;
}(Database);

module.exports = HotelCostsDB;