"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Database = require('./Database.js');

var PromotionDB =
/*#__PURE__*/
function (_Database) {
  _inherits(PromotionDB, _Database);

  function PromotionDB(file) {
    var _this;

    _classCallCheck(this, PromotionDB);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(PromotionDB).call(this, file));

    _this.db.defaults({
      promotion: [],
      promotionCount: 0,
      promotionLastId: 0
    }).write();

    return _this;
  }

  _createClass(PromotionDB, [{
    key: "save",
    value: function save(promotion) {
      if (this.get(promotion.getId())) {
        this._update(promotion.getId(), promotion);

        console.log('Promotion updated');
        return this.get(promotion.getId());
      } else if (this.getPlayerPromotion(promotion.getUserId())) {
        var p = this.getPlayerPromotion(promotion.getUserId());

        this._update(p.id, promotion);

        console.log('Promotion updated');
        return this.get(p.id);
      } else {
        var id = this.db.get('promotionLastId').value();
        this.db.update('promotionLastId', function (n) {
          return n + 1;
        }).write();
        this.db.get('promotion').push({
          id: id,
          userId: promotion.getUserId(),
          investment: promotion.getInvestment()
        }).write();
        this.db.update('promotionCount', function (n) {
          return n + 1;
        }).write();
        console.log('Promotion inserted into db, id: ' + id);
        return {
          id: id,
          userId: promotion.getUserId(),
          investment: promotion.getInvestment()
        };
      }
    }
  }, {
    key: "get",
    value: function get(id) {
      try {
        return this.db.get('promotion').find({
          id: id
        }).value();
      } catch (error) {
        console.log('db/getPromotion return: null');
        return null;
      }
    }
  }, {
    key: "getPlayerPromotion",
    value: function getPlayerPromotion(userId) {
      try {
        return this.db.get('promotion').find({
          userId: userId
        }).value();
      } catch (error) {
        console.log('db/getPlayerPromotion return: null');
        return null;
      }
    }
  }, {
    key: "_update",
    value: function _update(id, promotion) {
      console.log('update Promotion');
      this.db.get('promotion').find({
        id: id
      }).assign({
        userId: promotion.getUserId(),
        investment: promotion.getInvestment()
      }).write();
    } // removePromotionById(id)

  }]);

  return PromotionDB;
}(Database);

module.exports = PromotionDB;