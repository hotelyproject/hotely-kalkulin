"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Database = require('./Database.js');

var HRDB =
/*#__PURE__*/
function (_Database) {
  _inherits(HRDB, _Database);

  function HRDB(file) {
    var _this;

    _classCallCheck(this, HRDB);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(HRDB).call(this, file));

    _this.db.defaults({
      hr: [],
      hrCount: 0,
      hrLastId: 0
    }).write();

    return _this;
  }

  _createClass(HRDB, [{
    key: "save",
    value: function save(hr) {
      if (this.get(hr.getId())) {
        this._update(hr.getId(), hr);

        console.log('HR updated');
        return this.get(hr.getId());
      } else {
        var id = this.db.get('hrLastId').value();
        this.db.update('hrLastId', function (n) {
          return n + 1;
        }).write();
        this.db.get('hr').push({
          id: id,
          userId: hr.getUserId(),
          place: hr.getPlace(),
          position: hr.getPosition(),
          type: hr.getType(),
          count: hr.getCount()
        }).write();
        this.db.update('hrCount', function (n) {
          return n + 1;
        }).write();
        console.log('HR inserted into db, id: ' + id);
        return {
          id: id,
          userId: hr.getUserId(),
          place: hr.getPlace(),
          position: hr.getPosition(),
          type: hr.getType(),
          count: hr.getCount()
        };
      }
    }
  }, {
    key: "get",
    value: function get(id) {
      try {
        return this.db.get('hr').find({
          id: id
        }).value();
      } catch (error) {
        console.log('db/getHR return: null');
        return null;
      }
    }
  }, {
    key: "getPlayerHRs",
    value: function getPlayerHRs(userId) {
      return this.db.get('hr').filter({
        userId: userId
      }).value();
    }
  }, {
    key: "_update",
    value: function _update(id, hr) {
      console.log('update HR');
      this.db.get('hr').find({
        id: id
      }).assign({
        userId: hr.getUserId(),
        place: hr.getPlace(),
        position: hr.getPosition(),
        type: hr.getType(),
        count: hr.getCount()
      }).write();
    } // removeHRById(id)

  }]);

  return HRDB;
}(Database);

module.exports = HRDB;