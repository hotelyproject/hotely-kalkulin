"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Database = require('./Database.js');

var EquiptQualityResultsDB =
/*#__PURE__*/
function (_Database) {
  _inherits(EquiptQualityResultsDB, _Database);

  function EquiptQualityResultsDB() {
    var _this;

    var file = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : './equiptQualityResultsDB.json';

    _classCallCheck(this, EquiptQualityResultsDB);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(EquiptQualityResultsDB).call(this, file));

    _this.db.defaults({
      result: [],
      resultCount: 0,
      resultLastId: 0
    }).write();

    return _this;
  }

  _createClass(EquiptQualityResultsDB, [{
    key: "save",
    value: function save(userId, result, year, month) {
      var id = this.db.get('resultLastId').value();
      this.db.update('resultLastId', function (n) {
        return n + 1;
      }).write();
      this.db.get('result').push({
        id: id,
        userId: userId,
        result: result,
        year: year,
        month: month
      }).write();
      this.db.update('resultCount', function (n) {
        return n + 1;
      }).write();
      console.log('Equipt quality result inserted into db, id: ' + id);
    }
  }, {
    key: "get",
    value: function get(userId, year, month) {
      // todo test with 0 records
      return this.db.get('result').filter({
        userId: userId,
        year: year,
        month: month
      }).value();
    }
  }]);

  return EquiptQualityResultsDB;
}(Database);

module.exports = EquiptQualityResultsDB;