"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Database = require('./Database.js');

var HotelRevenueDB =
/*#__PURE__*/
function (_Database) {
  _inherits(HotelRevenueDB, _Database);

  function HotelRevenueDB(file) {
    var _this;

    _classCallCheck(this, HotelRevenueDB);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(HotelRevenueDB).call(this, file));

    _this.db.defaults({
      revenue: [],
      revenueCount: 0,
      revenueLastId: 0
    }).write();

    return _this;
  }

  _createClass(HotelRevenueDB, [{
    key: "save",
    value: function save(revenue) {
      if (this.get(revenue.getId())) {
        this._update(revenue.getId(), revenue);

        return this.get(revenue.getId());
      } else {
        var id = this.db.get('revenueLastId').value();
        this.db.update('revenueLastId', function (n) {
          return n + 1;
        }).write();
        this.db.get('revenue').push({
          id: id,
          userId: revenue.getUserId(),
          revenue: revenue.getRevenue(),
          day: revenue.getDay(),
          month: revenue.getMonth(),
          year: revenue.getYear()
        }).write();
        this.db.update('revenueCount', function (n) {
          return n + 1;
        }).write();
        console.log('Revenue inserted into db, id: ' + id);
        return {
          id: id,
          userId: revenue.getUserId(),
          revenue: revenue.getRevenue(),
          day: revenue.getDay(),
          month: revenue.getMonth(),
          year: revenue.getYear()
        };
      }
    }
  }, {
    key: "get",
    value: function get(id) {
      try {
        return this.db.get('revenue').find({
          id: id
        }).value();
      } catch (error) {
        console.log('db/getRevenue return: null', error);
        return null;
      }
    }
  }, {
    key: "getPlayerHotelRevenues",
    value: function getPlayerHotelRevenues(userId) {
      try {
        return this.db.get('revenue').filter({
          userId: userId
        }).value();
      } catch (error) {
        console.log('db/getPlayerRevenue return: null', error);
        return null;
      }
    }
  }, {
    key: "getPlayerRevenuesInMonth",
    value: function getPlayerRevenuesInMonth(userId, month, year) {
      try {
        return this.db.get('revenue').filter({
          userId: userId,
          month: month,
          year: year
        }).value();
      } catch (error) {
        console.log('db/getPlayerRevenue return: null', error);
        return null;
      }
    }
  }, {
    key: "_update",
    value: function _update(id, revenue) {
      this.db.get('revenue').find({
        id: id
      }).assign({
        userId: revenue.getUserId(),
        revenue: revenue.getRevenue(),
        day: revenue.getDay(),
        month: revenue.getMonth(),
        year: revenue.getYear()
      }).write();
      console.log('Revenue updated');
    }
  }]);

  return HotelRevenueDB;
}(Database);

module.exports = HotelRevenueDB;