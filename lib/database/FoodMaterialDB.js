"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Database = require('./Database.js');

var FoodMaterialDB =
/*#__PURE__*/
function (_Database) {
  _inherits(FoodMaterialDB, _Database);

  function FoodMaterialDB(file) {
    var _this;

    _classCallCheck(this, FoodMaterialDB);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(FoodMaterialDB).call(this, file));

    _this.db.defaults({
      foodMaterial: [],
      foodMaterialCount: 0,
      foodMaterialLastId: 0
    }).write();

    return _this;
  }

  _createClass(FoodMaterialDB, [{
    key: "save",
    value: function save(material) {
      if (this.get(material.getId())) {
        this._update(material.getId(), material);

        console.log('FoodMaterial updated');
        return this.get(material.getId());
      } else if (this.getPlayerFoodMaterial(material.getUserId())) {
        var m = this.getPlayerFoodMaterial(material.getUserId());

        this._update(m.id, material);

        console.log('FoodMaterial updated');
        return this.get(m.id);
      } else {
        var id = this.db.get('foodMaterialLastId').value();
        this.db.update('foodMaterialLastId', function (n) {
          return n + 1;
        }).write();
        this.db.get('foodMaterial').push({
          id: id,
          userId: material.getUserId(),
          level: material.getLevel()
        }).write();
        this.db.update('foodMaterialCount', function (n) {
          return n + 1;
        }).write();
        console.log('FoodMaterial inserted into db, id: ' + id);
        return {
          id: id,
          userId: material.getUserId(),
          level: material.getLevel()
        };
      }
    }
  }, {
    key: "get",
    value: function get(id) {
      try {
        return this.db.get('foodMaterial').find({
          id: id
        }).value();
      } catch (error) {
        console.log('db/getFoodMaterial return: null');
        return false;
      }
    }
  }, {
    key: "getPlayerFoodMaterial",
    value: function getPlayerFoodMaterial(userId) {
      try {
        return this.db.get('foodMaterial').find({
          userId: userId
        }).value();
      } catch (error) {
        console.log('db/getFoodMaterial return: null');
        return false;
      }
    }
  }, {
    key: "_update",
    value: function _update(id, material) {
      console.log('update FoodMaterial');
      this.db.get('foodMaterial').find({
        id: id
      }).assign({
        userId: material.getUserId(),
        level: material.getLevel()
      }).write();
    } // removeFoodMaterial(id)

  }]);

  return FoodMaterialDB;
}(Database);

module.exports = FoodMaterialDB;