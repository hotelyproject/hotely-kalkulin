"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Database = require('./Database.js');

var ConfigDB =
/*#__PURE__*/
function (_Database) {
  _inherits(ConfigDB, _Database);

  function ConfigDB(file) {
    var _this;

    _classCallCheck(this, ConfigDB);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(ConfigDB).call(this, file));

    _this.db.defaults({
      config: [],
      configCount: 1
    }).write();

    return _this;
  }

  _createClass(ConfigDB, [{
    key: "save",
    value: function save(config) {
      if (this.get()) {
        this._update(config);
      } else {
        this.db.get('config').push(config).write();
        console.log('Config inserted into db');
      }
    }
  }, {
    key: "get",
    value: function get() {
      try {
        return this.db.get('config').find().value();
      } catch (error) {
        return null;
      }
    }
  }, {
    key: "_update",
    value: function _update(config) {
      this.db.get('config').find().assign(config).write();
      console.log('config updated');
    }
  }]);

  return ConfigDB;
}(Database);

module.exports = ConfigDB;