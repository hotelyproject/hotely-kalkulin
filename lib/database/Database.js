"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var low = require('lowdb');

var FileSync = require('lowdb/adapters/FileSync');

var Database =
/*#__PURE__*/
function () {
  function Database(file) {
    _classCallCheck(this, Database);

    var adapter = new FileSync(file);
    this.db = low(adapter);
  }

  _createClass(Database, [{
    key: "save",
    value: function save() {}
  }, {
    key: "get",
    value: function get(id) {}
  }, {
    key: "_update",
    value: function _update() {}
  }, {
    key: "remove",
    value: function remove(id) {}
  }]);

  return Database;
}();

module.exports = Database;