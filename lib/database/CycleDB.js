"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Database = require('./Database.js');

var CycleDB =
/*#__PURE__*/
function (_Database) {
  _inherits(CycleDB, _Database);

  function CycleDB(file) {
    var _this;

    _classCallCheck(this, CycleDB);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(CycleDB).call(this, file));

    _this.db.defaults({
      cycle: [],
      cycleCount: 0,
      cycleLastId: 0
    }).write();

    return _this;
  }

  _createClass(CycleDB, [{
    key: "save",
    value: function save(hotelId, date, cycle) {
      if (this.get(hotelId, date)) {
        this._update(hotelId, date, cycle);
      } else {
        //const id = this.db.get('cycleLastId').value()
        //this.db.update('cycleLastId', n => n + 1).write();
        this.db.get('cycle').push({
          hotelId: hotelId,
          month: date.month,
          year: date.year,
          cycle: cycle
        }).write();
        this.db.update('cycleCount', function (n) {
          return n + 1;
        }).write();
        console.log('Cycle inserted into db');
      }
    }
  }, {
    key: "get",
    value: function get(hotelId, date) {
      return this.db.get('cycle').find({
        hotelId: hotelId,
        month: date.month,
        year: date.year
      }).value();
    }
  }, {
    key: "_update",
    value: function _update(hotelId, date, cycle) {
      this.db.get('cycle').find({
        hotelId: hotelId,
        month: date.month,
        year: date.year
      }).assign({
        cycle: cycle
      }).write();
      console.log('Cycle updated');
    }
  }]);

  return CycleDB;
}(Database);

module.exports = CycleDB;