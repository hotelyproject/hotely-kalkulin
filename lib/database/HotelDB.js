"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Database = require('./Database.js');

var HotelDB =
/*#__PURE__*/
function (_Database) {
  _inherits(HotelDB, _Database);

  function HotelDB(file) {
    var _this;

    _classCallCheck(this, HotelDB);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(HotelDB).call(this, file));

    _this.db.defaults({
      hotel: [],
      hotelCount: 0,
      hotelLastId: 0
    }).write();

    return _this;
  }

  _createClass(HotelDB, [{
    key: "save",
    value: function save(hotel) {
      if (this.get(hotel.hotelId, {
        month: hotel.date.month,
        year: hotel.date.year
      })) {
        // if exist
        this._update(hotel);

        console.log('Hotel updated');
        return hotel;
      } else {
        //const id = this.db.get('hotelLastId').value();
        //this.db.update('hotelLastId', n => n + 1).write();
        //hotel.hotelId = id;
        this.db.get('hotel').push(hotel).write();
        this.db.update('hotelCount', function (n) {
          return n + 1;
        }).write();
        console.log('Hotel inserted into db');
        return hotel;
      }
    }
  }, {
    key: "get",
    value: function get(hotelId, date) {
      try {
        return this.db.get('hotel').find({
          hotelId: hotelId,
          date: {
            month: date.month,
            year: date.year
          }
        }).value();
      } catch (error) {
        return null;
      }
    }
  }, {
    key: "_update",
    value: function _update(hotel) {
      console.log('update Hotel');
      this.db.get('hotel').find({
        hotelId: hotel.hotelId,
        date: {
          month: hotel.date.month,
          year: hotel.date.year
        }
      }).assign(hotel).write();
    }
  }]);

  return HotelDB;
}(Database);

module.exports = HotelDB;