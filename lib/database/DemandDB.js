"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Database = require('./Database.js');

var DemandDB =
/*#__PURE__*/
function (_Database) {
  _inherits(DemandDB, _Database);

  function DemandDB(file) {
    var _this;

    _classCallCheck(this, DemandDB);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(DemandDB).call(this, file));

    _this.db.defaults({
      demand: [],
      demandCount: 0,
      demandLastId: 0
    }).write();

    return _this;
  }

  _createClass(DemandDB, [{
    key: "save",
    value: function save(demand) {
      if (this.get(demand.id)) {
        this._update(demand);

        console.log('Demand updated');
        return demand;
      } else {
        var id = this.db.get('demandLastId').value();
        this.db.update('demandLastId', function (n) {
          return n + 1;
        }).write();
        demand.id = id;
        this.db.get('demand').push(demand).write();
        this.db.update('demandCount', function (n) {
          return n + 1;
        }).write();
        console.log('Demand inserted into db, id: ' + id);
        return demand;
      }
    }
  }, {
    key: "get",
    value: function get(id) {
      try {
        return this.db.get('demand').find({
          id: id
        }).value();
      } catch (error) {
        console.log(error);
      }

      console.log('db/getDemand return null');
      return null;
    }
  }, {
    key: "getAllDemands",
    value: function getAllDemands(hotelId) {
      return this.db.get('demand').filter({
        hotelId: hotelId
      }).value();
    }
  }, {
    key: "getAcceptedDemands",
    value: function getAcceptedDemands(hotelId, date) {
      return this.db.get('demand').filter({
        accepted: true,
        hotelId: hotelId,
        startDate: {
          month: date.month,
          year: date.year
        }
      }).value();
    }
  }, {
    key: "getDemandsInMonth",
    value: function getDemandsInMonth(hotelId, date) {
      return this.db.get('demand').filter({
        startDate: {
          month: date.month,
          year: date.year
        },
        hotelId: hotelId
      }).value();
    }
  }, {
    key: "_update",
    value: function _update(demand) {
      console.log('update Demand');
      this.db.get('demand').find({
        id: demand.id
      }).assign(demand).write();
    }
  }]);

  return DemandDB;
}(Database);

module.exports = DemandDB;