"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Database = require('./Database.js');

var ResultDB =
/*#__PURE__*/
function (_Database) {
  _inherits(ResultDB, _Database);

  function ResultDB(file) {
    var _this;

    _classCallCheck(this, ResultDB);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(ResultDB).call(this, file));

    _this.db.defaults({
      result: [],
      resultCount: 0,
      resultLastId: 0
    }).write();

    return _this;
  }

  _createClass(ResultDB, [{
    key: "save",
    value: function save(userId, date, values) {
      if (this.getPlayersResults(userId, date)) {
        var r = this.getPlayersResults(userId, date);

        this._update(r.id, values);

        console.log('Results updated');
        return {
          id: r.id,
          userId: userId,
          month: month,
          year: year,
          value: values
        };
      } else {
        var id = this.db.get('resultLastId').value();
        this.db.update('resultLastId', function (n) {
          return n + 1;
        }).write();
        this.db.get('result').push({
          id: id,
          userId: userId,
          month: month,
          year: year,
          value: values
        }).write();
        this.db.update('resultCount', function (n) {
          return n + 1;
        }).write();
        console.log('Result inserted into db, id: ' + id);
        return {
          id: id,
          userId: userId,
          month: month,
          year: year,
          value: values
        };
      }
    }
  }, {
    key: "getPlayersResults",
    value: function getPlayersResults(hotelId, date) {
      try {
        return this.db.get('result').find({
          hotelId: hotelId,
          month: date.month,
          year: date.year
        }).value();
      } catch (error) {
        console.log('db/getResults return: null');
        return null;
      }
    }
  }, {
    key: "getAllPlayersResults",
    value: function getAllPlayersResults(hotelId) {
      try {
        return this.db.get('result').filter({
          hotelId: hotelId
        }).value();
      } catch (error) {
        console.log('db/getResults return: null');
        return null;
      }
    }
  }, {
    key: "_update",
    value: function _update(id, values) {
      this.db.get('result').find({
        id: id
      }).assign({
        value: values
      }).write();
    }
  }]);

  return ResultDB;
}(Database);

module.exports = ResultDB;