"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var DatabaseHandler = require('./DatabaseHandler.js');

var Demand = require('./models/Demand.js');

var DemandFactory =
/*#__PURE__*/
function () {
  function DemandFactory() {
    _classCallCheck(this, DemandFactory);

    this.db = new DatabaseHandler();
  }
  /*
   * Generate random demnads
   *
   * @return {array} return array of random demands.
   */


  _createClass(DemandFactory, [{
    key: "generateDemands",
    value: function generateDemands(range) {
      var demands = [];

      for (var i = 0; i < range; i++) {
        var name = 'User New ' + i;
        var numberOfBeds = Math.floor(Math.random() * 5);
        var startDay = Math.floor(Math.random() * 31);
        var startMonth = Math.floor(Math.random() * 12);
        var period = Math.floor(Math.random() * 20);
        var userId = Math.floor(Math.random() * 20);
        demands.push(new Demand(null, name, numberOfBeds, null, startDay, startMonth, period, userId));
      }

      return demands;
    }
    /*
     * Accept demand and save it
     *
     * @param {demnad} demand    demand
     *
     * @return {demand} return updated demand
     */

  }, {
    key: "acceptDemand",
    value: function acceptDemand(demand) {
      demand.setAccepted(true);
      var d = this.db.saveDemand(demand);
      return new Demand(d.id, d.name, d.numberOfBeds, d.room, d.startDay, d.startMonth, d.period, d.accepted, d.userId);
    }
    /*
     * Find accepted demands
     *
     * @return {array} return array of all accepted demands.
     */

  }, {
    key: "getAcceptedDemands",
    value: function getAcceptedDemands() {
      var demands = this.db.getAcceptedDemands();
      var arrayOfDemands = [];

      for (var i = 0; i < demands.length; i++) {
        arrayOfDemands.push(new Demand(demands[i].id, demands[i].name, demands[i].numberOfBeds, demands[i].room, demands[i].startDay, demands[i].startMonth, demands[i].period, demands[i].accepted, demands[i].userId));
      }

      return arrayOfDemands;
    }
    /*
     * Find demand
     *
     * @param {number} id    id of demand
     *
     * @return {demand} return selected demand.
     */

  }, {
    key: "get",
    value: function get(id) {
      var d = this.db.getDemand(id);
      return new Demand(d.id, d.name, d.numberOfBeds, d.room, d.startDay, d.startMonth, d.period, d.accepted, d.userId);
    }
    /*
     * Return number of available rooms
     *
     * @param {number} count    number of rooms
     * @param {number} period   selected month
     *
     * @return {number} return number of available rooms.
     */

  }, {
    key: "getNumberOfFreeRooms",
    value: function getNumberOfFreeRooms(count, period) {
      var demands = this.getAcceptedDemands();
      var numberOfOccupiedRooms = 0;

      for (var i = 0; i < demands.length; i++) {
        if (demands[i].getStartMonth() == period) {
          numberOfOccupiedRooms++;
        }
      }

      return count - numberOfOccupiedRooms;
    }
  }]);

  return DemandFactory;
}();

module.exports = DemandFactory;