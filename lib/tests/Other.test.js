"use strict";

var Kalkulin = require('./../Kalkulin');

var Demand = require('./../models/Demand');

var Room = require('./../models/Room.js');

var k = new Kalkulin();
test('setSettings', function () {
  var s = {
    userId: 2,
    f: {
      investment: 1,
      level: 2
    },
    rm: {
      level: 1
    },
    fm: {
      level: 1
    },
    p: {
      investment: 1
    },
    hr: {
      place: 4,
      position: 2,
      type: 2,
      count: 2
    },
    hras: {
      position: 1,
      type: 2,
      count: 2
    }
  };
  var result = k.setSettings(s);
  expect(result).toBeUndefined();
});
test('removeOldDemandFromRooms', function () {
  var result = k.removeOldDemandFromRooms();
  expect(result).toBeUndefined();
});
test('setDemandToRooms', function () {
  var d = new Demand(10, 2, "User New 0", 2, 10, 7, 2019, 10, false, true, 0.7, true);
  var r = new Room(16, 2, null, 10, 4, 0.7);
  var result = k.setDemandToRooms(d, [r]);
  expect(result).toBeUndefined();
});
test('generateDemands', function () {
  var result = k.generateDemands(3, 1, true, 1, false);
  expect(result).toBeUndefined();
});
test('acceptDemand', function () {
  var result = k.acceptDemand(new Demand(10, 2, "User New 0", 2, 10, 7, 2019, 10, false, true, 0.7, true));
  expect(result).toEqual(new Demand(10, 2, "User New 0", 2, 10, 7, 2019, 10, true, true, 0.7, true));
});
test('acceptDemand Reqeust', function () {
  var FEDemand = {
    id: 33,
    userId: 1,
    name: "User New 1",
    numberOfGuests: 2,
    startDay: 15,
    startMonth: 8,
    startYear: 2020,
    period: 4,
    accepted: false,
    withFood: true,
    roomType: 0.7,
    group: false
  };
  var result = k.acceptDemandRequest(FEDemand);
  expect(result).toEqual(new Demand(33, 1, "User New 1", 2, 15, 8, 2020, 4, false, true, 0.7, false));
});
test('getGroupDemands', function () {
  var result = k.getGroupDemands(2);
  expect(result).toEqual([new Demand(10, 2, "User New 0", 2, 10, 7, 2019, 10, true, true, 0.7, true)]);
});
test('setDailySettings', function () {
  var daySetting = {
    0: 100,
    1: 110,
    2: 5,
    3: 33,
    4: 68,
    5: 99,
    6: 12,
    7: 12,
    8: 12,
    9: 12,
    10: 12,
    11: 12,
    12: 12,
    13: 12,
    14: 12
  };
  var result = k.setDailySettings(2, 0, 11, 2019, daySetting);
  expect(result).toBeUndefined();
});