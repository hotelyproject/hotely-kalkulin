"use strict";

var Kalkulin = require('./../Kalkulin');

var k = new Kalkulin(); // todo change expected results

test('monthly hotel costs should be grather than 0', function () {
  var res = k.calculateMonthlyHotelCosts(1);
  expect(res).toBeGreaterThan(0);
});
test('monthly hotel fixed costs should be grather than 0', function () {
  var res = k.calculateMonthlyFixedCosts(1);
  expect(res).toBeGreaterThan(0);
});
test('daily hotel costs should be grather than 0', function () {
  var res = k.calculateDailyDirectCosts(1, 1, 1, 2020);
  expect(res).toBeGreaterThan(0);
});
test('material AS costs should be grather than 0', function () {
  var res = k.calculateMatCostsAS(1, 1, 1, 2020);
  expect(res).toBeGreaterThan(0);
});
test('material FB costs should be grather than 0', function () {
  var res = k.calculateMatCostsFB(1, 1, 1, 2020);
  expect(res).toBeGreaterThan(0);
});
test('material costs should be grather than 0', function () {
  var res = k.calculateMatCosts(1, 1, 1, 2020);
  expect(res).toBeGreaterThan(0);
});
test('material costs should be 0', function () {
  // todo
  var res = k.calculateOtaCommission(1);
  expect(res).toBe(0);
});
test('daily costs should be grather than 0', function () {
  var res = k.calculateDailyCosts(1);
  expect(res).toBeGreaterThan(0);
});
test('HR costs should be grather than 0', function () {
  var res = k.calculateHRCosts(1);
  expect(res).toBeGreaterThan(0);
});
test('reception costs should be grather than 0', function () {
  var res = k.calculateCostEmplRec(1);
  expect(res).toBeGreaterThan(0);
});
test('housekeeping costs should be grather than 0', function () {
  var res = k.calculateCostEmplHouse(1);
  expect(res).toBeGreaterThan(0);
});
test('breakfast empl costs should be grather than 0', function () {
  var res = k.calculateCostEmplBreak(1);
  expect(res).toBeGreaterThan(0);
});
test('group dinner empl costs should be grather than 0', function () {
  var res = k.calculateCostEmplGdinn(1);
  expect(res).toBeGreaterThan(0);
});
test('ala carte empl costs should be grather than 0', function () {
  var res = k.calculateCostEmplalcDinn(1);
  expect(res).toBeGreaterThan(0);
});
test('bar empl costs should be grather than 0', function () {
  var res = k.calculateCostEmplLb(1);
  expect(res).toBeGreaterThan(0);
});
test('other empl costs should be grather than 0', function () {
  var res = k.calculateCostEmplOthers(1);
  expect(res).toBeGreaterThan(0);
});
test('costs for hotel inovation should be grather than 0', function () {
  var res = k.calculateEquipCosts(1);
  expect(res).toBeGreaterThan(0);
});
test('promotion costs should be grather than 0', function () {
  var res = k.calculatePromoCosts(1);
  expect(res).toBeGreaterThan(0);
});
test('other services costs should be grather than 0', function () {
  var res = k.calculateOtherServicesCosts(1);
  expect(res).toBeGreaterThan(0);
});
test('fixed costs should be grather than 0', function () {
  var res = k.calculateFixedCosts(1);
  expect(res).toBeGreaterThan(0);
});