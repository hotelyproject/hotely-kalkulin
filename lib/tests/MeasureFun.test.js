"use strict";

var Kalkulin = require('./../Kalkulin');

var Demand = require('./../models/Demand');

var k = new Kalkulin();
test('CountPriceForDemand', function () {
  var d = new Demand(1, 1, "User New 1", 1, 17, 11, 2019, 16, true, true, 0.7, false);
  var result = k.countPriceForDemand(1, d);
  expect(result).not.toBeNull();
  expect(result).not.toBeUndefined();
});
test('averageDailyRate', function () {
  var result = k.averageDailyRate(1);
  expect(result).not.toBeNull();
  expect(result).not.toBeUndefined();
});
test('occupancyRate', function () {
  var result = k.occupancyRate(1);
  expect(result).not.toBeNull();
  expect(result).not.toBeUndefined();
});
test('calculateQuestSatisfaction', function () {
  var result = k.calculateQuestSatisfaction(1);
  expect(result).not.toBeNull();
  expect(result).not.toBeUndefined();
});
test('calculateOfferAtract', function () {
  var result = k.calculateOfferAtract(1);
  expect(result).not.toBeNull();
  expect(result).not.toBeUndefined();
});
test('calculateProductsQuality', function () {
  var result = k.calculateProductsQuality(1);
  expect(result).not.toBeNull();
  expect(result).not.toBeUndefined();
});
test('getPotentialHRFB', function () {
  var result = k.getPotentialHRFB(1);
  expect(result).not.toBeNull();
  expect(result).not.toBeUndefined();
});
test('getPotentialHRAsRec', function () {
  var result = k.getPotentialHRAsRec(1);
  expect(result).not.toBeNull();
  expect(result).not.toBeUndefined();
});
test('calculateHRPotential', function () {
  var result = k.calculateHRPotential(1);
  expect(result).not.toBeNull();
  expect(result).not.toBeUndefined();
});
test('calculateMaterialQuality', function () {
  var result = k.calculateMaterialQuality(1);
  expect(result).not.toBeNull();
  expect(result).not.toBeUndefined();
});
test('calculateEquipQuality', function () {
  var result = k.calculateEquipQuality(1);
  expect(result).not.toBeNull();
  expect(result).not.toBeUndefined();
});
test('saveResults', function () {
  var result = k.saveResults(1);
  expect(result).toBeUndefined();
});