"use strict";

var Kalkulin = require('./../Kalkulin');

var Demand = require('./../models/Demand');

var Facility = require('./../models/Facility.js');

var FoodMaterial = require('./../models/FoodMaterialQuality.js');

var PotentialHR = require('./../models/PotentialHR.js');

var PotentialHRAS = require('./../models/PotentialHRAS.js');

var Promotion = require('./../models/Promotion.js');

var RoomMaterial = require('./../models/RoomMaterialQuality.js');

var Room = require('./../models/Room.js');

var Hotel = require('./../models/Hotel.js');

var GameConfiguration = require('./../GameConfiguration.js');

var k = new Kalkulin();
test('Save demand', function () {
  var d = new Demand(1, 1, "User New 1", 1, 17, 9, 2019, 16, false, true, 0.7, false);
  var demand = k.saveDemand(d);
  expect(demand).toEqual(d);
});
test('Save facility', function () {
  var i = new Facility(1, 2, 1, 2);
  var t = k.saveFacility(i);
  expect(t).toEqual(i);
});
test('Save FoodMaterial', function () {
  var i = new FoodMaterial(1, 2, 1);
  var t = k.saveFoodMaterial(i);
  expect(t).toEqual(i);
});
test('Save PotentialHR', function () {
  var i = new PotentialHR(1, 1, 2, 2, 1, 2);
  var t = k.saveHR(i);
  expect(t).toEqual(i);
});
test('Save PotentialHRAS', function () {
  var i = new PotentialHRAS(1, 1, 2, 3, 2);
  var t = k.saveHRAS(i);
  expect(t).toEqual(i);
});
test('Save Promotion', function () {
  var i = new Promotion(1, 2, 1);
  var t = k.savePromotion(i);
  expect(t).toEqual(i);
});
test('Save Promotion Request', function () {
  var json = {
    userId: 0.522,
    investment: 1500
  };
  var t = k.savePromotionRequest(json);
  console.log("save promotion request", t);
  expect(t).toBeDefined();
});
test('Save RoomMaterial', function () {
  var i = new RoomMaterial(1, 2, 1);
  var t = k.saveRoomMaterial(i);
  expect(t).toEqual(i);
});
test('Save Room', function () {
  var i = new Room(1, 1, null, 5, 3, 0.5);
  var t = k.saveRoom(i);
  expect(t).toEqual(i);
});
test('Save Hotel', function () {
  var i = new Hotel(1, 2);
  var t = k.saveHotel(i);
  expect(t).toEqual(i);
});
test('Save game config', function () {
  var d = new GameConfiguration(0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.4, 0.3, 0.3, 0.8, 13000, 24000, 111111, 11111, 2222222, 333333, 4444444, 555555, 6666666, 777777, 55555, 4444444, 33333, 2222222, 33333, 5555, 2222222, 333333, 4444444, 555555, 6666666, 777777, 55555, 4444444, 33333, 2222222, 33333, 5554444);
  var h = k.saveConfig(d);
  expect(h).toBeUndefined();
});