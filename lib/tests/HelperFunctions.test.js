"use strict";

var Kalkulin = require('./../Kalkulin');

var k = new Kalkulin();
test('Days', function () {
  var r = k.getDaysInMonth();
  expect(r).toBe(30);
});
test('Date', function () {
  var r = k.getLastMonth();
  expect(r).not.toBeUndefined();
});
test('Date', function () {
  var r = k.getNextMonth();
  expect(r).not.toBeUndefined();
});