"use strict";

var Kalkulin = require('./../Kalkulin');

var k = new Kalkulin();
test('monthly revenue done should be greather than 0', function () {
  var res = k.calculateMonthlyRevDone(1, 1, 2020);
  expect(res).toBeGreaterThan(0);
});
test('monthly acc revenue done should be greather than 0', function () {
  var res = k.calculateMonthlyAccRevDone(1, 1, 2020);
  expect(res).toBeGreaterThan(0);
});
test('monthly oth revenue done should be greather than 0', function () {
  var res = k.calculateMonthlyOthRevDone(1, 1, 2020);
  expect(res).toBeGreaterThan(0);
});
test('monthly revenue sold should be greather than 0', function () {
  var res = k.calculateMonthlyRevSold(1, 1, 2020);
  expect(res).toBeGreaterThan(0);
});
test('monthly acc revenue sold should be greather than 0', function () {
  var res = k.calculateMonthlyAccRevSold(1, 1, 2020);
  expect(res).toBeGreaterThan(0);
});
test('monthly oth revenue sold should be greather than 0', function () {
  var res = k.calculateMonthlyOthRevSold(1, 1, 2020);
  expect(res).toBeGreaterThan(0);
});