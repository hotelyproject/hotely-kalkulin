"use strict";

var Kalkulin = require('./../Kalkulin');

var k = new Kalkulin();
test('List null', function () {
  expect(k.getList(-1)).toBeNull();
});
test('Demand null', function () {
  expect(k.getDemand(-1)).toBeNull();
});
test('Facality null', function () {
  expect(k.getFacility(-1)).toBeNull();
});
test('FoodMaterial null', function () {
  expect(k.getFoodMaterial(-1)).toBeNull();
});
test('HRAS null', function () {
  expect(k.getHRAS(-1)).toBeNull();
});
test('getHR null', function () {
  expect(k.getHR(-1)).toBeNull();
});
test('Promotion null', function () {
  expect(k.getPromotion(-1)).toBeNull();
});
test('RoomMaterial null', function () {
  expect(k.getRoomMaterial(-1)).toBeNull();
});
test('Room null', function () {
  expect(k.getRoom(-1)).toBeNull();
});