"use strict";

var Kalkulin = require('./../Kalkulin');

var Demand = require('./../models/Demand');

var Facility = require('./../models/Facility.js');

var FoodMaterial = require('./../models/FoodMaterialQuality.js');

var PotentialHR = require('./../models/PotentialHR.js');

var PotentialHRAS = require('./../models/PotentialHRAS.js');

var Promotion = require('./../models/Promotion.js');

var RoomMaterial = require('./../models/RoomMaterialQuality.js');

var Room = require('./../models/Room.js');

var Hotel = require('./../models/Hotel.js');

var GameConfiguration = require('./../GameConfiguration.js');

var k = new Kalkulin();
test('List contain', function () {
  var cycle = k.getList(2);
  expect(cycle[0]).toContain(80);
});
test('Get demand', function () {
  var d = new Demand(1, 1, "User New 1", 1, 17, 9, 2019, 16, false, true, 0.7, false);
  var demand = k.getDemand(1);
  expect(demand).toEqual(d);
});
test('Get facility', function () {
  var i = new Facility(1, 2, 1, 2);
  var t = k.getFacility(1);
  expect(t).toEqual(i);
});
test('Get FoodMaterial', function () {
  var i = new FoodMaterial(1, 2, 1);
  var t = k.getFoodMaterial(1);
  expect(t).toEqual(i);
});
test('Get PotentialHR', function () {
  var i = new PotentialHR(1, 1, 2, 2, 1, 2);
  var t = k.getHR(1);
  expect(t).toEqual(i);
});
test('Get PotentialHRAS', function () {
  var i = new PotentialHRAS(1, 1, 2, 3, 2);
  var t = k.getHRAS(1);
  expect(t).toEqual(i);
});
test('Get Promotion', function () {
  var i = new Promotion(1, 2, 1);
  var t = k.getPromotion(1);
  expect(t).toEqual(i);
});
test('Get RoomMaterial', function () {
  var i = new RoomMaterial(1, 2, 1);
  var t = k.getRoomMaterial(1);
  expect(t).toEqual(i);
});
test('Get Room', function () {
  var i = new Room(1, 1, null, 5, 3, 0.5);
  var t = k.getRoom(1);
  expect(t).toEqual(i);
});
test('getNumberOfRooms', function () {
  var t = k.getNumberOfRooms(1);
  expect(t).toBe(80);
});
test('getPlayerRoomsByOccupancy', function () {
  var t = k.getPlayerRoomsByOccupancy(2);
  expect(t).toEqual({
    "emptyRooms": [],
    "fullRooms": [new Room(15, 2, 9, 14, 4, 0.7), new Room(16, 2, 10, 10, 4, 0.7)]
  });
});
test('getRoomsByDemand', function () {
  var t = k.getRoomsByDemand(new Demand(1, 1, "User New 1", 1, 17, 9, 2019, 16, false, true, 0.7, false));
  expect(t).toEqual([]);
});
test('getPlayersRooms', function () {
  var t = k.getPlayersRooms(2);
  expect(t).toEqual([new Room(15, 2, 9, 14, 4, 0.7), new Room(16, 2, 10, 10, 4, 0.7)]);
});
test('getPlayerRoomMaterial', function () {
  var t = k.getPlayerRoomMaterial(2);
  expect(t).toEqual(new RoomMaterial(1, 2, 1));
});
test('getPlayerPromotion', function () {
  var t = k.getPlayerPromotion(2);
  expect(t).toEqual(new Promotion(1, 2, 1));
});
/*
test('getPlayerHRs', () => {
 const t = k.getPlayerHRs(2);
 expect(t).toEqual([new PotentialHR(2,2,3,2,2,2)]);
});/*
test('getPlayerHRAS', () => {
 const t = k.getPlayerHRAS(2);
 expect(t).toEqual([new PotentialHRAS(2,2,2,2,2)]);
});*/

test('getPlayerFoodMaterial', function () {
  var t = k.getPlayerFoodMaterial(1);
  expect(t).toEqual(new FoodMaterial(0, 1, 0.5));
});
test('getPlayerFacility', function () {
  var t = k.getPlayerFacility(1);
  expect(t).toEqual(new Facility(0, 1, 0.44, 0.8));
});
test('getNumberOfFreeRooms', function () {
  var t = k.getNumberOfFreeRooms(1, 10, 11);
  expect(t).toBeGreaterThan(0);
});
test('getDemandsInMonth', function () {
  var t = k.getDemandsInMonth(1, 10, 2019);
  expect(t).toEqual([new Demand(5, 1, "User New 2", 2, 24, 10, 2019, 9, true, true, 0.7, true)]);
});
/*
test('getAcceptedDemands', () => {
    const t = k.getAcceptedDemands(1,10,2019);
    expect(t).toEqual(new Demand(5,1,"User New 2",2,24,10,2019,9,true,true,true));
});*/

test('getResults', function () {
  var result = {
    equipQuality: 1,
    materialQuality: 0.75,
    hrPotential: 36.737712600388654,
    offerAtract: 0.64,
    productsPortfolio: 0,
    offerAccesibility: 1,
    guestSatisfaction: 50.609640750485816,
    productsQuality: 19.243856300194327
  };
  var t = k.getResults(1);
  expect(t).toEqual(result);
});
test('getAllPlayerResults', function () {
  var t = k.getAllPlayerResults(1);
  expect(t).not.toBeUndefined();
});
test('getResultsFromDb', function () {
  var result = {
    "id": 0,
    "userId": 1,
    "month": 10,
    "year": 2019,
    "value": {
      "equipQuality": 1,
      "materialQuality": 0.75,
      "hrPotential": 119.67886019822639,
      "offerAtract": 0.94,
      "productsPortfolio": 1,
      "offerAccesibility": 1,
      "guestSatisfaction": 154.28607524778297,
      "productsQuality": 60.714430099113194
    }
  };
  var t = k.getResultsFromDb(1, 10, 2019);
  expect(t).toEqual(result);
});
test('Get Hotel', function () {
  var d = new Hotel(0, 1);
  var h = k.getHotel(0);
  expect(h).toEqual(d);
});
test('Get game config', function () {
  var d = new GameConfiguration(0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.4, 0.3, 0.3, 0.8, 13000, 24000, 111111, 11111, 2222222, 333333, 4444444, 555555, 6666666, 777777, 55555, 4444444, 33333, 2222222, 33333, 5555, 2222222, 333333, 4444444, 555555, 6666666, 777777, 55555, 4444444, 33333, 2222222, 33333, 5554444);
  var h = k.getConfig(d);
  expect(h).toEqual(d);
});