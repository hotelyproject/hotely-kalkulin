"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var DatabaseHandler = require('./DatabaseHandler.js');

var FoodMaterial = require('./models/FoodMaterialQuality');

var FoodMaterialFactory =
/*#__PURE__*/
function () {
  function FoodMaterialFactory() {
    _classCallCheck(this, FoodMaterialFactory);

    this.db = new DatabaseHandler();
  }
  /*
  * Find FoodMaterial
  *
  * @param {number} id    id of FoodMaterial
  *
  * @return {FoodMaterial} return selected FoodMaterial.
  */


  _createClass(FoodMaterialFactory, [{
    key: "get",
    value: function get(id) {
      var m = this.db.getFoodMaterial(id);
      return new FoodMaterial(m.id, m.level, m.breakfastCost, m.groupDinnerCost, m.alaCarteCost, m.lobbyBarCost);
    }
    /*
     * Save or upadate FoodMaterial
     *
     * @param {FoodMaterial}     FoodMaterial
     *
     * @return {} return 
     */

  }, {
    key: "save",
    value: function save(material) {
      this.db.saveFoodMaterial(material);
    }
  }]);

  return FoodMaterialFactory;
}();

module.exports = FoodMaterialFactory;