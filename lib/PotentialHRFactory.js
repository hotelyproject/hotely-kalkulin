"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var DatabaseHandler = require('./DatabaseHandler.js');

var PotentialHR = require('./models/PotentialHR.js');

var PotentialHRFactory =
/*#__PURE__*/
function () {
  function PotentialHRFactory() {
    _classCallCheck(this, PotentialHRFactory);

    this.db = new DatabaseHandler();
  }
  /*
  * Find HR
  *
  * @param {number} id    id of HR
  *
  * @return {potentialHR} return selected HR.
  */


  _createClass(PotentialHRFactory, [{
    key: "get",
    value: function get(id) {
      var h = this.db.getHR(id);
      return new PotentialHR(h.id, h.position, h.type, h.count, h.wage);
    }
    /*
     * Save or upadate HR
     *
     * @param {potentialHR}     HR
     *
     * @return {} return 
     */

  }, {
    key: "save",
    value: function save(hr) {
      this.db.saveHR(hr);
    }
  }]);

  return PotentialHRFactory;
}();

module.exports = PotentialHRFactory;