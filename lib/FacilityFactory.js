"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var DatabaseHandler = require('./DatabaseHandler.js');

var Facility = require('./models/Facility.js');

var FacilityFactory =
/*#__PURE__*/
function () {
  function FacilityFactory() {
    _classCallCheck(this, FacilityFactory);

    this.db = new DatabaseHandler();
  }
  /*
  * Find facility
  *
  * @param {number} id    id of facility
  *
  * @return {facility} return selected facility.
  */


  _createClass(FacilityFactory, [{
    key: "get",
    value: function get(id) {
      var f = this.db.getFacility(id);
      return new Facility(f.id, f.investment, f.level);
    }
    /*
     * Save or upadate facility
     *
     * @param {facility}     facility
     *
     * @return {} return 
     */

  }, {
    key: "save",
    value: function save(facility) {
      this.db.saveFacility(facility);
    }
  }]);

  return FacilityFactory;
}();

module.exports = FacilityFactory;