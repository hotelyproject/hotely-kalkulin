"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var low = require('lowdb');

var FileSync = require('lowdb/adapters/FileSync');

var DatabaseHandler =
/*#__PURE__*/
function () {
  function DatabaseHandler() {
    var file = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'db.json';

    _classCallCheck(this, DatabaseHandler);

    var adapter = new FileSync(file);
    this.db = low(adapter); // Set some defaults (required if your JSON file is empty)

    this.db.defaults({
      lists: [],
      demands: [],
      facilities: [],
      hr: [],
      promotions: [],
      foodMaterials: [],
      roomMaterials: [],
      countLists: 0,
      listsLastId: 0,
      countDemands: 0,
      demandsLastId: 0,
      countFacilities: 0,
      facilitiesLastId: 0,
      countHr: 0,
      hrLastId: 0,
      countPromotion: 0,
      promotionLastId: 0,
      countFoodMaterial: 0,
      foodMaterialLastId: 0,
      countRoomMaterial: 0,
      roomMaterialLastId: 0
    }).write();
  } // LONG CYCLE **************************************************


  _createClass(DatabaseHandler, [{
    key: "saveList",
    value: function saveList() {
      var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.db.get('listsLastId').value();
      var item = arguments.length > 1 ? arguments[1] : undefined;

      if (this.getList(id)) {
        this._updateList(id, item);

        console.log('List updated');
      } else {
        this.db.update('listsLastId', function (n) {
          return n + 1;
        }).write();
        this.db.get('lists').push({
          id: id,
          value: item
        }).write();
        this.db.update('countLists', function (n) {
          return n + 1;
        }).write();
        console.log('List inserted into db, id: ' + id);
      }
    }
  }, {
    key: "getList",
    value: function getList(id) {
      return this.db.get('lists').find({
        id: id
      }).value().value;
    }
  }, {
    key: "_updateList",
    value: function _updateList(id, item) {
      this.db.get('lists').find({
        id: id
      }).assign({
        value: item
      }).write();
    }
  }, {
    key: "_removeList",
    value: function _removeList(id) {
      this.db.get('lists').remove({
        id: id
      }).write();
      this.db.update('countLists', function (n) {
        return n - 1;
      }).write();
      console.log('List removed from db');
    } // DEMANDS **************************************************

  }, {
    key: "saveDemand",
    value: function saveDemand(demand) {
      if (this.getDemand(demand.getId())) {
        this._updateDemand(demand.getId(), demand);

        console.log('Demand updated');
        return this.getDemand(demand.getId());
      } else {
        var id = this.db.get('demandsLastId').value();
        this.db.update('demandsLastId', function (n) {
          return n + 1;
        }).write();
        this.db.get('demands').push({
          id: id,
          name: demand.getName(),
          numberOfBeds: demand.getNumberOfBeds(),
          room: demand.getRoomNumber(),
          startDay: demand.getStartDay(),
          startMonth: demand.getStartMonth(),
          period: demand.getPeriod(),
          accepted: demand.getAccepted(),
          userId: demand.getUserId()
        }).write();
        this.db.update('countDemands', function (n) {
          return n + 1;
        }).write();
        console.log('Demand inserted into db, id: ' + id);
        return {
          id: id,
          name: demand.getName(),
          numberOfBeds: demand.getNumberOfBeds(),
          room: demand.getRoomNumber(),
          startDay: demand.getStartDay(),
          startMonth: demand.getStartMonth(),
          period: demand.getPeriod(),
          accepted: demand.getAccepted(),
          userId: demand.getUserId()
        };
      }
    }
  }, {
    key: "getDemand",
    value: function getDemand(id) {
      try {
        return this.db.get('demands').find({
          id: id
        }).value();
      } catch (error) {
        console.log(error);
      }

      console.log('db/getDemand return false');
      return false;
    }
  }, {
    key: "getAcceptedDemands",
    value: function getAcceptedDemands() {
      return this.db.get('demands').filter({
        accepted: true
      }).value();
    }
  }, {
    key: "_updateDemand",
    value: function _updateDemand(id, demand) {
      console.log('updateDemand');
      this.db.get('demands').find({
        id: id
      }).assign({
        name: demand.getName(),
        numberOfBeds: demand.getNumberOfBeds(),
        room: demand.getRoomNumber(),
        startDay: demand.getStartDay(),
        startMonth: demand.getStartMonth(),
        period: demand.getPeriod(),
        accepted: demand.getAccepted(),
        userId: demand.getUserId()
      }).write();
    }
  }, {
    key: "_removeDemand",
    value: function _removeDemand(id) {
      if (getDemand(id)) {
        this.db.get('demands').remove({
          id: id
        }).write();
        this.db.update('countDemands', function (n) {
          return n - 1;
        }).write();
        console.log('Demand removed from db');
      } else {
        console.log('Error demand doesnt exists');
      }
    } // Facility ***********************************

  }, {
    key: "saveFacility",
    value: function saveFacility(facility) {
      if (this.getFacility(facility.getId())) {
        this._updateFacility(facility.getId(), facility);

        console.log('Facility updated');
      } else {
        var id = this.db.get('facilitiesLastId').value();
        this.db.update('facilitiesLastId', function (n) {
          return n + 1;
        }).write();
        this.db.get('facilities').push({
          id: id,
          level: facility.getLevel(),
          investment: facility.getInvestment()
        }).write();
        this.db.update('countFacilities', function (n) {
          return n + 1;
        }).write();
        console.log('Facility inserted into db, id: ' + id);
      }
    }
  }, {
    key: "getFacility",
    value: function getFacility(id) {
      try {
        return this.db.get('facilities').find({
          id: id
        }).value();
      } catch (error) {
        console.log(error);
      }

      console.log('db/getFacility return: false');
      return false;
    }
  }, {
    key: "_updateFacility",
    value: function _updateFacility(id, facility) {
      console.log('updateFacility');
      this.db.get('facilities').find({
        id: id
      }).assign({
        level: facility.getLevel(),
        investment: facility.getInvestment()
      }).write();
    } // _removeFacalityById(id)
    // PotentialHR *********************************

  }, {
    key: "saveHR",
    value: function saveHR(hr) {
      if (this.getHR(hr.getId())) {
        this._updateHR(hr.getId(), hr);

        console.log('HR updated');
      } else {
        var id = this.db.get('hrLastId').value();
        this.db.update('hrLastId', function (n) {
          return n + 1;
        }).write();
        this.db.get('hr').push({
          id: id,
          position: hr.getPosition(),
          type: hr.getType(),
          count: hr.getCount(),
          wage: hr.getWage()
        }).write();
        this.db.update('countHr', function (n) {
          return n + 1;
        }).write();
        console.log('HR inserted into db, id: ' + id);
      }
    }
  }, {
    key: "getHR",
    value: function getHR(id) {
      try {
        return this.db.get('hr').find({
          id: id
        }).value();
      } catch (error) {
        console.log(error);
      }

      console.log('db/getHR return: false');
      return false;
    }
  }, {
    key: "_updateHR",
    value: function _updateHR(id, hr) {
      console.log('updateHR');
      this.db.get('hr').find({
        id: id
      }).assign({
        position: hr.getPosition(),
        type: hr.getType(),
        count: hr.getCount(),
        wage: hr.getWage()
      }).write();
    } // removeHRById(id)
    // Promotion ****************************

  }, {
    key: "savePromotion",
    value: function savePromotion(promotion) {
      if (this.getPromotion(promotion.getId())) {
        this._updatePromotion(promotion.getId(), promotion);

        console.log('Promotion updated');
      } else {
        var id = this.db.get('promotionLastId').value();
        this.db.update('promotionLastId', function (n) {
          return n + 1;
        }).write();
        this.db.get('promotions').push({
          id: id,
          investment: promotion.getInvestment()
        }).write();
        this.db.update('countPromotion', function (n) {
          return n + 1;
        }).write();
        console.log('Promotion inserted into db, id: ' + id);
      }
    }
  }, {
    key: "getPromotion",
    value: function getPromotion(id) {
      try {
        return this.db.get('promotions').find({
          id: id
        }).value();
      } catch (error) {
        console.log(error);
      }

      console.log('db/getPromotion return: false');
      return false;
    }
  }, {
    key: "_updatePromotion",
    value: function _updatePromotion(id, promotion) {
      console.log('updatePromotion');
      this.db.get('promotions').find({
        id: id
      }).assign({
        investment: promotion.getInvestment()
      }).write();
    } // removePromotionById(id)
    // FoodMaterialQuality *********************

  }, {
    key: "saveFoodMaterial",
    value: function saveFoodMaterial(material) {
      if (this.getFoodMaterial(material.getId())) {
        this._updateFoodMaterial(material.getId(), material);

        console.log('FoodMaterial updated');
      } else {
        var id = this.db.get('foodMaterialLastId').value();
        this.db.update('foodMaterialLastId', function (n) {
          return n + 1;
        }).write();
        this.db.get('foodMaterials').push({
          id: id,
          level: material.getLevel(),
          breakfastCost: material.getBreakfastCost(),
          groupDinnerCost: material.getGroupDinnerCost(),
          alaCarteCost: material.getAlaCarteCost(),
          lobbyBarCost: material.getLobbtBarCost()
        }).write();
        this.db.update('countFoodMaterial', function (n) {
          return n + 1;
        }).write();
        console.log('FoodMaterial inserted into db, id: ' + id);
      }
    }
  }, {
    key: "getFoodMaterial",
    value: function getFoodMaterial(id) {
      try {
        return this.db.get('foodMaterials').find({
          id: id
        }).value();
      } catch (error) {
        console.log(error);
      }

      console.log('db/getFoodMaterial return: false');
      return false;
    }
  }, {
    key: "_updateFoodMaterial",
    value: function _updateFoodMaterial(id, material) {
      console.log('updateFoodMateria');
      this.db.get('foodMaterials').find({
        id: id
      }).assign({
        level: material.getLevel(),
        breakfastCost: material.getBreakfastCost(),
        groupDinnerCost: material.getGroupDinnerCost(),
        alaCarteCost: material.getAlaCarteCost(),
        lobbyBarCost: material.getLobbtBarCost()
      }).write();
    } // removeFoodMaterial(id)
    // RoomMaterial ********************************

  }, {
    key: "saveRoomMaterial",
    value: function saveRoomMaterial(material) {
      if (this.getRoomMaterial(material.getId())) {
        this._updateRoomMaterial(material.getId(), material);

        console.log('RoomMaterial updated');
      } else {
        var id = this.db.get('roomMaterialLastId').value();
        this.db.update('roomMaterialLastId', function (n) {
          return n + 1;
        }).write();
        this.db.get('roomMaterials').push({
          id: id,
          level: material.getLevel(),
          cost: material.getCost()
        }).write();
        this.db.update('countRoomMaterial', function (n) {
          return n + 1;
        }).write();
        console.log('RoomMaterial inserted into db, id: ' + id);
      }
    }
  }, {
    key: "getRoomMaterial",
    value: function getRoomMaterial(id) {
      try {
        return this.db.get('roomMaterials').find({
          id: id
        }).value();
      } catch (error) {
        console.log(error);
      }

      console.log('db/getRoomMaterial return: false');
      return false;
    }
  }, {
    key: "_updateRoomMaterial",
    value: function _updateRoomMaterial(id, material) {
      console.log('updateRoomMaterial');
      this.db.get('roomMaterials').find({
        id: id
      }).assign({
        level: material.getLevel(),
        cost: material.getCost()
      }).write();
    } // removeRoomMaterial(id)
    // Hotel ********************************
    // todo

  }]);

  return DatabaseHandler;
}();

module.exports = DatabaseHandler;