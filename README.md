# Kalkulin docs #

Engine use lowdb database.

### How it works ###
Hotel - has monthly configuration for hotel
Demand - is accepted by hotel

## Funcs ##
`getCycle(hotelId, date)` - select big table
`saveCycle(hotelId, date, cycle)` - save/update big table

`getAcceptedDemands(hotelId, date)` return accepted demands for hotel in month
`acceptDemand(hotelId, demand)` Set demand accepted, update demand in db and update hotel cycle
`generateDemands(date, range)` generate new demands and save them in db

`getResults(hotelId, date)` Return all results for hotel

#### Kalkulin ####
Class Kalkulin has following functionality:

##### Game configuration #####

Class GameConfiguration manage all admin confiration.

{
    cHRASRec,           //koeficienty upravující váhu HR potenciálu recepce a housekeepingu v celkovém HR potenciálu ubytovacích služeb
    cHRASHous,          //koeficienty upravující váhu HR potenciálu recepce a housekeepingu v celkovém HR potenciálu ubytovacích služeb
    cHRAS,              //koeficienty upravující váhu HR potenciálu jednotlivých typů služeb v celkovém HR potenciálu hotelu
    cHRFB,              //koeficienty upravující váhu HR potenciálu jednotlivých typů služeb v celkovém HR potenciálu hotelu
    chrfbBreakKitchen,  //koeficienty upravující váhu HR potenciálů stravovacích služeb během snídaní (break) v kuchyni (kitchen) a v restauraci (rest) v celkovém HR potenciálu stravovacích služeb během snídaní
    chrfbBreakRest,     //koeficienty upravující váhu HR potenciálů stravovacích služeb během snídaní (break) v kuchyni (kitchen) a v restauraci (rest) v celkovém HR potenciálu stravovacích služeb během snídaní
    chrfbBreak,         //koeficienty upravující váhu HR potenciálu stravovacích služeb během snídaní (break) a skupinových večeří (group) v celkovém HR potenciálu stravovacích služeb
    chrfbGroup,         //koeficienty upravující váhu HR potenciálu stravovacích služeb během snídaní (break) a skupinových večeří (group) v celkovém HR potenciálu stravovacích služeb
    cpqhr,              //koeficienty upravující váhy HR potenciálu, Kvality surovin a Kvality vybavení hotelu na celkovém vnímání Kvality služeb hotelu
    cpqmq,              //koeficienty upravující váhy HR potenciálu, Kvality surovin a Kvality vybavení hotelu na celkovém vnímání Kvality služeb hotelu
    cpqeq,              //koeficienty upravující váhy HR potenciálu, Kvality surovin a Kvality vybavení hotelu na celkovém vnímání Kvality služeb hotelu
    chrfbGroupKitchen,  //koeficienty upravující váhu HR potenciálů stravovacích služeb během skupinových večeří (group) v kuchyni (kitchen) a v restauraci (rest) v celkovém HR potenciálu stravovacích služeb během skupinových večeří
    chrfbGroupRest,     //koeficienty upravující váhu HR potenciálů stravovacích služeb během skupinových večeří (group) v kuchyni (kitchen) a v restauraci (rest) v celkovém HR potenciálu stravovacích služeb během skupinových večeří
    cMQAS,              //koeficienty upravující váhu kvality surovin použitých na ubytovacím a stravovacím úseku hotelu
    cMQFB,              //koeficienty upravující váhu kvality surovin použitých na ubytovacím a stravovacím úseku hotelu
    coaeq,              //koeficienty upravující váhy Kvality vybavení, Portfolia služeb a Úrovně značky hotelu na celkovém vnímání Atraktivity nabídky hotelu
    coapp,              //koeficienty upravující váhy Kvality vybavení, Portfolia služeb a Úrovně značky hotelu na celkovém vnímání Atraktivity nabídky hotelu
    coabl,              //koeficienty upravující váhy Kvality vybavení, Portfolia služeb a Úrovně značky hotelu na celkovém vnímání Atraktivity nabídky hotelu
    brandLevel,         //Úroveň značky hotelu; nastavuje správce hry, může nabývat hodnot v rozmezí 0 (bídná) až 1 (vynikající)
    juniorReceptionWage,            //mzda juniora na recepci
    seniorReceptionWage,            //mzda seniora na recepci
    juniorRoomServiceWage,          //mzda juniora housekeeping
    seniorRoomServiceWage,          //mzda seniora housekeeping
    juniorBreakfastChefWage,        //mzda juniorního kuchaře na snídaních
    seniorBreakfastChefWage,        //mzda seniorního kuchaře na snídaních
    helpBreakfastWage,              //mzda pomocné síly v kuchyni na snídaních
    juniorBreakfastWaiterWage,      //mzda juniorního číšníka na snídaních
    seniorBreakfastWaiterWage,      //mzda seniorního číšníka na snídaních
    juniorDinnerChefWage,           //mzda juniorního kuchaře na večeři
    seniorDinnerChefWage,           //mzda seniorního kuchaře na večeři
    helpDinnerWage,                 //mzda pomocné síly v kuchyni na večeři
    juniorDinnerWaiterWage,         //mzda juniorního číšníka na večeři
    seniorDinnerWaiterWage,         //mzda seniorního číšníka na večeři
    roomQualityStandardCost,        //náklady RoomMaterial standard
    roomQualityPremiumCost,         //náklady RoomMaterial premium
    foodQualityEconomyBreakfastCost,//náklady na snídaně economy
    foodQualityEconomyDinnerCost,   //náklady na večeře economy
    foodQualityEconomyAlaCarteCost, //náklady na a la carte economy
    foodQualityEconomyBarCost,      //náklady na bar economy
    foodQualityStandardBreakfastCost,//náklady na snídaně standard
    foodQualityStandardDinnerCost,  //náklady na večeře standard
    foodQualityStandardAlaCarteCost,//náklady na a la carte standard
    foodQualityStandardBarCost,     //náklady na bar standard
    foodQualityPremiumBreakfastCost,//náklady na snídaně premium
    foodQualityPremiumDinnerCost,   //náklady na večeře premium
    foodQualityPremiumAlaCarteCost, //náklady na a la carte premium
    foodQualityPremiumBarCost,      //náklady na bar premium
}

#### Get ####
`getConfig()` - Returns game configuration.

#### Save ####
`saveConfig(config)` - Save or update.


##### Cycle table #####

`saveCycle(hotelId, date, cycle)`
Param hotelId [number]
Param date [object] {month, year}
Param cycle [array]

##### Getting cycle from db #####
`getCycle(hotelId, date)`
Param hotelId [number]
Param date [object] {month, year}
Returns 2d list

`getGreenTableRooms(hotelId, date)`
Param date [object] {month, year}
Returns object with arrays for month
{
    roomStatus - arrays [Total room count, sold rooms, remained rooms]
    detailRoomStatus - arrays [standard rooms sold, standard rooms remain, better rooms sold, better rooms remain]
}


##### Demand #####
{
  id = null,
  hotelId = null,
  numberOfGuests,             // počet hostů
  numberOfRooms = 1,          // počet pokojů
  reservationDate = {         // datum rezervace
    day: 0,
    month: 0,
    year: 0
  },
  startDate = {               // datum nástupu
    day: 0,
    month: 0,
    year: 0
  },
  period,                     // doba pobytu
  accepted = false,           // akceptovaná poptávka
  stornoDate = null,          // datum storna
  price = 0,                  // cena
  type,                       // typ
  requirements = {              // podmínky poptávky
    maxPrice: 0,                // maximalni cena
    standardRoom: false,        // standartní pokoj (ano/ne)
    premiumRoom: false,         // prémium pokoj (ano/ne)
    connectedRoom: false,       // spojené pokoje (ano/ne)
    breakfast: false,           // snídaně (ano/ne)
    fullBoard: false,           // polopenze/plná penze (ano/ne)
    alaCarte: false,            // a la carte restaurace (ano/ne)
    kidsMenu: false,            // dětské menu/vybavení pro děti (ano/ne)
    roomService: false,         // room service (ano/ne)
    parking: false,             // parkování (ano/ne)
    busParking: false,          // parkování pro autobus (ano/ne)
    shuttle: false,             // taxi/shuttle (ano/ne)
    wellnes: false,             // wellnes a masáže (ano/ne)
    anotherServices: false,     // další služby (ano/ne)
    conferenceSpace: false,     // konferenční prostory (ano/ne)
    raut: false,                // raut/slavnostní oběd (ano/ne)
    bar: false,                 // bar (ano/ne)
  }
}

#### Get ####
`getDemand(id)`
Param id [number]
Return demand object

`getAllDemands(hotelId)` return all demands for hotel
Param hotelId [number]
Return [array]

`getAcceptedDemands(hotelId, date)` return accepted demands in month
Param hotelId [number]
Param date [object] {month, year}
Return [array]

`getAcceptedDemandsInDay(hotelId, date)`
Param hotelId [number]
Param date [object] {day, month, year}
Return [array]

`getDemandsInMonth(hotelId, date)` return all demands in selected month
Return [array]

#### Save ####

`saveDemand(demand)`
Param demand [object]

#### Other ####

`acceptDemand(hotelId, demand)` Set demand accepted, update in db and update cycle table
Param hotelId [number]
Param demand [object]
Return demand [object]

`generateDemands(date, range)` generate new demands and save it
Param date [object] {day, month, year}
Param range [number]
Return [array] demands

##### Hotel #####
Class for monthl hotel configuration
{
    hotelId = null,
    month = null,
    year = null,
    facilityInvestment = 0,       // investive do vybaveni hotelu 0.01/0.02/0.03
    promotionInvestment = 0,      // investice do propagace
    foodMaterialQuality = 0.5,    // accomodation - economy: 0.5, standard: 0.7, premium: 1
    roomMaterialQuality = 0.7,    // kvalita ubytovani - standard: 0.7, premium: 1
    hr = {
      as: {              // accomodation service
        reception: {     // pracovnici recepce
          junior: 0,
          senior: 0,
        },
        housekeeping: {  // pracovnici uklidu
          junior: 0,
          senior: 0,
        }
      },
      fb: {              // pracovnici restaurace
        breakfast: {     // snidane
          kitchen: {     // kuhari
            junior: 0,
            senior: 0,
            auxiliary: 0
          },              // cisnici
          restaurant: {
            junior: 0,
            senior: 0
          }
        },
        dinner: {        // vecere
          kitchen: {     // kuchari
            junior: 0,
            senior: 0,
            auxiliary: 0
          },              // cisnici
          restaurant: {
            junior: 0,
            senior: 0
          }
        }
      }
    },
    rooms = {                     // pocet jednotlivych pokoju
      standard: 60,
      premium: 20,
    },
    equipment = {
      standardRoom: false,        // standartní pokoj (ano/ne)
      premiumRoom: false,         // prémium pokoj (ano/ne)
      connectedRoom: false,       // spojené pokoje (ano/ne)
      breakfast: false,           // snídaně (ano/ne)
      fullBoard: false,           // polopenze/plná penze (ano/ne)
      alaCarte: false,            // a la carte restaurace (ano/ne)
      kidsMenu: false,            // dětské menu/vybavení pro děti (ano/ne)
      roomService: false,         // room service (ano/ne)
      parking: false,             // parkování (ano/ne)
      busParking: false,          // parkování pro autobus (ano/ne)
      shuttle: false,             // taxi/shuttle (ano/ne)
      wellnes: false,             // wellnes a masáže (ano/ne)
      anotherServices: false,     // další služby (ano/ne)
      conferenceSpace: false,     // konferenční prostory (ano/ne)
      raut: false,                // raut/slavnostní oběd (ano/ne)
      bar: false,                 // bar (ano/ne)
    }
}

#### Get ####

`getHotel(hotelId, date)`
Param hotelId [number]
Param date [object] {month, year}
Return [hotel]

#### Save ####

`saveHotel(hotel)`
Param hotel [object]

##### Rate #####
`occupancyRate(hotelId, date)` calculate % of occupancy.
Param hotelId [number]
Param date [object] {month, year}
Returns [number].

`averageDailyRate(hotelId, date)` calculate average price of sold room.
Param hotelId [number]
Param date [object] {month, year}
Returns [number].

`calculateRevenue(hotelId, date)` calculate average cost per room. todo
Param hotelId [number]
Param date [object] {month, year}
Returns [object]
{
  revenuePerAvalilableRoom: [number],
  totalRevenuePerAvalilableRoom: [number]
}

##### Potential functions #####

`calculateEquipQuality(hotelId, date)`
`calculateMaterialQuality(hotelId, date)`
`calculateHRPotential(hotelId, date)` todo control
`calculateOfferAtract(hotelId, date)` todo
`calculateProductsPortfolio(hotelId, date)`
`calculateOfferAccesibility(hotelId, date)`
`calculateQuestSatisfaction(hotelId, date)` todo
`calculateProductsQuality(hotelId, date)`

`getResults(hotelId, date)` Return all results above
Param hotelId [number]
Param date [object] {month, year}
Return [object]
{
    equipQuality: [number],
    materialQuality: [number],
    hrPotential: [number],
    offerAtract: [number],
    productsPortfolio: [number],
    offerAccesibility: [number],
    guestSatisfaction: [number],
    productsQuality: [number],
}

##### Hotel Costs #####
todo
`calculateMonthlyHotelCosts(userId)` return all costs for month
`calculateMonthlyFixedCosts(userId)` return number
`calculateDailyDirectCosts(userId, day, month, year)` return number
`calculateMatCostsAS(userId, day, month, year)` return number
`calculateMatCostsFB(userId, day, month, year)`return number
`calculateMatCosts(userId, day, month, year)`return number
`calculateOtaCommission(userId)` todo
`calculateDailyCosts(userId)` return number
`calculateHRCosts(userId)` return number
`calculateCostEmplRec(userId)` return number
`calculateCostEmplHouse(userId)` return number
`calculateCostEmplBreak(userId)` return number
`calculateCostEmplGdinn(userId)` return number
`calculateCostEmplalcDinn(userId)` return number
`calculateCostEmplLb(userId)` return number
`calculateCostEmplOthers(userId)` return number
`calculateEquipCosts(userId)` return number
`calculatePromoCosts(userId)` return number
`calculateOtherServicesCosts(userId)` todo
`calculateFixedCosts(userId)`todo

##### Hotel Revenue #####
todo
`calculateMonthlyRevDone(userId, month, year)` - Calculate monthly revenue done
`calculateMonthlyAccRevDone(userId, month, year)` todo
`calculateMonthlyOthRevDone(userId, month, year)` todo
`calculateMonthlyRevSold(userId, month, year)` - Calculate monthly revenue sold
`calculateMonthlyAccRevSold(userId, month, year)` todo
`calculateMonthlyOthRevSold(userId, month, year)` todo

#### Other functinos ####

`generateTableForMonth(hotelId, date)`
Param hotelId [number]
Param date [object] {month, year}
Returns default 2d [array]


todo
`setDailySettings(userId, day, month, year, settings)` - Settings like room price etc...
Param userId [number]
Param day [number] 0-29
Param settings [object]

`getDaysInMonth(date)`
Param date [object] {month, year}
Returns [number]

`getHRPositions()`
Returns [object]
{
    'restaurant': 1,
    'kitchen': 2
}

`getHRPlaces()`
Returns [object]
{
    'reception': 1,
    'housekeeping': 2,
    'breakfast': 3,
    'dinner': 4
}

`getHRTypes()`
Returns [object]
{
    'auxiliary': 1,
    'junior': 2,
    'senior': 3
}

`getHRLevels()`
Returns [object]
{
    'standard': 0.7,
    'premium': 1
}

`getLevels()` 
Returns [object]
{
    'economy': 0.5,
    'standard': 0.7,
    'premium': 1
}

`getRoomTypes()` 
Returns [object]
{
    'standard': 0.7,
    'premium': 1
}

`getgetRevenueTypes()`
return [object]
{
    acc: 1,
    oth: 2,
}

`hotelCostsTypes()`
return [object]
{
    hr: 1,
    material: 2,
    equip: 3,
    promotion: 4,
    other: 5,
    fix: 6,
}
