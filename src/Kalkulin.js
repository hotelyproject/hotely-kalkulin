const DemandDB = require('./database/DemandDB.js');
const CycleDB = require('./database/CycleDB.js');
const RoomDB = require('./database/RoomDB.js');
const ResultDB = require('./database/ResultDB.js');
const HotelDB = require('./database/HotelDB.js');
const ConfigDB = require('./database/ConfigDB.js');
const RevenueDB = require('./database/HotelRevenueDB');
const HotelCostsDB = require('./database/HotelCostsDB');

const Demand = require('./models/Demand.js');
const Room = require('./models/Room.js');
const Hotel = require('./models/Hotel.js');
const Revenue = require('./models/Revenue');
const HotelCosts = require('./models/HotelCosts');

const GameConfiguration = require('./GameConfiguration.js');

class Kalkulin {
  constructor(
    cycleDb = 'cycleDB.json',
    demandDb = 'demandDB.json',
    roomDb = 'RoomDB.json',
    resultDb = 'resultDB.json',
    hotelDb = 'hotelDB.json',
    configDb = 'gameConfigDB.json',
  ) {
    this.dbDemand = new DemandDB(demandDb);
    this.dbCycle = new CycleDB(cycleDb);
    this.dbRoom = new RoomDB(roomDb);
    this.dbResult = new ResultDB(resultDb);
    this.dbHotel = new HotelDB(hotelDb);
    this.dbConfig = new ConfigDB(configDb);
    this.dbRevenue = new RevenueDB('revenueDB.json');
    this.dbHotelCosts = new HotelCostsDB('costsDB.json');

    // indexes
    this.INDXSTANDARTROOMPRICE = 0;
    this.INDXBETTERROOMPRICE = 1;
    this.INDXPROVISIONOTA = 2;
    this.INDXSALEFORCOMPANYCLIENTS = 3;
    this.INDXSALEFORCOMPANYCLIENTSWITHACTION = 4;
    this.INDXSALECKINDIVIDUAL = 5;
    this.INDXSALECKGROUP = 6;
    this.INDXGRATUITE = 7;
    this.INDXADVANCENONREF = 8;
    this.INDXRELEASEPROADVANCE = 9;

    this.INDXTOTALROOMSCOUNT = 10;
    this.INDXROOMSSOLD = 11;
    this.INDXROOMSREMAIN = 12;
    this.INDXSOLDSTANDARTROOM = 13;
    this.INDXSOLDSTANDARTROOMREMAIN = 14;
    this.INDXSOLDBETTERROOM = 15;
    this.INDXSOLDBETTERROOMREMAIN = 16;

    this.INDXONLINEOFFERSTANDARTROOM = 17;
    this.INDXONLINEOFFERBETTERROOM = 18;
    this.INDXCLOSETOARRIVAL = 19;
    this.INDXCLOSETODEPARTURE = 20;
    this.INDXMINIMUMDAYOFSTAY = 21;
  }

  /**
   * 
   * @param {number} hotelId 
   * @param {object} date { month, year }
   * @param {array} cycle 
   */
  saveCycle(hotelId, date, cycle) {
    this.dbCycle.save(hotelId, date, cycle);
  }

  /**
   * 
   * @param {number} hotelId number
   * @param {object} date { month, year }
   * 
   * @returns {Array} cycle
   */
  getCycle(hotelId, date) {
    let cycle = this.dbCycle.get(hotelId, date);

    if (cycle) {
      return cycle.cycle;
    }
    
    cycle = this.generateTableForMonth(hotelId, date);
    this.saveCycle(hotelId, date, cycle);
    return cycle;
  }

  // Settings
  setDailySettings(userId, day, month, year, settings) {
    let list = this.getPlayerList(userId, month, year);
    const arr = [
      this.INDXSTANDARTROOMPRICE,
      this.INDXBETTERROOMPRICE,
      this.INDXPROVISIONOTA,
      this.INDXSALEFORCOMPANYCLIENTS,
      this.INDXSALEFORCOMPANYCLIENTSWITHACTION,
      this.INDXSALECKINDIVIDUAL,
      this.INDXSALECKGROUP,
      this.INDXGRATUITE,
      this.INDXADVANCENONREF,
      this.INDXRELEASEPROADVANCE,
      this.INDXONLINEOFFERSTANDARTROOM,
      this.INDXONLINEOFFERBETTERROOM,
      this.INDXCLOSETOARRIVAL,
      this.INDXCLOSETODEPARTURE,
      this.INDXMINIMUMDAYOFSTAY,
    ];

    let i = 0;
    for (const setting in settings) {
      if (settings[setting] != null || settings[setting] != undefined) {
        list[day][arr[i]] = settings[setting];
      }
      i += 1;
    }

    this.saveList(userId, month, year, list);
  }

  /**
   * Add rooms from demand to the cycle
   * 
   * @param {object} demand
   */
  addReservationToCycle(demand) {
    let startDate = demand.startDate;
    let cycle = this.getCycle(demand.hotelId, startDate);
    let actualDay = demand.startDate.day;

    for (let i = 0; i < demand.period; i++) {
      cycle[actualDay][this.INDXROOMSSOLD] += demand.numberOfRooms;
      cycle[actualDay][this.INDXROOMSREMAIN] -= demand.numberOfRooms;

      if (demand.requirements.standardRoom === true) { // standard room
        console.log('standard room');
        cycle[actualDay][this.INDXSOLDSTANDARTROOM] += demand.numberOfRooms;
        cycle[actualDay][this.INDXSOLDSTANDARTROOMREMAIN] -= demand.numberOfRooms;
      } else { // premium
        console.log('premium room');
        cycle[actualDay][this.INDXSOLDBETTERROOM] += demand.numberOfRooms;
        cycle[actualDay][this.INDXSOLDBETTERROOMREMAIN] -= demand.numberOfRooms;
      }

      actualDay += 1;

      if (actualDay >= cycle.length) {
        this.saveCycle(demand.hotelId, startDate, cycle);
        startDate = this.getNextMonth(startDate);
        cycle = this.getCycle(demand.hotelId, startDate);
        actualDay = 0;
      }
    }

    this.saveCycle(demand.hotelId, startDate, cycle);
  }

  // Demand ##############################################################

  /**
   * Find demand
   *
   * @param {number} id    id of demand
   *
   * @return {demand} return demand or null
   */
  getDemand(id) {
    return this.dbDemand.get(id);
  }

  /**
   * Find all demands for hotel
   *
   * @param {number} id    id of GroupDemand
   *
   * @return {GroupDemand} return GroupDemands.
   */
  getAllDemands(hotelId) {
    return this.dbDemand.getAllDemands(hotelId);
  }

  /**
   * Accept demand and save it, call method for updating cycle
   *
   * @param {number} hotelId   
   * @param {demnad} demand    demand
   *
   * @return {demand} return updated demand
   */
  acceptDemand(hotelId, demand) {
    if (demand.accepted) {
      console.log('Demand is already accepted.');
      return demand;
    }

    demand.hotelId = hotelId;
    demand.accepted = true;
    this.saveDemand(demand);
    this.addReservationToCycle(demand);

    return demand;
  }

  /**
   * Save Demand or update
   *
   * @param {Demand} demand     demand
   */
  saveDemand(demand) {
    this.dbDemand.save(demand);
  }

  /**
   * Find accepted demands in month
   *
   * @param {number} hotelId  id of hotel
   * @param {object} date     {month, year}
   *
   * @return {array}          array of all accepted demands in month.
   */
  getAcceptedDemands(hotelId, date) {
    return this.dbDemand.getAcceptedDemands(hotelId, date);
  }

  /**
   * Find accepted demands in selected day
   *
   * @param {number} hotelId    hotel ID
   * @param {object} date       { day, month, year }
   *
   * @return {array}
   */
  getAcceptedDemandsInDay(hotelId, date) {
    const demands = this.getAcceptedDemands(hotelId, date);

    const actualDemands = demands.filter(
      demand =>
        demand.startDay <= date.day &&
        demand.startDay + demand.getPeriod() >= day,
    );

    return actualDemands;
  }

  /**
   * Find all demands in selected month
   *
   * @param {number} hotelId  id of hotel
   * @param {number} date     { month, year }
   *
   * @return {array} return array of all demands.
   */
  getDemandsInMonth(hotelId, date) {
    return this.dbDemand.getDemandsInMonth(hotelId, date);
  }

  // todo OTA demand
  /**
   * Generate and save random demnads
   *
   * @param {object} date       {day, month, year}
   * @param {number} range      number of demands
   *
   * @return {array}            return array of demands
   */
  generateDemands(
    date,
    range,
  ) {
    let demands = [];
    for (let i = 0; i < range; i++) {
      const numberOfGuests = Math.floor(Math.random() * 5);
      const numberOfRooms = Math.floor(Math.random() * 2);
      const period = Math.floor(Math.random() * 10);

      let d = new Demand(
        null,
        null,
        numberOfGuests,
        numberOfRooms,
        date,
        date,
        period,
        false,
        null, //storno date
        0,
        0,
        {
          maxPrice: 0,
          standardRoom: true,
          premiumRoom: false,
          connectedRoom: false,
          breakfast: false,
          fullBoard: false,
          alaCarte: false,
          kidsMenu: false,
          roomService: false,
          parking: false,
          busParking: false,
          shuttle: false,
          wellnes: false,
          anotherServices: false,
          conferenceSpace: false,
          raut: false,
          bar: false
        }
      );

      this.saveDemand(d);
      demands.push(d);
    }

    return demands;
  }  

  // Room ############################################################

  /**
   * Return Room type
   *
   * @return {float} type
   */
  getRoomTypes() {
    const types = {
      standard: 0.7,
      premium: 1,
    };

    return types;
  }

  // Hotel ############################################################

  /**
   * Find Hotel configuration by hotel id in date
   *
   * @param {number} id       id of user
   * @param {object} date     {month, year}
   *
   * @return {Hotel}          return player's Hotel.
   */
  getHotel(hotelId, date) {
    return this.dbHotel.get(hotelId, date);
  }

  /**
   * Save or update Hotel
   *
   * @param {Hotel} hotel     Hotel
   *
   * @return {Hotel}         return Hotel.
   */
  saveHotel(hotel) {
    return this.dbHotel.save(hotel);
  }

  //Game config #######################################################

  saveConfig(config) {
    this.dbConfig.save(config);
  }

  getConfig() {
    return this.dbConfig.get();
  }

  // Guests #######################################################

  /**
   * Calculate average number of guests in month
   *
   * @param {number} userId     user ID
   * @param {number} month      month
   * @param {number} year       year
   *
   * @return {number}           average number of guests in month
   */
  getAverageNumberOfGuestsInMonth(userId, month, year) {
    const demands = this.getAcceptedDemands(userId, month, year);
    const days = this.getDaysInMonth(month); //todo days in selected month
    let guests = 0;

    demands.forEach(demand => {
      guests += demand.numberOfGuests;
    });

    return guests / days;
  }

  /**
   * Calculate number of guests in month without dinner
   *
   * @param {number} userId     user ID
   * @param {number} month      month
   * @param {number} year       year
   *
   * @return {number}           average number of guests in month
   */
  getNumberOfGuestsWithNoDinnerInMonth(userId, month, year) {
    const demands = this.getAcceptedDemands(userId, month, year);
    let guestsWithNoDinner = 0;

    demands.forEach(demand => {
      if (demand.getWithFood()) {
        //todo no dinner == withFood ???
        guestsWithNoDinner += demand.getNumberOfGuests();
      }
    });

    return guestsWithNoDinner;
  }

  // Hotel costs model ############################################

  hotelCostsTypes() {
    return {
      hr: 1,
      material: 2,
      equip: 3,
      promotion: 4,
      other: 5,
      fix: 6,
    };
  }

  /*
   * Save/update hotel costs
   *
   * @param {HotelCosts} costs    costs
   *
   * @return {HotelCosts}         HotelCosts
   */
  saveHotelCosts(costs) {
    const cost = this.dbHotelCosts.save(costs);

    if (!cost) {
      return null;
    }

    return new HotelCosts(
      cost.id,
      cost.userId,
      cost.costs,
      cost.day,
      cost.month,
      cost.year,
      cost.type,
    );
  }

  /*
   * Find all costs for hotel in month
   *
   * @param {number} userId       user ID
   * @param {number} month        month
   * @param {number} year         year
   *
   * @return {array}              all costs
   */
  getHotelCostsInMonth(userId, month, year) {
    const costs = this.dbHotelCosts.getPlayerCostsInMonth(userId, month, year);

    const list = costs.map(cost => {
      return new HotelCosts(
        cost.id,
        cost.userId,
        cost.costs,
        cost.day,
        cost.month,
        cost.year,
        cost.type,
      );
    });

    return list;
  }

  // Hotel costs ##################################################

  /*
   * Calculate costs for hotel
   *
   * @param {number} userId           user ID
   *
   * @return {number} monthly costs   all costs
   */
  calculateMonthlyHotelCosts(userId) {
    //todo vse jednou za mesic nebo za den?
    const monthlyFixedCosts = this.calculateMonthlyFixedCosts(userId);
    const days = this.getDaysInMonth();
    let dailyDirectCosts = 0;

    for (let i = 0; i < days; i++) {
      dailyDirectCosts += this.calculateDailyDirectCosts(userId, i); //todo jednotlivy den
    }

    return monthlyFixedCosts + dailyDirectCosts; //todo mesix asi nebude podle date
  }

  calculateMonthlyFixedCosts(hotelId, date) {
    const hrCosts = this.calculateHRCosts(hotelId, date);
    const equipCosts = this.calculateEquipCosts(hotelId, date);
    const promoCosts = this.calculatePromoCosts(hotelId, date);
    const otherServicesCosts = this.calculateOtherServicesCosts(hotelId, date);
    const otherFixedCosts = this.calculateFixedCosts(hotelId, date);

    return (
      hrCosts + equipCosts + promoCosts + otherServicesCosts + otherFixedCosts
    );
  }

  calculateDailyDirectCosts(userId, day, month, year) {
    const matCosts = this.calculateMatCosts(userId, day, month, year);
    const oTaCommission = this.calculateOtaCommission(userId);

    return matCosts + oTaCommission;
  }

  calculateMatCostsAS(hotelId, date) {
    //Denní náklady na materiál ubytovacího úseku za jednu obsazenou pokojonoc
    const config = this.getConfig();
    const roomMatCostsAsStandard = config.roomQualityStandardCost;
    const roomMatCostsAsPremium = config.roomQualityPremiumCost;

    const table = this.getCycle(hotelId, date);
    const dnorStandard = table[date.day][this.INDXSOLDSTANDARTROOM]; //Daily Number of Occupied Rooms v danem dni
    const dnorPremium = table[date.day][this.INDXSOLDBETTERROOM];

    return (
      roomMatCostsAsStandard * dnorStandard +
      roomMatCostsAsPremium * dnorPremium
    );
  }

  calculateMatCostsFB(hotelId, date) {
    const hotel = this.getHotel(hotelId, date);
    const demands = this.getAcceptedDemandsInDay(hotelId, date);
    let dNAQ = 0; //Počet ubytovaných osob v daném dni
    let dNAGQD = 0; //Počet ubytovaných skupinových osob s polopenzí v daném dni with dinner
    let dNAQND = 0; //Počet ubytovaných osob bez polopenze v daném dni no dinner

    demands.forEach(demand => {
      dNAQ += demand.getNumberOfGuests();

      if (demand.withFood()) {
        if (demand.getGroup()) {
          dNAGQD += demand.getNumberOfGuests();
        }
      } else {
        dNAQND += demand.getNumberOfGuests();
      }
    });

    const materialQuality = this.getPlayerFoodMaterial(hotelId);

    let breakMatCostsFB = 0; //todo // Denní náklady na materiál snídaní za jednoho ubytovaného hosta. zadano spravcem
    let groupDinnMatCostsFB = 0; //todo
    let alCMatCostsFB = 0; //todo
    let barMatCostsFB = 0; //todo

    switch (materialQuality.getLevel()) {
      case this.getLevels().economy:
        breakMatCostsFB = 50;
        groupDinnMatCostsFB = 50;
        alCMatCostsFB = 50;
        barMatCostsFB = 50;
        break;

      case this.getLevels().standard:
        breakMatCostsFB = 50;
        groupDinnMatCostsFB = 50;
        alCMatCostsFB = 50;
        barMatCostsFB = 50;
        break;

      case this.getLevels().premium:
        breakMatCostsFB = 50;
        groupDinnMatCostsFB = 50;
        alCMatCostsFB = 50;
        barMatCostsFB = 50;
        break;

      default:
        break;
    }

    return (
      breakMatCostsFB * dNAQ +
      groupDinnMatCostsFB * dNAGQD +
      alCMatCostsFB * dNAQND * 0.3 +
      barMatCostsFB * dNAQ * 0.5
    );
  }

  calculateMatCosts(hotelId, date) {
    const matCostsAS = this.calculateMatCostsAS(hotelId, date);
    const matCostsFB = this.calculateMatCostsFB(hotelId, date);

    return matCostsAS + matCostsFB;
  }

  calculateOtaCommission(userId) {
    const otaComRate = 0; //Procento odvodů OTA v den nákupu
    //pocet hostu ubytovanych pres ota - todo demand type ota, firma, akce...

    return 0;
  }

  /*
   * Calculate daily costs
   *
   * @param {number} userId     user ID
   *
   * @return {number}           daily costs
   */
  calculateDailyCosts(userId) {
    const monthlyFixedCosts = this.calculateMonthlyFixedCosts(userId);
    const days = this.getDaysInMonth();
    const dailyDirectCosts = this.calculateDailyDirectCosts(userId);

    return monthlyFixedCosts / days + dailyDirectCosts;
  }

  /*
   * Monthly costs for all HR
   *
   * @param {number} userId     user ID
   *
   * @return {number}           costs for all employes
   */
  calculateHRCosts(hotelId, date) {
    const costEmplRec = this.calculateCostEmplRec(hotelId, date);//ok spojit
    const costEmplHouse = this.calculateCostEmplHouse(hotelId, date);//ok
    const costEmplBreak = this.calculateCostEmplBreak(hotelId, date);//ok
    const costEmplgDinn = this.calculateCostEmplGdinn(hotelId, date);//ok
    const costEmplalcDinn = this.calculateCostEmplalcDinn(hotelId, date);
    const costEmpllb = this.calculateCostEmplLb(hotelId, date);
    const costEmplOthers = this.calculateCostEmplOthers(hotelId, date);

    return (
      costEmplRec +
      costEmplHouse +
      costEmplBreak +
      costEmplgDinn +
      costEmplalcDinn +
      costEmpllb +
      costEmplOthers
    );
  }

  /*
   * Monthly costs for reception empl
   *
   * @param {number} userId     user ID
   *
   * @return {number}           costs
   */
  calculateCostEmplRec(hotelId, date) {
    const config = this.getConfig();
    const hotel = this.getHotel(hotelId, date);
    const recJunior = hotel.hr.as.reception.junior;
    const recSenior = hotel.hr.as.reception.senior;

    const costEmplRecJunior = config.juniorReceptionWage; // 13500; //const? todo
    const costEmplRecSenior = config.seniorReceptionWage; //34000; //const?

    return (recJunior * costEmplRecJunior + recSenior * costEmplRecSenior) * 4;
  }

  /*
   * Monthly costs for housekeeping empl
   *
   * @param {number} userId     user ID
   *
   * @return {number}           costs
   */
  calculateCostEmplHouse(hotelId, date) {
    const config = this.getConfig();
    const hotel = this.getHotel(hotelId, date);
    const houseJunior = hotel.hr.as.housekeeping.junior;
    const houseSenior = hotel.hr.as.housekeeping.senior;

    const costEmplHouseJunior = config.juniorRoomServiceWage; // 13500; //const? todo
    const costEmplHouseSenior = config.seniorRoomServiceWage; // 27000; //const?

    return (
      (houseJunior * costEmplHouseJunior + houseSenior * costEmplHouseSenior) *
      2
    );
  }

  /*
   * Monthly costs for F&B breakfast empl
   *
   * @param {number} userId     user ID
   *
   * @return {number}           costs
   */
  calculateCostEmplBreak(hotelId, date) {
    const config = this.getConfig();
    const hotel = this.getHotel(hotelId, date);

    const brCookJunior = hotel.hr.fb.breakfast.kitchen.junior;
    const brCookSenior = hotel.hr.fb.breakfast.kitchen.senior;
    const brAssistJunior = hotel.hr.fb.breakfast.kitchen.auxiliary;
    const brServiceJunior = hotel.hr.fb.breakfast.restaurant.junior;
    const brServiceSenior = hotel.hr.fb.breakfast.restaurant.senior;

    const costEmplbrCookJunior = config.juniorBreakfastChefWage;
    const costEmplbrCookSenior = config.seniorBreakfastChefWage;
    const costEmplbrAssistJunior = config.helpBreakfastWage;
    const costEmplbrServiceJunior = config.juniorBreakfastWaiterWage;
    const costEmplbrServiceSenior = config.seniorBreakfastWaiterWage;

    return (
      brCookJunior * costEmplbrCookJunior +
      brCookSenior * costEmplbrCookSenior +
      brAssistJunior * costEmplbrAssistJunior +
      brServiceJunior * costEmplbrServiceJunior +
      brServiceSenior * costEmplbrServiceSenior
    );
  }

  /*
   * Monthly costs for F&B group dinner empl
   *
   * @param {number} userId     user ID
   *
   * @return {number}           costs
   */
  calculateCostEmplGdinn(hotelId, date) {
    const config = this.getConfig();
    const hotel = this.getHotel(hotelId, date);

    const gDinnCookJunior = hotel.hr.fb.dinner.kitchen.junior;
    const gDinnCookSenior = hotel.hr.fb.dinner.kitchen.senior;
    const gDinnAssistJunior = hotel.hr.fb.dinner.kitchen.auxiliary;
    const gDinnServiceJunior = hotel.hr.fb.dinner.restaurant.junior;
    const gDinnServiceSenior = hotel.hr.fb.dinner.restaurant.senior;

    const costEmplgdinnCookJunior = config.juniorDinnerChefWage;
    const costEmplgdinnCookSenior = config.seniorDinnerChefWage;
    const costEmplgdinnAssistJunior = config.helpDinnerWage;
    const costEmplgdinnServiceJunior = config.juniorDinnerWaiterWage;
    const costEmplgdinnServiceSenior = config.seniorDinnerWaiterWage;

    return (
      gDinnCookJunior * costEmplgdinnCookJunior +
      gDinnCookSenior * costEmplgdinnCookSenior +
      gDinnAssistJunior * costEmplgdinnAssistJunior +
      gDinnServiceJunior * costEmplgdinnServiceJunior +
      gDinnServiceSenior * costEmplgdinnServiceSenior
    );
  }

  /*
   * Monthly costs for ala carte empl
   *
   * @param {number} userId     user ID
   *
   * @return {number}           costs
   */
  calculateCostEmplalcDinn(hotelId, date) {
    const guests = this.getNumberOfGuestsWithNoDinnerInMonth(
      hotelId,
      date.month,
      date.year - 1,
    ); //průměrný denní počet ubytovaných osob bez polopenze ve stejném měsíci předchozího roku
    const days = this.getDaysInMonth();
    const avgDNAQND = guests / days;

    const costEmplGdinnService = 100; //todo 𝐶𝑜𝑠𝑡𝐸𝑚𝑝𝑙𝑔𝑑𝑖𝑛𝑛𝑠𝑒𝑟𝑣𝑖𝑐𝑒 neni v docs

    return avgDNAQND * 0.3 * 0.05 * costEmplGdinnService;
  }

  /*
   * Monthly costs for loby bar empl
   *
   * @param {number} userId     user ID
   *
   * @return {number}           costs
   */
  calculateCostEmplLb(hotelId, date) {
    const costEmplLbJunior = 50; //todo neni v docs
    const costEmplLbSenior = 100; //todo neni v docs
    const avgDNAQ = this.getAverageNumberOfGuestsInMonth(
      hotelId,
      date.month,
      date.year - 1,
    ); // todo pokud neni tak 0? //average Daily Number of Accommodated Quests in the same month of the previous year

    if (avgDNAQ * 0.5 > 30) {
      return costEmplLbSenior + costEmplLbJunior; //todo jen senior asi blbe vzorec
    } else {
      return costEmplLbSenior;
    }
  }

  /*
   * Calculate costs for other employes
   *
   * @param {number} userId     user ID
   *
   * @return {number}           costs
   */
  calculateCostEmplOthers(userId) {
    //todo kolik a kolik budou stat
    const hotCap = this.getNumberOfRooms(userId);
    const emplCost = 100; //todo neni v docs

    if (hotCap > 50) {
      return emplCost * 10;
    } else if (hotCap > 100) {
      return emplCost * 13;
    } else if (hotCap > 200) {
      return emplCost * 20;
    } else {
      return emplCost * 7;
    }
  }

  /*
   * Calculate costs for hotel inovation
   *
   * @param {number} userId     user ID
   *
   * @return {number}           costs
   */
  calculateEquipCosts(hotelId, date) {
    const hotel = this.getHotel(hotelId, date);
    const revenues = this.getPlayerRevenuesInMonth(
      hotelId,
      this.getLastMonth(date).month,
      this.getLastMonth(date).year,
    );
    let tRev = 0; //Total Revenue in a previous month

    revenues.forEach(revenue => {
      tRev += revenue.getRevenue();
    });

    return hotel.facilityInvestment * tRev;
  }

  /*
   * Calculate promotion costs for hotel
   *
   * @param {number} userId     user ID
   *
   * @return {number}           promo costs
   */
  calculatePromoCosts(hotelId, date) {
    const hotel = this.getHotel(hotelId, date);
    const arPromoCosts = hotel.promotionInvestment;
    const daysInMonth = this.getDaysInMonth(date);
    const hotCap = hotel.rooms.standard + hotel.rooms.premium;

    return arPromoCosts * hotCap * daysInMonth;
  }

  /*
   * Calculate costs for services
   *
   * @param {number} userId     user ID
   *
   * @return {number}           promo costs
   */
  calculateOtherServicesCosts(userId) {
    // todo other services
    return 0;
  }

  /*
   * Calculate fixed costs
   *
   * @param {number} userId     user ID
   *
   * @return {number}           fixed costs
   */
  calculateFixedCosts(userId) {
    // todo fixed costs
    return 0; // nastavuje spravce
  }

  // Revenue model ######################################################

  getRevenueTypes() {
    return {
      acc: 1,
      oth: 2,
    };
  }

  /*
   * Find players revenues
   *
   * @param {number} userId   id of player
   *
   * @return {array}          hotel revenues
   */
  getPlayerRevenues(userId) {
    const revenues = this.dbRevenue.getPlayerHotelRevenues(userId);

    const list = revenues.map(revenue => {
      return new Revenue(
        revenue.id,
        revenue.userId,
        revenue.revenue,
        revenue.day,
        revenue.month,
        revenue.year,
        revenue.type,
      );
    });

    return list;
  }

  /*
   * Find player revenues for month
   *
   * @param {number} userId   id of player
   * @param {number} month
   * @param {number} year
   *
   * @return {array}          revenue
   */
  getPlayerRevenuesInMonth(userId, month, year) {
    const revenues = this.dbRevenue.getPlayerRevenuesInMonth(
      userId,
      month,
      year,
    );

    const list = revenues.map(revenue => {
      return new Revenue(
        revenue.id,
        revenue.userId,
        revenue.revenue,
        revenue.day,
        revenue.month,
        revenue.year,
        revenue.type,
      );
    });

    return list;
  }

  /*
   * Save/update revenue
   *
   * @param {Revenue} revenue   revenue
   *
   * @return {Revenue}          revenue
   */
  saveRevenue(rev) {
    const revenue = this.dbRevenue.save(rev);

    return new Revenue(
      revenue.id,
      revenue.userId,
      revenue.revenue,
      revenue.day,
      revenue.month,
      revenue.year,
      revenue.type,
    );
  }

  // Revenue ######################################################

  /*
   * Calculate monthly revenue done
   *
   * @param {number} userId     user ID
   * @param {number} month      month
   * @param {number} year       year
   *
   * @return {number}           revenue done
   */
  calculateMonthlyRevDone(userId, month, year) {
    // dokazu spocitat pocet dni?
    //?month
    return (
      this.calculateMonthlyAccRevDone(userId, month, year) +
      this.calculateMonthlyOthRevDone(userId, month, year)
    );
  }

  /*
   * Calculate monthly acc revenue done
   *
   * @param {number} userId     user ID
   * @param {number} month      month
   * @param {number} year       year
   *
   * @return {number}           acc revenue done
   */
  calculateMonthlyAccRevDone(userId, month, year) {
    const days = this.daysInMonth();
    let monthlyAccRevDone = 0;

    for (let i = 0; i < days; i++) {
      const dailyAccRevDone = 1; //todo neni v docs
      monthlyAccRevDone += dailyAccRevDone;
    }

    return monthlyAccRevDone;
  }

  /*
   * Calculate oth revenue done
   *
   * @param {number} userId     user ID
   * @param {number} month      month
   * @param {number} year       year
   *
   * @return {number}           oth revenue
   */
  calculateMonthlyOthRevDone(userId, month, year) {
    const days = this.daysInMonth();
    let monthlyOthRevDone = 0;

    for (let i = 0; i < days; i++) {
      const dailyOthRevDone = 1; //todo neni v docs
      monthlyOthRevDone += dailyAccRevenue;
    }

    return monthlyOthRevDone;
  }

  /*
   * Calculate monthly revenue sold
   *
   * @param {number} userId     user ID
   * @param {number} month      month
   * @param {number} year       year
   *
   * @return {number}           monthly revenue
   */
  calculateMonthlyRevSold(userId, month, year) {
    //?month
    return (
      this.calculateMonthlyAccRevSold(userId, month, year) +
      this.calculateMonthlyOthRevSold(userId, month, year)
    );
  }

  /*
   * Calculate monthly acc revenue
   *
   * @param {number} userId     user ID
   * @param {number} month      month
   * @param {number} year       year
   *
   * @return {number}           revenue
   */
  calculateMonthlyAccRevSold(userId, month, year) {
    const days = this.daysInMonth();
    let monthlyAccRevSold = 0;

    for (let i = 0; i < days; i++) {
      const dailyAccRevenue = 1; //todo neni v docs
      monthlyAccRevSold += dailyAccRevenue;
    }

    return monthlyAccRevSold;
  }

  /*
   * Calculate monthly other revenue
   *
   * @param {number} userId     user ID
   * @param {number} month      month
   * @param {number} year       year
   *
   * @return {number}           revenue
   */
  calculateMonthlyOthRevSold(userId, month, year) {
    const days = this.daysInMonth();
    let monthlyOthRevSold = 0;

    for (let i = 0; i < days; i++) {
      const dailyOthRevSold = 1; //todo neni v docs
      monthlyOthRevSold += dailyOthRevSold;
    }

    return monthlyOthRevSold;
  }
  // Calculating ##############################################################

  /**
   * 
   * @param {number} hotelId 
   * @param {object} date { month, year }
   * 
   * @returns {object}
   */
  getResultsFromDb(hotelId, date) {
    const r = this.dbResult.getPlayersResults(hotelId, date); 

    if (r) {
      return r.value;
    } else {
      return null;
    }
  }

  /**
   * 
   * @param {number} hotelId 
   * 
   * @returns {array}
   */
  getAllPlayerResults(hotelId) {
    return this.dbResult.getAllPlayersResults(hotelId);
  }

  /**
   * 
   * @param {number} hotelId 
   * @param {object} date { month, year }
   */
  getResults(hotelId, date) {
    const eq = this.calculateEquipQuality(hotelId, date);
    const mq = this.calculateMaterialQuality(hotelId, date);
    const hr = this.calculateHRPotential(hotelId, date);
    const oa = this.calculateOfferAtract(hotelId, date); // todo
    const pp = this.calculateProductsPortfolio(hotelId, date);
    const oac = this.calculateOfferAccesibility(hotelId, date);
    const gs = this.calculateQuestSatisfaction(hotelId, date); // todo
    const pq = this.calculateProductsQuality(hotelId, date);

    return {
      equipQuality: eq,
      materialQuality: mq,
      hrPotential: hr,
      offerAtract: oa,
      productsPortfolio: pp,
      offerAccesibility: oac,
      guestSatisfaction: gs,
      productsQuality: pq,
    };
  }

  saveResults(hotelId, date) {// todo ukladat?
    const results = this.getResults(userId);
    const lastMonth = this.getLastMonth(date);

    this.dbResult.save(hotelId, lastMonth, results);
  }

  generateTableForMonth(hotelId, date) {
    const hotel = this.getHotel(hotelId, date);
    const days = this.getDaysInMonth(date);
    const day = [
      99,
      119,
      15,
      10,
      15,
      25,
      30,
      21,
      -20,
      21,
      80,//hotel.rooms.standard + hotel.rooms.premium,
      0,
      80,
      0,
      60,//hotel.rooms.standard,
      0,
      20,//hotel.rooms.premium,
      20,
      10,
      0,
      0,
      0,
    ];
    let month = [];

    for (let i = 0; i < days; i++) {
      month.push(day);
    }

    return month;
  }

  getGreenTableRooms(hotelId, date) {
    const cycle = this.getCycle(hotelId, date);

    let list1 = [];
    let list2 = [];

    cycle.forEach(day => {
      list1.push([
        day[this.INDXTOTALROOMSCOUNT],
        day[this.INDXROOMSSOLD],
        day[this.INDXROOMSREMAIN],
      ]);

      list2.push([
        day[this.INDXSOLDSTANDARTROOM],
        day[this.INDXSOLDSTANDARTROOMREMAIN],
        day[this.INDXSOLDBETTERROOM],
        day[this.INDXSOLDBETTERROOMREMAIN],
      ]);
    });

    return {
      roomStatus: list1,
      detailRoomStatus: list2,
    };
  }

  calculateEquipQuality(hotelId, date) {
    const hotel = this.getHotel(hotelId, date);

    const allResults = this.getResultsFromDb(hotelId, date);
    let equipQualityLastMonth = allResults.equipQuality;

    if (!equipQualityLastMonth) {
      equipQualityLastMonth = 0.8; //todo constant pokud neni hodnota
    }
    let ocrLastMonth = this.occupancyRate(hotelId, date); // Průměrná měsíční Obsazenost hotelu (OCcupency Rate);

    if (!ocrLastMonth) {
      ocrLastMonth = 0.5; //todo
    }

    let equipQuality =
      equipQualityLastMonth - 0.03 * ocrLastMonth + hotel.facilityInvestment;

    if (equipQuality > 1) {
      equipQuality = 1;
    }

    return equipQuality;
  }

  calculateMaterialQuality(hotelId, date) {
    const config = this.getConfig();
    const hotel = this.getHotel(hotelId, date);

    return (
      config.cMQAS * hotel.roomMaterialQuality +
      config.cMQFB * hotel.foodMaterialQuality
    );
  }

  calculateHRPotential(hotelId, date) {
    const config = this.getConfig();
    const hrFb = this.getPotentialHrFb(hotelId, date);//todo infinity
    const hrAs = this.getPotentialHrAs(hotelId, date);//todo infinity
    
    return config.cHRAS * hrAs + config.cHRFB * hrFb;
  }

  getPotentialHrAs(hotelId, date) {
    const config = this.getConfig();
    const hotel = this.getHotel(hotelId, date);
    const cycle = this.getCycle(hotelId, date);
    const daysInMonth = this.getDaysInMonth(date);

    let occupiedDays = 0;
    for (let i = 0; i < cycle.length; i++) {
      occupiedDays += cycle[i][this.INDXROOMSSOLD];
    }

    const emplRecJun = hotel.hr.as.reception.junior * this.getHRLevels().junior;
    const emplRecSen = hotel.hr.as.reception.senior * this.getHRLevels().senior;
    const emplRec = emplRecJun + emplRecSen;

    const anorpd = occupiedDays / daysInMonth; // Average Number of Occupied Rooms Per Day
    
    const emplRecOpt = anorpd * 0.025; // optimální míra výkonnosti pracovníků recepce potřebná pro obsloužení hostů na obsazených pokojích
    const potentialHrAsRec = emplRec / emplRecOpt;

    const emplHousJun = hotel.hr.as.housekeeping.junior * this.getHRLevels().junior;
    const emplHousSen = hotel.hr.as.housekeeping.senior * this.getHRLevels().senior;
    const emplHous = emplHousJun + emplHousSen;

    const emplHousOpt = anorpd * 0.071;
    const potentialHrAsHous = emplHous / emplHousOpt;
    
    return config.cHRASRec * potentialHrAsRec + config.cHRASHous * potentialHrAsHous;
  }

  // str 9, 10
  getPotentialHrFb(hotelId, date) {
    //todo demandy
    const config = this.getConfig();
    const hotel = this.getHotel(hotelId, date);
    const daysInMonth = this.getDaysInMonth(date);
    const demandsLastMonth = this.getAcceptedDemands(
      hotelId,
      date
    );

    let accommodatedQuests = 0;
    for (let i = 0; i < demandsLastMonth.length; i++) {
      let period = demandsLastMonth[i].period;
      let firstDay = demandsLastMonth[i].startDate.day;

      if (period + firstDay < daysInMonth) {
        accommodatedQuests += demandsLastMonth[i].numberOfGuests * period;
      } else {
        accommodatedQuests +=
          demandsLastMonth[i].numberOfGuests * (daysInMonth - firstDay);
      }
    }

    const anaqpd = accommodatedQuests / daysInMonth; //Average Number of Accommodated Group Quests Per Day

    const emplBreakKitchenOpt = anaqpd * 0.0425;
    const potentialHRBreakKitchen =
      (hotel.hr.fb.breakfast.kitchen.junior * this.getHRLevels().junior +
        hotel.hr.fb.breakfast.kitchen.senior * this.getHRLevels().senior) /
      emplBreakKitchenOpt;

    const emplBreakRestOpt = anaqpd * 0.025;
    const potentialHRBreakRest =
      (hotel.hr.fb.breakfast.restaurant.junior * this.getHRLevels().junior +
        hotel.hr.fb.breakfast.restaurant.senior * this.getHRLevels().senior) /
      emplBreakRestOpt;

    const potentialHRFBBreak =
      config.chrfbBreakKitchen * potentialHRBreakKitchen +
      config.chrfbBreakRest * potentialHRBreakRest;

    let groupDemandsLastMonth = demandsLastMonth;//todo poptavky s veceri
    
    let accommodatedGroupQuestsWithFood = 0;
    for (let i = 0; i < groupDemandsLastMonth.length; i++) {

      if (groupDemandsLastMonth[i].requirements.breakfast) {//todo
        let period = groupDemandsLastMonth[i].period;
        let firstDay = groupDemandsLastMonth[i].startDate.day;
        if (period + firstDay < daysInMonth) {
          accommodatedGroupQuestsWithFood +=
            groupDemandsLastMonth[i].numberOfGuests * period;
        } else {
          accommodatedGroupQuestsWithFood +=
            groupDemandsLastMonth[i].numberOfGuests *
            (daysInMonth - firstDay);
        }
      }
    }

    const anagqfpd = accommodatedGroupQuestsWithFood / daysInMonth; //Average Number of Accommodated Group Quests with Food Per Day

    const emplGroupKitchenOpt = anagqfpd * 0.0675;
    const potentialHRFBGroupKitchen =
      (hotel.hr.fb.dinner.kitchen.junior * this.getHRLevels().junior +
        hotel.hr.fb.dinner.kitchen.senior * this.getHRLevels().senior) /
      emplGroupKitchenOpt;

    const emplGroupRestOpt = anagqfpd * 0.06;
    const potentialHRFBGroupRest =
      (hotel.hr.fb.dinner.restaurant.junior * this.getHRLevels().junior +
        hotel.hr.fb.dinner.restaurant.senior +
        this.getHRLevels().senior) /
      emplGroupRestOpt;

    const potentialHRFBGroup =
      potentialHRFBGroupKitchen * config.chrfbGroupKitchen +
      potentialHRFBGroupRest * config.chrfbGroupRest;
    
    return (
      config.chrfbBreak * potentialHRFBBreak +
      config.chrfbGroup * potentialHRFBGroup
    );
  }

  calculateProductsQuality(hotelId, date) {
    const config = this.getConfig();
    const hrPotential = this.calculateHRPotential(hotelId, date);
    const matQuality = this.calculateMaterialQuality(hotelId, date);
    const equipQuality = this.calculateEquipQuality(hotelId, date);

    return (
      config.cpqhr * hrPotential +
      config.cpqmq * matQuality +
      config.cpqeq * equipQuality
    );
  }

  calculateProductsPortfolio(hotelId, date) {//todo
    const allServices = 16;
    const hotelEquipt = this.getHotel(hotelId, date).equipment;
    let hotelServices = 0;

    Object.keys(hotelEquipt).forEach(function(value) {
      if (hotelEquipt[value] == true) {
        hotelServices += 1;
      }
    });

    return hotelServices / allServices;
  }

  calculateOfferAtract(hotelId, date) {
    const config = this.getConfig();
    const equipQuality = this.calculateEquipQuality(hotelId, date);
    const productsPortfolio = this.calculateProductsPortfolio(hotelId, date);

    return config.coaeq * equipQuality + config.coapp * productsPortfolio + config.coabl * config.brandLevel;
  }

  calculateQuestSatisfaction(hotelId, date) {
    //todo
    const productsQuality = this.calculateProductsQuality(hotelId, date);
    let overbookTolerance = 0;
    //hoste kteří nebyly v předchozím měsíci ubytováni z důvodů plné kapacitu hotelu
    const lastMonth = this.getLastMonth(date);//todo reservation...
    const allDemandsLastMonth = this.getDemandsInMonth(
      hotelId,
      lastMonth
    );
    let denyReservations = 0;

    for (let i = 0; i < allDemandsLastMonth.length; i++) {
      if (allDemandsLastMonth[i].accepted == false) {
        denyReservations += 1;
      }
    }
    let wqr = denyReservations / allDemandsLastMonth.length;

    if (wqr <= 0.02) {
      overbookTolerance = 1;
    } else {
      overbookTolerance = 1 - (wqr - 0.02) * 5;
    }

    if (overbookTolerance < 0) {
      overbookTolerance = 0;
    }

    return 5 * ((productsQuality + overbookTolerance) / 2);
  }

  // todo - neni vzorec
  calculateOfferAccesibility(hotelId, date) {
    const hotel = this.getHotel(hotelId, date)
    let offerAccesibility = 1;

    const promotion = hotel.promotionInvestment;

    if (offerAccesibility > 1) {
      offerAccesibility = 1;
    }
    return offerAccesibility;
  }

  getLastMonth(date) {
    let lastMonth = date.month - 1; //todo fix
    let year = date.year;

    if (lastMonth <= -1) {
      lastMonth = 11;
      year -= 1;
    }

    return {
      month: lastMonth,
      year: year,
    };
  }

  getNextMonth(date) {
    let nextMonth = date.month + 1;
    let year = date.year;

    if (nextMonth >= 12) {
      nextMonth = 0;
      year += 1;
    }

    return {
      month: nextMonth,
      year: year,
    };
  }

  getHRPositions() {
    const positions = {
      restaurant: 1,
      kitchen: 2,
    };

    return positions;
  }

  getHRPlaces() {
    const places = {
      reception: 1,
      housekeeping: 2,
      breakfast: 3,
      dinner: 4,
    };

    return places;
  }

  getHRTypes() {
    const types = {
      auxiliary: 1,
      junior: 2,
      senior: 3,
    };

    return types;
  }

  getHRLevels() {
    return {
      junior: 0.7,
      senior: 1,
    };
  }

  getLevels() {
    const levels = {
      economy: 0.5,
      standard: 0.7,
      premium: 1,
    };

    return levels;
  }

  getDaysInMonth(date) {
    return new Date(date.year, date.month, 0).getDate();
  } //todo muze nastavit spravce

  // Measure functions #######################################################

  occupancyRate(hotelId, date) {
    const hotel = this.getHotel(hotelId, date);
    const rooms = hotel.rooms.standard + hotel.rooms.premium;
    const cycle = this.getCycle(hotelId, date);
    let soldRooms = 0;

    for (let i = 0; i < cycle.length; i++) {
      soldRooms += cycle[i][this.INDXROOMSSOLD];
    }

    return ((rooms - soldRooms) / rooms) * 100;
  }

  averageDailyRate(hotelId, date) {
    const cycle = this.getPlayerList(
      userId,
      date.month,
      date.year,
    );
    const today = 1;

    let totalSum = 0;
    let roomsCount = 0;

    for (let i = 0; i < today; i++) {
      totalSum +=
        cycle[i][this.INDXSOLDSTANDARTROOM] *
        cycle[i][this.INDXSTANDARTROOMPRICE];
      totalSum +=
        cycle[i][this.INDXSOLDBETTERROOM] * cycle[i][this.INDXBETTERROOMPRICE];
      roomsCount += cycle[i][this.INDXROOMSSOLD];
    }

    return totalSum / roomsCount;
  }

  calculateRevenue(hotelId, date) {
    const config = this.getConfig();
    const hotel = this.getHotel(hotelId, date);
    const rooms = this.getNumberOfRooms(hotelId);
    const services = this.getPlayerRoomMaterial(hotelId);
    const foodServices = this.getPlayerFoodMaterial(hotelId);
    const days = this.getDaysInMonth(date);

    let sumRoom = 0;
    if (services.getLevel() == this.getLevels().standard) {
      sumRoom = config.getRoomQualityStandardCost() * rooms * days;
    } else {
      sumRoom = config.getRoomQualityPremiumCost() * rooms * days;
    }

    let sumFood = 0;
    if (foodServices.getLevel() == this.getLevels().economy) {
      sumFood = config.getFoodQualityEconomyAlaCarteCost();
      sumFood += config.getFoodQualityEconomyBarCost();
      sumFood += config.getFoodQualityEconomyBreakfastCost();
      sumFood += config.getFoodQualityEconomyDinnerCost();
    } else if (foodServices.getLevel() == this.getLevels().standard) {
      sumFood = config.getFoodQualityStandardAlaCarteCost();
      sumFood += config.getFoodQualityStandardBarCost();
      sumFood += config.getFoodQualityStandardBreakfastCost();
      sumFood += config.getFoodQualityStandardDinnerCost();
    } else {
      sumFood = config.getFoodQualityPremiumAlaCarteCost();
      sumFood += config.getFoodQualityPremiumBarCost();
      sumFood += config.getFoodQualityPremiumBreakfastCost();
      sumFood += config.getFoodQualityPremiumDinnerCost();
    }
    sumFood = sumFood * rooms * days;

    return {
      revenuePerAvalilableRoom: sumRoom / rooms,
      totalRevenuePerAvalilableRoom: (sumRoom + sumFood) / rooms,
    };
  }
  //todo asi neni potreba
  countPriceForDemand(userId, demand) {
    let day = demand.getStartDay();
    let year = this.getActualYear();
    const period = demand.getPeriod();
    const month = demand.getStartMonth();
    let list = this.getPlayerList(userId, month, year);
    const rooms = this.getRoomsByDemand(demand);
    const roomsCount = rooms.length;
    let price = 0;

    let nextMonth = this.getNextMonth(date);

    for (let i = 0; i < period; i++) {
      if (day > 29) {
        day = 0;
        list = this.getPlayerList(userId, nextMonth.month, nextMonth.year);
      }
      if (demand.getRoomType() == this.getRoomTypes().standard) {
        price += list[day][this.INDXSTANDARTROOMPRICE] * roomsCount;
      } else {
        //premium
        price += list[day][this.INDXBETTERROOMPRICE] * roomsCount;
      }
      day += 1;
    }

    return price;
  }
}

module.exports = Kalkulin;

// generování demnadu pro všechny + pak přiřadit tomu kdo vyhral
// monthly rev sold/done - co to je?, budu schopen zjistit pocet dni podle mesice?
// calculateCostEmplLb - podle hodnoty v minulem roce - co když žádná není? 0? - víc takových vzorců, není špatně vzorec?

//todo co kdyt nebude zaznam hotelu v budoucnu... datum atd z konfigurace
console.log(new Kalkulin().getResults(0, {month: 4, year: 2020}));