const ModelDate = require('./ModelDate');

class HotelCosts extends ModelDate {
  constructor(id = null, userId, costs, day, month, year, type) {
    super(id, userId, day, month, year);
    this.costs = costs;
    this.type = type; // hr/eq/mat/prop/oth/fix
  }

  getCosts() {
    return this.costs;
  }

  setCosts(costs) {
    this.costs = costs;
  }

  getType() {
    return this.type;
  }

  setType(type) {
    this.type = type;
  }
}
module.exports = HotelCosts;
