const Model = require('./Model');

class Room extends Model {
  constructor(id, userId, demandId, roomNumber, numberOfBeds, type) {
    super(id, userId);
    this.demandId = demandId;           // ID demandu, který má tento pokoj zarezervovaný
    this.roomNumber = roomNumber;       // číslo pokoje
    this.numberOfBeds = numberOfBeds;   // počet postelí v pokoji
    this.type = type;                   // typ pokoje (standard: 0.7, premium: 1)
  }
}
module.exports = Room;
