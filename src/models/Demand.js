class Demand {
  constructor(
    id = null,
    hotelId = null,
    numberOfGuests,
    numberOfRooms = 1,
    reservationDate = {
      day: 0,
      month: 0,
      year: 0
    },
    startDate = {
      day: 0,
      month: 0,
      year: 0
    },
    period,
    accepted = false,
    stornoDate = null,
    price = 0,
    type,
    requirements = {
      maxPrice: 0,                // maximalni cena
      standardRoom: false,        // standartní pokoj (ano/ne)
      premiumRoom: false,         // prémium pokoj (ano/ne)
      connectedRoom: false,       // spojené pokoje (ano/ne)
      breakfast: false,           // snídaně (ano/ne)
      fullBoard: false,           // polopenze/plná penze (ano/ne)
      alaCarte: false,            // a la carte restaurace (ano/ne)
      kidsMenu: false,            // dětské menu/vybavení pro děti (ano/ne)
      roomService: false,         // room service (ano/ne)
      parking: false,             // parkování (ano/ne)
      busParking: false,          // parkování pro autobus (ano/ne)
      shuttle: false,             // taxi/shuttle (ano/ne)
      wellnes: false,             // wellnes a masáže (ano/ne)
      anotherServices: false,     // další služby (ano/ne)
      conferenceSpace: false,     // konferenční prostory (ano/ne)
      raut: false,                // raut/slavnostní oběd (ano/ne)
      bar: false,                 // bar (ano/ne)
    }
  ) {
    this.id = id;
    this.hotelId = hotelId;
    this.numberOfGuests = numberOfGuests;   // počet hostů
    this.numberOfRooms = numberOfRooms;     // pocet pokoju
    this.reservationDate = reservationDate; // datum rezervace
    this.startDate = startDate;             // od kdy
    this.period = period;                   // délka ubytování
    this.accepted = accepted;               // příjmuta?
    this.stornoDate = stornoDate;           // datum storna
    this.price = price;                     // cena poptavky
    this.type = type;                       // typ
    this.requirements = requirements;       // pozadavky
  }
}

module.exports = Demand;
