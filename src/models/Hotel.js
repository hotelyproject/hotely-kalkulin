const ModelDate = require('./ModelDate');

class Hotel extends ModelDate {
  constructor(
    hotelId = null,
    month = null,
    year = null,
    facilityInvestment = 0,       // investive do vybaveni hotelu 0.01/0.02/0.03
    promotionInvestment = 0,      // investice do propagace
    foodMaterialQuality = 0.5,    // accomodation - economy: 0.5, standard: 0.7, premium: 1
    roomMaterialQuality = 0.7,    // kvalita ubytovani - standard: 0.7, premium: 1
    hr = {
      as: {              // accomodation service
        reception: {     // pracovnici recepce
          junior: 0,
          senior: 0,
        },
        housekeeping: {  // pracovnici uklidu
          junior: 0,
          senior: 0,
        }
      },
      fb: {              // pracovnici restaurace
        breakfast: {     // snidane
          kitchen: {     // kuhari
            junior: 0,
            senior: 0,
            auxiliary: 0
          },              // cisnici
          restaurant: {
            junior: 0,
            senior: 0
          }
        },
        dinner: {        // vecere
          kitchen: {     // kuchari
            junior: 0,
            senior: 0,
            auxiliary: 0
          },              // cisnici
          restaurant: {
            junior: 0,
            senior: 0
          }
        }
      }
    },
    rooms = {                     // pocet jednotlivych pokoju
      standard: 60,
      premium: 20,
    },
    equipment = {
      standardRoom: false,        // standartní pokoj (ano/ne)
      premiumRoom: false,         // prémium pokoj (ano/ne)
      connectedRoom: false,       // spojené pokoje (ano/ne)
      breakfast: false,           // snídaně (ano/ne)
      fullBoard: false,           // polopenze/plná penze (ano/ne)
      alaCarte: false,            // a la carte restaurace (ano/ne)
      kidsMenu: false,            // dětské menu/vybavení pro děti (ano/ne)
      roomService: false,         // room service (ano/ne)
      parking: false,             // parkování (ano/ne)
      busParking: false,          // parkování pro autobus (ano/ne)
      shuttle: false,             // taxi/shuttle (ano/ne)
      wellnes: false,             // wellnes a masáže (ano/ne)
      anotherServices: false,     // další služby (ano/ne)
      conferenceSpace: false,     // konferenční prostory (ano/ne)
      raut: false,                // raut/slavnostní oběd (ano/ne)
      bar: false,                 // bar (ano/ne)
    },
  ) {
    super(month, year);
    this.hotelId = hotelId;
    this.facilityInvestment = facilityInvestment;   // investice do vybaveni
    this.promotionInvestment = promotionInvestment; // investice do prezentace
    this.foodMaterialQuality = foodMaterialQuality; // kvalita jidla
    this.roomMaterialQuality = roomMaterialQuality; // kvalita vybaveni
    this.hr = hr;                           // zamestnanci hotelu
    this.rooms = rooms;                     // počet pokojů
    this.equipment = equipment;             // vybavení hotelu (object)
  }
}

module.exports = Hotel;
