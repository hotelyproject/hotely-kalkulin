const ModelDate = require('./ModelDate');

class Revenue extends ModelDate {
  constructor(id = null, userId, revenue, day, month, year, type) {
    super(id, userId, day, month, year);
    this.revenue = revenue;
    this.type = type; // acc/oth
  }

  getRevenue() {
    return this.revenue;
  }

  setRevenue(revenue) {
    this.revenue = revenue;
  }

  getType() {
    return this.type;
  }

  setType(type) {
    this.type = type;
  }
}

module.exports = Revenue;
