//abstract model
module.exports = class Model {
  constructor(id = null, hotelId, month, year) {
    this.id = id; 
    this.hotelId = hotelId; 
    this.month = month;
    this.year = year;
  }
}
