exports.Kalkulin = "./Kalkulin";
exports.GameConfiguration = "./GameConfiguration.js";

exports.FoodMaterial = "./models/FoodMaterialQuality";
exports.RoomMaterial = "./models/RoomMaterialQuality";
exports.Demand = "./models/Demand";
exports.Facility = "./models/Facility";
exports.PotentialHR = "./models/PotentialHR";
exports.Promotion = "./models/Promotion";
exports.Hotel = "./models/Hotel";
exports.Room = "./models/Room"