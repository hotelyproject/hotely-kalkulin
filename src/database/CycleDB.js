const Database = require('./Database.js');

class CycleDB extends Database {
  constructor(file) {
    super(file);

    this.db
      .defaults({
        cycle: [],
        cycleCount: 0,
        cycleLastId: 0,
      })
      .write();
  }

  save(hotelId, date, cycle) {
    if (this.get(hotelId, date)) {
      this._update(hotelId, date, cycle);
      
    } else {
      //const id = this.db.get('cycleLastId').value()
      //this.db.update('cycleLastId', n => n + 1).write();

      this.db
        .get('cycle')
        .push({
          hotelId: hotelId,
          month: date.month,
          year: date.year,
          cycle: cycle,
        })
        .write();

      this.db.update('cycleCount', n => n + 1).write();

      console.log('Cycle inserted into db');
    }
  }

  get(hotelId, date) {
    return this.db
      .get('cycle')
      .find({
        hotelId: hotelId,
        month: date.month,
        year: date.year,
      })
      .value();
  }

  _update(hotelId, date, cycle) {
    this.db
      .get('cycle')
      .find({
        hotelId: hotelId,
        month: date.month,
        year: date.year,
      })
      .assign({ cycle: cycle })
      .write();
    console.log('Cycle updated');
  }
}
module.exports = CycleDB;
