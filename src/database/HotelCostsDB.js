const Database = require('./Database.js');

class HotelCostsDB extends Database {
  constructor(file) {
    super(file);

    this.db
      .defaults({
        costs: [],
        costsCount: 0,
        costsLastId: 0,
      })
      .write();
  }

  save(hotelCosts) {
    //todo podle data
    if (this.get(hotelCosts.getId())) {
      this._update(hotelCosts.getId(), hotelCosts);

      return this.get(hotelCosts.getId());
    } else {
      const id = this.db.get('costsLastId').value();
      this.db.update('costsLastId', n => n + 1).write();

      this.db
        .get('costs')
        .push({
          id: id,
          userId: hotelCosts.getUserId(),
          costs: hotelCosts.getCosts(),
          day: hotelCosts.getDay(),
          month: hotelCosts.getMonth(),
          year: hotelCosts.getYear(),
          type: hotelCosts.getType(),
        })
        .write();

      this.db.update('costsCount', n => n + 1).write();
      console.log('hotelCosts inserted into db, id: ' + id);

      return {
        id: id,
        userId: hotelCosts.getUserId(),
        costs: hotelCosts.getCosts(),
        day: hotelCosts.getDay(),
        month: hotelCosts.getMonth(),
        year: hotelCosts.getYear(),
        type: hotelCosts.getType(),
      };
    }
  }

  get(id) {
    try {
      return this.db
        .get('costs')
        .find({ id: id })
        .value();
    } catch (error) {
      console.log('db/getCosts return: null', error);
      return null;
    }
  }

  getPlayerCostsInMonth(userId, month, year) {
    try {
      return this.db
        .get('costs')
        .filter({
          userId: userId,
          month: month,
          year: year,
        })
        .value();
    } catch (error) {
      console.log('db/getPlayerCosts return: null', error);
      return null;
    }
  }

  _update(id, hotelCosts) {
    console.log('update HotelCosts');
    this.db
      .get('costs')
      .find({ id: id })
      .assign({
        userId: hotelCosts.getUserId(),
        costs: hotelCosts.getCosts(),
        day: hotelCosts.getDay(),
        month: hotelCosts.getMonth(),
        year: hotelCosts.getYear(),
        type: hotelCosts.getType(),
      })
      .write();
  }
}

module.exports = HotelCostsDB;
