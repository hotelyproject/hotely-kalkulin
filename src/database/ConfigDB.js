const Database = require('./Database.js');

class ConfigDB extends Database {
  constructor(file) {
    super(file);

    this.db
      .defaults({
        config: [],
        configCount: 1,
      })
      .write();
  }

  save(config) {
    if (this.get()) {
      this._update(config);
    } else {
      this.db
        .get('config')
        .push(config)
        .write();
      console.log('Config inserted into db');
    }
  }

  get() {
    try {
      return this.db
        .get('config')
        .find()
        .value();
    } catch (error) {
      return null;
    }
  }

  _update(config) {
    this.db
      .get('config')
      .find()
      .assign(config)
      .write();

    console.log('config updated');
  }
}
module.exports = ConfigDB;