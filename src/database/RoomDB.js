const Database = require('./Database.js');

class RoomDB extends Database {
  constructor(file) {
    super(file);

    this.db
      .defaults({
        room: [],
        roomCount: 0,
        roomLastId: 0,
      })
      .write();
  }

  save(room) {
    if (this.get(room.getId())) {
      this._update(room.getId(), room);
      console.log('Room updated');

      return this.get(room.getId());
    } else {
      const id = this.db.get('roomLastId').value();
      this.db.update('roomLastId', n => n + 1).write();

      this.db
        .get('room')
        .push({
          id: id,
          userId: room.getUserId(),
          demandId: room.getDemandId(),
          roomNumber: room.getRoomNumber(),
          numberOfBeds: room.getNumberOfBeds(),
          type: room.getType(),
        })
        .write();

      this.db.update('roomCount', n => n + 1).write();

      console.log('Room inserted into db, id: ' + id);

      return {
        id: id,
        userId: room.getUserId(),
        demandId: room.getDemandId(),
        roomNumber: room.getRoomNumber(),
        numberOfBeds: room.getNumberOfBeds(),
        type: room.getType(),
      };
    }
  }

  get(id) {
    try {
      return this.db
        .get('room')
        .find({ id: id })
        .value();
    } catch (error) {
      console.log('db/getRoom return: null');
      return null;
    }
  }

  getPlayersRooms(userId) {
    return this.db
      .get('room')
      .filter({ userId: userId })
      .value();
  }

  getRoomsByDemand(userId, demandId) {
    return this.db
    .get('room')
    .filter({ 
        userId: userId,
        demandId: demandId
     })
    .value();
  }

  getAllOccupiedRooms() {
    const allRooms =  this.db
      .get('room')
      .value();
    let occupiedRooms = [];

    allRooms.forEach(room => {
      if (room.demandId != null) {
        occupiedRooms.push(room);
      }
    });
    
    return occupiedRooms;
  }

  _update(id, room) {
    console.log('update Room');
    this.db
      .get('room')
      .find({ id: id })
      .assign({
        userId: room.getUserId(),
        demandId: room.getDemandId(),
        roomNumber: room.getRoomNumber(),
        numberOfBeds: room.getNumberOfBeds(),
        type: room.getType(),
      })
      .write();
  }
}
module.exports = RoomDB;