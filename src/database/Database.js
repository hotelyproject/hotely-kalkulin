const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

class Database {
    constructor(file) {
        const adapter = new FileSync(file);
        this.db = low(adapter);
    }

    save() {

    }

    get(id) {

    }

    _update() {

    }

    remove(id) {

    }
}
module.exports = Database;