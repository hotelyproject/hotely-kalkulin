const Database = require('./Database.js');

class HotelRevenueDB extends Database {
  constructor(file) {
    super(file);
    this.db
      .defaults({
        revenue: [],
        revenueCount: 0,
        revenueLastId: 0,
      })
      .write();
  }

  save(revenue) {
    if (this.get(revenue.getId())) {
      this._update(revenue.getId(), revenue);

      return this.get(revenue.getId());
    } else {
      const id = this.db.get('revenueLastId').value();
      this.db.update('revenueLastId', n => n + 1).write();

      this.db
        .get('revenue')
        .push({
          id: id,
          userId: revenue.getUserId(),
          revenue: revenue.getRevenue(),
          day: revenue.getDay(),
          month: revenue.getMonth(),
          year: revenue.getYear(),
        })
        .write();

      this.db.update('revenueCount', n => n + 1).write();

      console.log('Revenue inserted into db, id: ' + id);

      return {
        id: id,
        userId: revenue.getUserId(),
        revenue: revenue.getRevenue(),
        day: revenue.getDay(),
        month: revenue.getMonth(),
        year: revenue.getYear(),
      };
    }
  }

  get(id) {
    try {
      return this.db
        .get('revenue')
        .find({ id: id })
        .value();
    } catch (error) {
      console.log('db/getRevenue return: null', error);
      return null;
    }
  }

  getPlayerHotelRevenues(userId) {
    try {
      return this.db
        .get('revenue')
        .filter({ userId: userId })
        .value();
    } catch (error) {
      console.log('db/getPlayerRevenue return: null', error);
      return null;
    }
  }

  getPlayerRevenuesInMonth(userId, month, year) {
    try {
      return this.db
        .get('revenue')
        .filter({
          userId: userId,
          month: month,
          year: year,
        })
        .value();
    } catch (error) {
      console.log('db/getPlayerRevenue return: null', error);
      return null;
    }
  }

  _update(id, revenue) {
    this.db
      .get('revenue')
      .find({ id: id })
      .assign({
        userId: revenue.getUserId(),
        revenue: revenue.getRevenue(),
        day: revenue.getDay(),
        month: revenue.getMonth(),
        year: revenue.getYear(),
      })
      .write();
    console.log('Revenue updated');
  }
}

module.exports = HotelRevenueDB;
