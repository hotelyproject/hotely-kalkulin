const Database = require('./Database.js');

class HotelDB extends Database {
  constructor(file) {
    super(file);

    this.db
      .defaults({
        hotel: [],
        hotelCount: 0,
        hotelLastId: 0,
      })
      .write();
  }

  save(hotel) {
    if (this.get(hotel.hotelId, {month: hotel.date.month, year: hotel.date.year})) { // if exist
        this._update(hotel);

        console.log('Hotel updated');
        return hotel;
    } else {
        //const id = this.db.get('hotelLastId').value();
        //this.db.update('hotelLastId', n => n + 1).write();
        //hotel.hotelId = id;
        
        this.db
        .get('hotel')
        .push(hotel)
        .write();

        this.db.update('hotelCount', n => n + 1).write();
        console.log('Hotel inserted into db');

        return hotel;
    }
  }

  get(hotelId, date) {
    try {
      return this.db
        .get('hotel')
        .find({ 
          hotelId: hotelId,
          date: {month: date.month, year: date.year}
         })
        .value();
    } catch (error) {
        return null;
    }
  }

  _update(hotel) {
    console.log('update Hotel');
    this.db
      .get('hotel')
      .find({ 
        hotelId: hotel.hotelId,
        date: {month: hotel.date.month, year: hotel.date.year}
       })
      .assign(hotel)
      .write();
  }
}

module.exports = HotelDB;