const Database = require('./Database.js');

class DemandDB extends Database {
  constructor(file) {
    super(file);
    this.db
      .defaults({
        demand: [],
        demandCount: 0,
        demandLastId: 0,
      })
      .write();
  }

  save(demand) {
    if (this.get(demand.id)) {
      this._update(demand);
      console.log('Demand updated');

      return demand;
    } else {
      const id = this.db.get('demandLastId').value();
      this.db.update('demandLastId', n => n + 1).write();

      demand.id = id;
      this.db
        .get('demand')
        .push(demand)
        .write();

      this.db.update('demandCount', n => n + 1).write();

      console.log('Demand inserted into db, id: ' + id);
      return demand;
    }
  }

  get(id) {
    try {
      return this.db
        .get('demand')
        .find({ id: id })
        .value();
    } catch (error) {
      console.log(error);
    }
    console.log('db/getDemand return null');
    return null;
  }

  getAllDemands(hotelId) {
    return this.db
      .get('demand')
      .filter({
        hotelId: hotelId,
      })
      .value();
  }

  getAcceptedDemands(hotelId, date) {
    return this.db
      .get('demand')
      .filter({
        accepted: true,
        hotelId: hotelId,
        startDate: {month: date.month, year: date.year}
      })
      .value();
  }

  getDemandsInMonth(hotelId, date) {
    return this.db
      .get('demand')
      .filter({
        startDate: {month: date.month, year: date.year},
        hotelId: hotelId,
      })
      .value();
  }

  _update(demand) {
    console.log('update Demand');
    this.db
      .get('demand')
      .find({ id: demand.id })
      .assign(demand)
      .write();
  }
}

module.exports = DemandDB;
