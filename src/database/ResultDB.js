const Database = require('./Database.js');

class ResultDB extends Database {
  constructor(file) {
    super(file);

    this.db
      .defaults({
        result: [],
        resultCount: 0,
        resultLastId: 0,
      })
      .write();
  }

  save(userId, date, values) {
    if (this.getPlayersResults(userId, date)) {
      const r = this.getPlayersResults(userId, date);
      this._update(r.id, values);

      console.log('Results updated');
      return {
        id: r.id,
        userId: userId,
        month: month,
        year: year,
        value: values,
    }; 
    } else {
      const id = this.db.get('resultLastId').value();
      this.db.update('resultLastId', n => n + 1).write();

      this.db
          .get('result')
          .push({
            id: id,
            userId: userId,
            month: month,
            year: year,
            value: values,
          })
          .write();

      this.db.update('resultCount', n => n + 1).write();

      console.log('Result inserted into db, id: ' + id);

      return {
          id: id,
          userId: userId,
          month: month,
          year: year,
          value: values,
      };  
    }
  }

  getPlayersResults(hotelId, date) {
    try {
      return this.db
        .get('result')
        .find({ 
          hotelId: hotelId,
          month: date.month,
          year: date.year
          })
        .value();
    } catch (error) {
      console.log('db/getResults return: null');
      return null;
    }
  }

  getAllPlayersResults(hotelId) {
    try {
      return this.db
        .get('result')
        .filter({ 
          hotelId: hotelId
          })
        .value();
    } catch (error) {
      console.log('db/getResults return: null');
      return null;
    }
  }

  _update(id, values) {
    this.db
    .get('result')
    .find({ id: id })
    .assign({
      value: values,
    })
    .write();
  }
}

module.exports = ResultDB;