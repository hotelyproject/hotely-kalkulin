const Kalkulin = require('./../Kalkulin');
const k = new Kalkulin();

test('List null', () => {
    expect(k.getList(-1)).toBeNull();
});
test('Demand null', () => {
    expect(k.getDemand(-1)).toBeNull();
});
test('Facality null', () => {
    expect(k.getFacility(-1)).toBeNull();
});
test('FoodMaterial null', () => {
    expect(k.getFoodMaterial(-1)).toBeNull();
});
test('HRAS null', () => {
    expect(k.getHRAS(-1)).toBeNull();
});
test('getHR null', () => {
    expect(k.getHR(-1)).toBeNull();
});
test('Promotion null', () => {
    expect(k.getPromotion(-1)).toBeNull();
});
test('RoomMaterial null', () => {
    expect(k.getRoomMaterial(-1)).toBeNull();
});
test('Room null', () => {
    expect(k.getRoom(-1)).toBeNull();
});