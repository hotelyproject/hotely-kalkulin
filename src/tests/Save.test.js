const Kalkulin = require('./../Kalkulin');
const Demand = require('./../models/Demand');
const Facility = require('./../models/Facility.js');
const FoodMaterial = require('./../models/FoodMaterialQuality.js');
const PotentialHR = require('./../models/PotentialHR.js');
const PotentialHRAS = require('./../models/PotentialHRAS.js');
const Promotion = require('./../models/Promotion.js');
const RoomMaterial = require('./../models/RoomMaterialQuality.js');
const Room = require('./../models/Room.js');
const Hotel = require('./../models/Hotel.js');
const GameConfiguration = require('./../GameConfiguration.js');
const k = new Kalkulin();

test('Save demand', () => {
    const d = new Demand(1,1,"User New 1",1,17,9,2019,16,false,true,0.7,false);
    const demand = k.saveDemand(d);
    expect(demand).toEqual(d);
});
test('Save facility', () => {
    const i = new Facility(1,2,1,2);
    const t = k.saveFacility(i);
    expect(t).toEqual(i);
});
test('Save FoodMaterial', () => {
    const i = new FoodMaterial(1,2,1);
    const t = k.saveFoodMaterial(i);
    expect(t).toEqual(i);
});
test('Save PotentialHR', () => {
    const i = new PotentialHR(1,1,2,2,1,2);
    const t = k.saveHR(i);
    expect(t).toEqual(i);
});
test('Save PotentialHRAS', () => {
    const i = new PotentialHRAS(1,1,2,3,2);
    const t = k.saveHRAS(i);
    expect(t).toEqual(i);
});
test('Save Promotion', () => {
    const i = new Promotion(1,2,1);
    const t = k.savePromotion(i);
    expect(t).toEqual(i);
});
test('Save Promotion Request', () => {
    const json = {
        userId : 0.522,
        investment : 1500
    }
    const t = k.savePromotionRequest(json);
    console.log("save promotion request", t)
    expect(t).toBeDefined();
});
test('Save RoomMaterial', () => {
    const i = new RoomMaterial(1,2,1);
    const t = k.saveRoomMaterial(i);
    expect(t).toEqual(i);
});
test('Save Room', () => {
    const i = new Room(1,1,null,5,3,0.5);
    const t = k.saveRoom(i);
    expect(t).toEqual(i);
});
test('Save Hotel', () => {
    const i = new Hotel(1,2);
    const t = k.saveHotel(i);
    expect(t).toEqual(i);
});
test('Save game config', () => {
    const d = new GameConfiguration(0.5,  0.5,   0.5,   0.5,   0.5,   0.5,   0.5,   0.5,   0.5,   0.5,   0.5,   0.5,   0.5,   0.5,   0.5,  0.4,  0.3,  0.3,  0.8,  13000,  24000,  111111,  11111,  2222222,  333333,  4444444,  555555,  6666666,  777777,  55555,  4444444,  33333,  2222222,  33333,  5555,  2222222,  333333,  4444444,  555555,  6666666,  777777,  55555,  4444444,  33333,  2222222,  33333,  5554444,  );
    const h = k.saveConfig(d);
    expect(h).toBeUndefined();
});