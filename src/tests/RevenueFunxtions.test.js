const Kalkulin = require('./../Kalkulin');
const k = new Kalkulin();

test('monthly revenue done should be greather than 0', () => {
    const res = k.calculateMonthlyRevDone(1, 1, 2020);
    expect(res).toBeGreaterThan(0);
})

test('monthly acc revenue done should be greather than 0', () => {
    const res = k.calculateMonthlyAccRevDone(1, 1, 2020);
    expect(res).toBeGreaterThan(0);
})

test('monthly oth revenue done should be greather than 0', () => {
    const res = k.calculateMonthlyOthRevDone(1, 1, 2020);
    expect(res).toBeGreaterThan(0);
})

test('monthly revenue sold should be greather than 0', () => {
    const res = k.calculateMonthlyRevSold(1, 1, 2020);
    expect(res).toBeGreaterThan(0);
})

test('monthly acc revenue sold should be greather than 0', () => {
    const res = k.calculateMonthlyAccRevSold(1, 1, 2020);
    expect(res).toBeGreaterThan(0);
})

test('monthly oth revenue sold should be greather than 0', () => {
    const res = k.calculateMonthlyOthRevSold(1, 1, 2020);
    expect(res).toBeGreaterThan(0);
})