const Kalkulin = require('./../Kalkulin');
const Demand = require('./../models/Demand');
const k = new Kalkulin();

test('CountPriceForDemand', () => {
    const d = new Demand(1,1,"User New 1",1,17,11,2019,16,true,true,0.7,false);
    const result = k.countPriceForDemand(1, d);
    expect(result).not.toBeNull();
    expect(result).not.toBeUndefined();
});
test('averageDailyRate', () => {
    const result = k.averageDailyRate(1);
    expect(result).not.toBeNull();
    expect(result).not.toBeUndefined();
});
test('occupancyRate', () => {
    const result = k.occupancyRate(1);
    expect(result).not.toBeNull();
    expect(result).not.toBeUndefined();
});
test('calculateQuestSatisfaction', () => {
    const result = k.calculateQuestSatisfaction(1);
    expect(result).not.toBeNull();
    expect(result).not.toBeUndefined();
});
test('calculateOfferAtract', () => {
    const result = k.calculateOfferAtract(1);
    expect(result).not.toBeNull();
    expect(result).not.toBeUndefined();
});
test('calculateProductsQuality', () => {
    const result = k.calculateProductsQuality(1);
    expect(result).not.toBeNull();
    expect(result).not.toBeUndefined();
});
test('getPotentialHRFB', () => {
    const result = k.getPotentialHRFB(1);
    expect(result).not.toBeNull();
    expect(result).not.toBeUndefined();
});
test('getPotentialHRAsRec', () => {
    const result = k.getPotentialHRAsRec(1);
    expect(result).not.toBeNull();
    expect(result).not.toBeUndefined();
});
test('calculateHRPotential', () => {
    const result = k.calculateHRPotential(1);
    expect(result).not.toBeNull();
    expect(result).not.toBeUndefined();
});
test('calculateMaterialQuality', () => {
    const result = k.calculateMaterialQuality(1);
    expect(result).not.toBeNull();
    expect(result).not.toBeUndefined();
});
test('calculateEquipQuality', () => {
    const result = k.calculateEquipQuality(1);
    expect(result).not.toBeNull();
    expect(result).not.toBeUndefined();
});
test('saveResults', () => {
    const result = k.saveResults(1);
    expect(result).toBeUndefined();
});