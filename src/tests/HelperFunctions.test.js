const Kalkulin = require('./../Kalkulin');
const k = new Kalkulin();

test('Days', () => {
    const r = k.getDaysInMonth();
    expect(r).toBe(30);
});
test('Date', () => {
    const r = k.getLastMonth();
    expect(r).not.toBeUndefined();
});
test('Date', () => {
    const r = k.getNextMonth();
    expect(r).not.toBeUndefined();
});