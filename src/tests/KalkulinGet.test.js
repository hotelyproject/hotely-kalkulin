const Kalkulin = require('./../Kalkulin');
const Demand = require('./../models/Demand');
const Facility = require('./../models/Facility.js');
const FoodMaterial = require('./../models/FoodMaterialQuality.js');
const PotentialHR = require('./../models/PotentialHR.js');
const PotentialHRAS = require('./../models/PotentialHRAS.js');
const Promotion = require('./../models/Promotion.js');
const RoomMaterial = require('./../models/RoomMaterialQuality.js');
const Room = require('./../models/Room.js');
const Hotel = require('./../models/Hotel.js');
const GameConfiguration = require('./../GameConfiguration.js');
const k = new Kalkulin();

test('List contain', () => {
    const cycle = k.getList(2);
    expect(cycle[0]).toContain(80);
});
test('Get demand', () => {
    const d = new Demand(1,1,"User New 1",1,17,9,2019,16,false,true,0.7,false);
    const demand = k.getDemand(1);
    expect(demand).toEqual(d);
});
test('Get facility', () => {
    const i = new Facility(1,2,1,2);
    const t = k.getFacility(1);
    expect(t).toEqual(i);
});
test('Get FoodMaterial', () => {
    const i = new FoodMaterial(1,2,1);
    const t = k.getFoodMaterial(1);
    expect(t).toEqual(i);
});
test('Get PotentialHR', () => {
    const i = new PotentialHR(1,1,2,2,1,2);
    const t = k.getHR(1);
    expect(t).toEqual(i);
});
test('Get PotentialHRAS', () => {
    const i = new PotentialHRAS(1,1,2,3,2);
    const t = k.getHRAS(1);
    expect(t).toEqual(i);
});
test('Get Promotion', () => {
    const i = new Promotion(1,2,1);
    const t = k.getPromotion(1);
    expect(t).toEqual(i);
});
test('Get RoomMaterial', () => {
    const i = new RoomMaterial(1,2,1);
    const t = k.getRoomMaterial(1);
    expect(t).toEqual(i);
});
test('Get Room', () => {
    const i = new Room(1,1,null,5,3,0.5);
    const t = k.getRoom(1);
    expect(t).toEqual(i);
});
test('getNumberOfRooms', () => {
    const t = k.getNumberOfRooms(1);
    expect(t).toBe(80);
});
test('getPlayerRoomsByOccupancy', () => {
    const t = k.getPlayerRoomsByOccupancy(2);
    expect(t).toEqual({"emptyRooms": [],"fullRooms": [new Room(15,2,9,14,4,0.7), new Room(16,2,10,10,4,0.7)]});
});
test('getRoomsByDemand', () => {
    const t = k.getRoomsByDemand(new Demand(1,1,"User New 1",1,17,9,2019,16,false,true,0.7,false));
    expect(t).toEqual([]);
});
test('getPlayersRooms', () => {
    const t = k.getPlayersRooms(2);
    expect(t).toEqual([new Room(15,2,9,14,4,0.7), new Room(16,2,10,10,4,0.7)]);
});
test('getPlayerRoomMaterial', () => {
    const t = k.getPlayerRoomMaterial(2);
    expect(t).toEqual(new RoomMaterial(1,2,1));
});
test('getPlayerPromotion', () => {
    const t = k.getPlayerPromotion(2);
    expect(t).toEqual(new Promotion(1,2,1));
});/*
test('getPlayerHRs', () => {
    const t = k.getPlayerHRs(2);
    expect(t).toEqual([new PotentialHR(2,2,3,2,2,2)]);
});/*
test('getPlayerHRAS', () => {
    const t = k.getPlayerHRAS(2);
    expect(t).toEqual([new PotentialHRAS(2,2,2,2,2)]);
});*/
test('getPlayerFoodMaterial', () => {
    const t = k.getPlayerFoodMaterial(1);
    expect(t).toEqual(new FoodMaterial(0,1,0.5));
});
test('getPlayerFacility', () => {
    const t = k.getPlayerFacility(1);
    expect(t).toEqual(new Facility(0,1,0.44,0.8));
});
test('getNumberOfFreeRooms', () => {
    const t = k.getNumberOfFreeRooms(1,10,11);
    expect(t).toBeGreaterThan(0);
});
test('getDemandsInMonth', () => {
    const t = k.getDemandsInMonth(1,10,2019);
    expect(t).toEqual([new Demand(5,1,"User New 2",2,24,10,2019,9,true,true,0.7,true)]);
});
/*
test('getAcceptedDemands', () => {
    const t = k.getAcceptedDemands(1,10,2019);
    expect(t).toEqual(new Demand(5,1,"User New 2",2,24,10,2019,9,true,true,true));
});*/
test('getResults', () => {
    const result = {
        equipQuality: 1,
        materialQuality: 0.75,
        hrPotential: 36.737712600388654,
        offerAtract: 0.64,
        productsPortfolio: 0,
        offerAccesibility: 1,
        guestSatisfaction: 50.609640750485816,
        productsQuality: 19.243856300194327
      };
    const t = k.getResults(1);
    expect(t).toEqual(result);
});
test('getAllPlayerResults', () => {
    const t = k.getAllPlayerResults(1);
    expect(t).not.toBeUndefined();
});
test('getResultsFromDb', () => {
    const result = {
        "id": 0,
        "userId": 1,
        "month": 10,
        "year": 2019,
        "value": {
          "equipQuality": 1,
          "materialQuality": 0.75,
          "hrPotential": 119.67886019822639,
          "offerAtract": 0.94,
          "productsPortfolio": 1,
          "offerAccesibility": 1,
          "guestSatisfaction": 154.28607524778297,
          "productsQuality": 60.714430099113194
        }
      };
    const t = k.getResultsFromDb(1,10, 2019);
    expect(t).toEqual(result);
});
test('Get Hotel', () => {
    const d = new Hotel(0,1);
    const h = k.getHotel(0);
    expect(h).toEqual(d);
});
test('Get game config', () => {
    const d = new GameConfiguration(0.5,  0.5,   0.5,   0.5,   0.5,   0.5,   0.5,   0.5,   0.5,   0.5,   0.5,   0.5,   0.5,   0.5,   0.5,  0.4,  0.3,  0.3,  0.8,  13000,  24000,  111111,  11111,  2222222,  333333,  4444444,  555555,  6666666,  777777,  55555,  4444444,  33333,  2222222,  33333,  5555,  2222222,  333333,  4444444,  555555,  6666666,  777777,  55555,  4444444,  33333,  2222222,  33333,  5554444,  );
    const h = k.getConfig(d);
    expect(h).toEqual(d);
});