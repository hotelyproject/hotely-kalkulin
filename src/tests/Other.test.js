const Kalkulin = require('./../Kalkulin');
const Demand = require('./../models/Demand');
const Room = require('./../models/Room.js');
const k = new Kalkulin();

test('setSettings', () => {
    const s = {
        userId: 2,
        f: {investment: 1, level: 2},
        rm: {level: 1},
        fm: {level: 1},
        p: {investment: 1},
        hr: {place: 4, position: 2, type: 2, count: 2},
        hras: {position: 1, type: 2, count: 2}
    };
    const result = k.setSettings(s);
    expect(result).toBeUndefined();
});
test('removeOldDemandFromRooms', () => {
    const result = k.removeOldDemandFromRooms();
    expect(result).toBeUndefined();
});
test('setDemandToRooms', () => {
    const d = new Demand(10,2,"User New 0",2,10,7,2019,10,false,true,0.7,true);
    const r = new Room(16,2,null,10,4,0.7);
    const result = k.setDemandToRooms(d, [r]);
    expect(result).toBeUndefined();
});
test('generateDemands', () => {
    const result = k.generateDemands(3,1,true,1,false);
    expect(result).toBeUndefined();
});
test('acceptDemand', () => {
    const result = k.acceptDemand(new Demand(10,2,"User New 0",2,10,7,2019,10,false,true,0.7,true));
    expect(result).toEqual(new Demand(10,2,"User New 0",2,10,7,2019,10,true,true,0.7,true));
});
test('acceptDemand Reqeust', () => {
    const FEDemand = {
        id: 33,
        userId: 1,
        name: "User New 1",
        numberOfGuests: 2,
        startDay: 15,
        startMonth: 8,
        startYear: 2020,
        period: 4,
        accepted: false,
        withFood: true,
        roomType: 0.7,
        group: false
      };
    const result = k.acceptDemandRequest(FEDemand);
    expect(result).toEqual(new Demand(33,1,"User New 1",2,15,8,2020,4,false,true,0.7,false));
});

test('getGroupDemands', () => {
    const result = k.getGroupDemands(2);
    expect(result).toEqual([new Demand(10,2,"User New 0",2,10,7,2019,10,true,true,0.7,true)]);
});
test('setDailySettings', () => {
    const daySetting = {
        0: 100,
        1: 110,
        2: 5,
        3:33,
        4:68,
        5:99,
        6:12,
        7:12,
        8:12,
        9:12,
        10:12,
        11:12,
        12:12,
        13:12,
        14:12
      }
    const result = k.setDailySettings(2,0,11,2019,daySetting);
    expect(result).toBeUndefined();
});